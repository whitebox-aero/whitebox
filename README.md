<div style="text-align: center;">
  <img src="branding/logo.svg" alt="Whitebox logo" />
</div>

## Description

Whitebox is a flight recording and sharing system for small airplanes, enhancing the flight experience by allowing pilots and passengers to record, review, and share flight data, communications, and media. It can also be used to review training flights during debrief, taking care of synchronizing and uploading the flight data to the cloud.

It's an open source counterpart to the black boxes airplanes have to record all information about a flight.

Documentation is available in [docs.whitebox.aero](https://docs.whitebox.aero).

## Certification

It is an uncertified system which is not directly connected to the airplane systems, much like a tablet running Foreflight or SkyDemon. It can thus be brought in with you in any general aviation airplane. Being also open source, it can be freely accessed and modified.

## Technical description

Whitebox is a small computer which you bring with you in the cockpit during flight, and connect to various additional recording devices, such as a 360 camera or the passengers phones.

It can be accessed by the pilot and the passengers, from tablets or phones over wifi (or your desktop computer after the flight), using the devices' browser.

## Roadmap

<!-- prettier-ignore-start -->

Features being considered (TBD):

- Video recording and playback:
    - 360 camera external view
    - Cockpit interior cameras views
    - Control the 360 camera orientation during the flight
    - Passenger phone recordings synchronization (photos/videos), tagged to specific flight times/locations
    - Automatically generate a video to share, showing the highlights of the flight (tagged during the flight)
- Automatic recording and transcription of radio and in-cockpit communications
- GPS position tracking and 3D route visualization (like/via FlySto)
- Note-taking for in-flight observations (instructions, comments, remarks by passengers)
- Automated synchronization of data to the cloud - bring the Whitebox within reach of a wifi connection and it will happen automatically
- Community contribution platform (public/forum/wiki) for sharing flight experiences:
    - A page dedicated to each flight, with access permission management
    - Link and embed flight recordings in external platforms (e.g., YouTube), including deep-linking to a specific section of the flight
- Pre-flight briefing and post-flight debriefing
    - Review critical phases of flights
    - Integration with external data sources (OpenFlightMaps, Flypedia) for airport and flight procedure information sharing: use data to share and discover information about airports, flight procedures and practices, radio recordings...

<!-- prettier-ignore-end -->

## System Overview

Whitebox is built on Django Channels, providing real-time communication capabilities. The system features:

- A web interface for flight tracking and recording
- Plugin system for extensibility
- Standard API for plugins to interact with the system
- Event-driven architecture
- Real-time communication between clients, server, and plugins
- Database for persistent storage
- Docker support for easy deployment

Frontend is built mobile-first, so you can conveniently use it on your phone or
tablet, even in the cockpit. It's built in React, with Tailwind CSS.

## Prerequisites

- [git](https://git-scm.com/) (for cloning the repository)
- [Git LFS](https://git-lfs.com/) (for downloading asset files when cloning the repository)
- [Docker](https://www.docker.com/) (for running with Docker)

## Prerequisites Installation

### Docker

The easiest way to run or develop Whitebox is by using Docker. It sets up the whole
environment for you, including the database, backend, and frontend.

Check whether Docker is installed by running:

```bash
docker --version
```

If not, you can install it using official convenience script like so:

```bash
curl -L https://get.docker.com/ -o docker-install.sh
chmod +x docker-install.sh
sudo ./docker-install.sh
```

### Git LFS

This repository uses [Git LFS](https://git-lfs.com) to store the asset files.
If you do not have it installed, the repository will clone successfully,
but the asset files will not be downloaded and the frontend will look broken.

To check whether Git LFS is installed, run:

```bash
git lfs --version
```

If it isn't, installation instructions are available for various distributions
and operating systems.

On Ubuntu, it's as simple as running:

```bash
sudo apt install git-lfs
```

For other distributions and operating systems, you can check out the
[installation instructions](https://github.com/git-lfs/git-lfs/blob/main/README.md#installing).

## Installation and Usage

Ensure that you have the all the [prerequisites](#prerequisites) installed before proceeding.

All official Whitebox plugins are included in the repository.
Hence, no extra steps are required to install them.

### Optional Instructions - Full Hardware Setup

If you are just setting up the project on your laptop to test it, you can skip this section.

<!-- prettier-ignore-start -->

Following is the recommended hardware list for the full Whitebox setup. Direct links to the products are provided for convenience, we do not endorse any specific sellers.

- **2 x Software-Defined Radio (SDR) devices**
    - [Nooelec RTL-SDR v5](https://www.desertcart.in/products/30167835-nooelec-rtl-sdr-v5-bundle-nesdr-smart-hf-vhf-uhf-100khz-1-75ghz-software-defined-radio-premium-rtlsdr-w-0-5ppm-tcxo-sma-input-aluminum-enclosure-3-antennas-rtl2832u-r820t2-based-radio)
- **1 x GPS device**
    - [NEO-M9N GNSS Module](https://gnss.store/neo-m9n-gnss-modules/119-elt0103.html) 
- **1 x Antenna (if GPS device does not come with one)**
    - [Antenna](https://www.amazon.in/dp/B07B92QW7N)
- **1 x Single Board Computer (SBC)**
    - Raspberry Pi 3 
    - Raspberry Pi 4
    - Raspberry Pi 5

<!-- prettier-ignore-end -->

The following steps are necessary for the setup with the full supported hardware configuration:

Connect your SBC to the internet via ethernet and SSH into it. Internet connection is required to setup the Whitebox.

Clone the repository:

```bash
git clone https://gitlab.com/whitebox-aero/whitebox.git
```

Run access point setup script to create a wifi network for the Whitebox:

```bash
sudo make setup-wifi
```

This will create a wifi network with the name `whitebox` and password `12345678`.

Raspberry Pi 3+ requires the following additional steps for access point to work properly:

```bash
COUNTRY=$(curl -s ipinfo.io/json | jq -r .country)
sudo raspi-config nonint do_wifi_country ${COUNTRY}
```

Additionally, you may need to restart the Raspberry Pi for the changes to take effect in some cases.

Now, Run udev rules setup script to allow Whitebox to access USB devices:

```bash
sudo make setup-udev
```

Connect the two SDR devices and the GPS device to the Whitebox.
Now, all that is left is to run the docker-compose file.

### Starting Whitebox

Now, all that is left is to run the docker-compose file. Run the following command to start the Whitebox:

```bash
docker compose up -d
```

Once the containers are built and started, you should be able to connect to the `whitebox` wifi network.

Now, you can access the Whitebox frontend on [http://10.42.0.1:80](http://10.42.0.1:80). Additionally, backend is available on [http://10.42.0.1:8000](http://10.42.0.1:8000).

- Learn more about the [Whitebox Architecture](https://docs.whitebox.aero/architecture/)
- Check out the [Development Guide](https://docs.whitebox.aero/development_guide/)
- Explore the [Plugin Guide](https://docs.whitebox.aero/plugin_guide/) for creating custom plugins

## Plugins

Official Whitebox plugins are available on [PyPI](https://pypi.org/search/?q=%22whitebox-plugin-%22&o=).

To develop your own plugins for Whitebox, you can refer to the [Plugin Guide](https://docs.whitebox.aero/plugin_guide/).

## Support

If you find an issue with the software, please [open a bug report ticket](https://gitlab.com/whitebox-aero/whitebox/-/issues/new),
or write to [support@whitebox.aero](mailto:support@whitebox.aero).

You can also come:

- [join the dev@ mailing-list](https://ml.whitebox.aero/mailman3/lists/dev.ml.whitebox.aero/)
- [chat with us on Matrix (#whitebox-aero:matrix.org)](https://matrix.to/#/#whitebox-aero:matrix.org)

## Contributing

To contribute, feel free to open a merge request, or reach out to contact@whitebox.aero.

## License

The project is released under the AGPLv3.

## Acknowledgements

This project is tested with [BrowserStack](https://www.browserstack.com).
