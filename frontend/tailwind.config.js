/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      'success': 'var(--color-success)',
      'error': 'var(--color-error)',
      'warning': 'var(--color-warning)',
      'info': 'var(--color-info)',
      'primary': 'var(--color-brand-primary)',

      'full-emphasis': 'var(--color-full-emphasis)',
      'high-emphasis': 'var(--color-high-emphasis)',
      'medium-emphasis': 'var(--color-medium-emphasis)',
      'low-emphasis': 'var(--color-low-emphasis)',
      'x-low-emphasis': 'var(--color-x-low-emphasis)',
      'white-full-emphasis': 'var(--color-white-full-emphasis)',

      'surface-primary': 'var(--color-surface-primary)',

      'borders-default': 'var(--color-borders-default)',
      'borders-dark': 'var(--color-borders-dark)',

      'input-bg': 'var(--color-input-bg))',

      // 'brand': {
      //   'primary': 'var(--color-brand-primary))',
      //   'green': 'var(--color-brand-green))',
      //   'orange': 'var(--color-brand-orange))',
      // },
      // 'gray': {
      //   '1': 'var(--color-gray-1))',
      //   '2': 'var(--color-gray-2))',
      //   '3': 'var(--color-gray-3))',
      //   '4': 'var(--color-gray-4))',
      //   '5': 'var(--color-gray-5))',
      //   '6': 'var(--color-gray-6))',
      //   '7': 'var(--color-gray-7))',
      // },
      'white': 'var(--color-white)',
      'red': 'var(--color-red)',
    },
    extend: {
      'px': {
        '0.25': '0.25rem',
      },
      'gap': {
        '0.75': '0.75rem',
      }
    },
  },
  plugins: [],
}
