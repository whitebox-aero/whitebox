import {Outlet} from "react-router";
import SnackBarContainer from "./components/common/SnackBarContainer";

const App = () => {
  return (
      <>
        <SnackBarContainer />
        <Outlet />
      </>
  )
}

export default App;
