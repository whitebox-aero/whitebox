import {act} from "@testing-library/react";
import useToastsStore from "../../stores/toasts";
import * as toastMethods from "../../stores/toasts";

describe('ToastsStore', () => {
  const getState = useToastsStore.getState

  beforeEach(() => {
    // Toasts use timers for auto-removal, so let's just mock them in all tests
    // as nothing should actually be deferred in these tests
    vi.useFakeTimers()
  })

  afterEach(() => {
    // Reset the timers mock after each test
    vi.useRealTimers()
  })

  it('should allow adding a toast', () => {
    // GIVEN the default state
    expect(getState().toasts).toHaveLength(0)

    // WHEN adding a toast
    const toast = {type: "success", message: "Hello, world!"}
    act(() => useToastsStore.getState().addToast(toast))

    // THEN the toast should be added
    expect(getState().toasts).toHaveLength(1)
    expect(getState().toasts[0]).toMatchObject(toast)
  })

  it('should allow removing a toast', () => {
    // GIVEN a toast
    const toast = {type: "success", message: "Hello, world!"}
    act(() => useToastsStore.getState().addToast(toast))
    expect(getState().toasts).toHaveLength(1)

    // WHEN removing the toast
    act(() => useToastsStore.getState().removeToast(toast.id))

    // THEN the toast should be removed
    expect(getState().toasts).toHaveLength(0)
  })

  it('should auto-remove a toast after 3 seconds', async () => {
    // GIVEN a toast
    const toast = {type: "success", message: "Hello, world!"}
    act(() => useToastsStore.getState().addToast(toast))
    expect(getState().toasts).toHaveLength(1)

    // WHEN waiting for 3 seconds (default toast removal time)
    await vi.advanceTimersByTime(3 * 1000)

    // THEN the toast should be removed
    expect(getState().toasts).toHaveLength(0)
  })

  it('should allow specifying a custom timeout for a toast', async () => {
    // GIVEN a toast with a custom timeout
    const toast = {type: "success", message: "Hello, world!", timeout: 1}
    act(() => useToastsStore.getState().addToast(toast))
    expect(getState().toasts).toHaveLength(1)

    // WHEN waiting for half-of-a-second (less than expiry time)
    await vi.advanceTimersByTime(0.5 * 1000)

    // THEN the toast not should be removed yet
    expect(getState().toasts).toHaveLength(1)

    // but
    // WHEN waiting for another half-of-a-second (to let the toast expire)
    await vi.advanceTimersByTime(0.5 * 1000)

    // THEN the toast should be removed
    expect(getState().toasts).toHaveLength(0)
  })

  it('should allow adding multiple toasts', () => {
    // GIVEN the default state
    expect(getState().toasts).toHaveLength(0)

    // WHEN adding multiple toasts
    const toast1 = {type: "success", message: "Hello, world!"}
    const toast2 = {type: "error", message: "Goodbye, world!"}
    act(() => {
      useToastsStore.getState().addToast(toast1)
      useToastsStore.getState().addToast(toast2)
    })

    // THEN the toasts should be added
    expect(getState().toasts).toHaveLength(2)
    expect(getState().toasts[0]).toMatchObject(toast1)
    expect(getState().toasts[1]).toMatchObject(toast2)
  })

  it('should remove only the toast with the given ID', () => {
    // GIVEN multiple toasts
    const toast1 = {type: "success", message: "Hello, world!"}
    const toast2 = {type: "error", message: "Goodbye, world!"}
    act(() => {
      useToastsStore.getState().addToast(toast1)
      useToastsStore.getState().addToast(toast2)
    })
    expect(getState().toasts).toHaveLength(2)

    // WHEN removing one toast
    act(() => useToastsStore.getState().removeToast(toast1.id))

    // THEN only the specified toast should be removed
    expect(getState().toasts).toHaveLength(1)
    expect(getState().toasts[0]).toMatchObject(toast2)
  })

  describe('toast types', () => {
    const toastMethodName2ExpectedTypeMap = {
      "toastSuccess": "success",
      "toastError": "error",
      "toastInfo": "info",
    }

    it.each(
        Object.entries(toastMethodName2ExpectedTypeMap)
    )('should allow adding a success toast', (methodName, expectedType) => {
      // GIVEN the default state
      expect(getState().toasts).toHaveLength(0)

      // WHEN adding a success toast
      const message = "Hello, world!"
      act(() => {
        const method = toastMethods[methodName]
        method({message})
      })

      // THEN the toast should be added
      expect(getState().toasts).toHaveLength(1)
      expect(getState().toasts[0]).toMatchObject({type: expectedType, message})
    })
  })
})
