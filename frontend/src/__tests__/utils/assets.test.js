import {isAssetVideo} from '../../utils/assets'

describe('assets utils', () => {
  describe('isAssetVideo', () => {
    it('should return true for .mp4 files', () => {
      const result = isAssetVideo('video.mp4')
      expect(result).toBe(true)
    })

    it('should return false for other files', () => {
      const result = isAssetVideo('image.jpg')
      expect(result).toBe(false)
    })
  })
})
