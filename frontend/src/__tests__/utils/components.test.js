import {getClasses} from "../../utils/components";

describe('components utils', () => {
  describe('getClasses', () => {
    it('should concatenate classes', () => {
      const result = getClasses('class1', 'class2', 'class3')
      expect(result).toBe('class1 class2 class3')
    })

    it('should ignore falsy values', () => {
      const result = getClasses('class1', false, 'class2', null, 'class3')
      expect(result).toBe('class1 class2 class3')
    })

    it('should trim classes', () => {
      const result = getClasses(' class1 ', ' class2 ', ' class3 ')
      expect(result).toBe('class1 class2 class3')
    })
  })
})
