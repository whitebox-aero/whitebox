import DashboardScreen from "../../../components/dashboard/DashboardScreen";
import {renderWithRouter} from "../../../utils/testing_utils";
import moxios from "moxios";
import API from "../../../services/api.js";

describe('DashboardScreen', () => {
  beforeEach(async () => {
    moxios.install(API.client)
  })

  afterEach(() => {
    moxios.uninstall(API.client)
  })

  it('should render TopNav', () => {
    const {container} = renderWithRouter(<DashboardScreen />)
    const topNav = container.querySelector('.c_topnav')
    expect(topNav).toBeInTheDocument()
  })

  it('should render Map', () => {
    const {container} = renderWithRouter(<DashboardScreen />)
    const map = container.querySelector('.c_map_area')
    expect(map).toBeInTheDocument()
  })
})
