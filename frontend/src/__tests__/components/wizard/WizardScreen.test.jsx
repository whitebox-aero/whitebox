import {screen, act} from "@testing-library/react";
import WizardScreen from "../../../components/wizard/WizardScreen";
import useDeviceWizardStore from "../../../stores/device_wizard";
import moxios from "moxios";
import API from "../../../services/api";
import {fixtureSupportedDeviceCodename1, fixtureSupportedDevices} from "../../../../tests/__fixtures__/devices";
import {renderWithRouter} from "../../../utils/testing_utils";


describe('WizardScreen', () => {
  const getWizardStoreState = useDeviceWizardStore.getState

  beforeEach(async () => {
    moxios.install(API.client)

    await moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: fixtureSupportedDevices,
      })
    })

    await act(async () => await getWizardStoreState().fetchSupportedDevices())
    await act(async () =>
        getWizardStoreState().setSelectedDeviceCodename(
            fixtureSupportedDeviceCodename1,
        ))
  })

  afterEach(() => {
    moxios.uninstall(API.client)
  })

  it('should render first page upon initial render', () => {
    renderWithRouter(<WizardScreen />)
    expect(screen.getByText("Let's get you set up")).toBeInTheDocument()
  })

  it.each([0, 1, 2, 3, 4, 5])(
      'should render appropriate page per DeviceWizardStore state',
      async (pageNo) => {
        act(() => getWizardStoreState().setPageNumber(pageNo))

        renderWithRouter(<WizardScreen />)

        let lookupText
        if (pageNo === 0)
          lookupText = "Let's get you set up"
        else if (pageNo === 1)
          lookupText = "Select a device"
        else if (pageNo >= 2 && pageNo <= 5)
          lookupText = "Device connection checklist"

        expect(screen.getByText(lookupText)).toBeInTheDocument()
      })
})
