import {
  getGenericClassNames
} from "../../../../components/common/scaffolding/dynamic_elements/utils";

describe('Dynamic elements utils.js', () => {
  describe('getGenericClassNames', () => {
    it('should return an array of classes', () => {
      const generatedClasses = getGenericClassNames({
        config: {
          style: {
            color: 'red',
            size: 'lg',
            font_weight: 'bold',
            font_height: 'tall',
            classes: ['class1', 'class2'],
          },
        },
        props: {
          className: 'custom-class-name-right-here-yo',
        },
      })

      expect(generatedClasses).toEqual([
        'c_generic_element',
        'text-red',
        'text-lg',
        'font-bold',
        'leading-tall',
        'class1',
        'class2',
        'custom-class-name-right-here-yo',
      ])
    })
  })
})
