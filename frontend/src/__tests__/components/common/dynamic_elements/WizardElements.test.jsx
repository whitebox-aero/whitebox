// Make sure to import this one before importing specific
import type2ComponentMap from "../../../../components/common/scaffolding/dynamic_elements/type2ComponentMap";  // eslint-disable-line

import {act, render} from "@testing-library/react";
import moxios from 'moxios'
import useDeviceWizardStore from "../../../../stores/device_wizard";
import API from "../../../../services/api";
import {generateDynamicElement} from "../../../../components/common/scaffolding/dynamic_elements/utils";
import userEvent from "@testing-library/user-event";

const fixtureSupportedDevicesWithAllFields = {
  "supported_devices": [
    {
      "codename": "device_1",
      "name": "Device 1",
      "connection_types": {
        "test_type": {
          "name": "Test Type",
          "fields": {
            "text": {
              "name": "Text Field",
              "type": "text",
              "required": false
            },
            "password": {
              "name": "Password Field",
              "type": "password",
              "required": true
            }
          }
        },
      },
      "wizard_steps": [
        {
          "type": "wizard_field_block",
          "config": {}
        }
      ]
    }
  ]
}

describe('WizardElements', () => {
  const targetDevice = fixtureSupportedDevicesWithAllFields.supported_devices[0]
  const deviceConnectionFields = targetDevice.connection_types.test_type.fields

  describe('DEWizardFieldBlock', () => {
    beforeEach(async () => {
      moxios.install(API.client)

      await moxios.wait(() => {
        const request = moxios.requests.mostRecent()
        request.respondWith({
          status: 200,
          response: fixtureSupportedDevicesWithAllFields,
        })
      })

      const getDeviceWizardState = useDeviceWizardStore.getState
      await act(async () => {
        await getDeviceWizardState().fetchSupportedDevices()
        await getDeviceWizardState().setSelectedDeviceCodename('device_1')
        await getDeviceWizardState().setDeviceSetupStep(0)
      })
    })

    afterEach(() => {
      moxios.uninstall(API.client)
    })

    describe('rendering', () => {
      it('should render all fields properly', () => {
        const index = 0
        const config = targetDevice.wizard_steps[index]
        expect(config.type).toEqual('wizard_field_block')

        const ComponentToRender = generateDynamicElement({
          config: config,
          index: index,
        })

        const {container} = render(ComponentToRender)

        const renderedFields = container.querySelectorAll('.c_generic_element')

        // One for parent block, plus fields
        const expectedFieldCount = 1 + Object.keys(deviceConnectionFields).length
        expect(renderedFields).toHaveLength(expectedFieldCount)

        // Convert NodeList to Array
        const renderedFieldsList = [...renderedFields]

        const fullFieldConfig = Object.entries(deviceConnectionFields)

        renderedFieldsList.forEach((field, index) => {
          if (index === 0) {
            // Skip the parent block
            return
          }

          const lookupIndex = index - 1  // Skip the parent block
          const [fieldName, fieldConfig] = fullFieldConfig[lookupIndex]

          switch(fieldConfig.type) {
            case 'text':
              expect(field).not.toBeNull()
              expect(field).not.toHaveAttribute('required')
              expect(field).toHaveAttribute('placeholder', fieldConfig.name)
              expect(field).toHaveAttribute('name', fieldName)
              break
            case 'password':
              expect(field).not.toBeNull()
              expect(field).toHaveAttribute('required')
              expect(field).toHaveAttribute('placeholder', fieldConfig.name)
              expect(field).toHaveAttribute('name', fieldName)
              break
            default:
              throw new Error(`Unknown field type: ${fieldConfig.type}`)
          }
        })
      })
    })

    describe('interaction', () => {
      it('should update connection details on field change', async () => {
        const user = userEvent.setup()

        const index = 0
        const config = targetDevice.wizard_steps[index]

        const ComponentToRender = generateDynamicElement({
          config: config,
          index: index,
        })

        const {container} = render(ComponentToRender)

        const renderedFields = container.querySelectorAll('.c_generic_element')

        // One for parent block, plus fields
        const expectedFieldCount = 1 + Object.keys(deviceConnectionFields).length
        expect(renderedFields).toHaveLength(expectedFieldCount)

        // Convert NodeList to Array
        const renderedFieldsList = [...renderedFields]

        const fullFieldConfig = Object.entries(deviceConnectionFields)

        for (let i = 0; i < renderedFieldsList.length; i++) {
          if (index === 0) {
            // Skip the parent block
            return
          }

          const field = renderedFieldsList[i]

          const lookupIndex = index - 1  // Skip the parent block
          const [fieldName, fieldConfig] = fullFieldConfig[lookupIndex]

          switch(fieldConfig.type) {
            case 'text':
            case 'password': {
              await act(() => {
                user.type(field, 'new-value')
              })

              const setDetails =
                  useDeviceWizardStore.getState().connectionDetails[fieldName]

              expect(setDetails).toEqual('new-value')
              break
            }
            default:
              throw new Error(`Unknown field type: ${fieldConfig.type}`)
          }
        }
      })
    })
  })
})
