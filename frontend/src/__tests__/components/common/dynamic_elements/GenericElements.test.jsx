import {render} from "@testing-library/react";
import {DEBlock} from "../../../../components/common/scaffolding/dynamic_elements/GenericElements";

describe('DEBlock', () => {
  it('should render with children', () => {
    const config = {
      type: 'block',
    }
    const children = (
        <div className="children-lookup-class">Children Lookup Text</div>
    )
    const props = {prop: 'prop'}

    const {container} = render(
        <DEBlock config={config} children={children} {...props} />
    )

    const childrenElement = container.querySelector('.children-lookup-class')
    expect(childrenElement).toHaveTextContent('Children Lookup Text')
  })
})