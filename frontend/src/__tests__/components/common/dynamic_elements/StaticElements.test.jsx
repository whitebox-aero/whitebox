import {render} from "@testing-library/react";
import {DEImage, DEText, DEVideo} from "../../../../components/common/scaffolding/dynamic_elements/StaticElements";
import API from "../../../../services/api";

describe('DEText', () => {
  it('should render', () => {
    const config = {
      type: 'text',
      text: 'text-value',
    }

    const {container} = render(
        <DEText config={config} />
    )

    const textElement = container.querySelector('p')
    expect(textElement).toHaveTextContent('text-value')
  })
})

describe('DEImage', () => {
  it('should render', () => {
    const config = {
      type: 'image',
      url: '/url-value',
    }

    const {container} = render(
        <DEImage config={config} />
    )
    const expectedUrl = API.getUrl(config.url)

    const imageElement = container.querySelector('img')
    expect(imageElement).toHaveAttribute('src', expectedUrl)
  })
})

describe('DEVideo', () => {
  it('should render', () => {
    const config = {
      type: 'video',
      url: '/url-value',
    }

    const {container} = render(
        <DEVideo config={config} />
    )
    const expectedUrl = API.getUrl(config.url)

    const videoElement = container.querySelector('video')
    expect(videoElement).toBeInTheDocument()

    const sourceElement = container.querySelector('video > source')
    expect(sourceElement).toHaveAttribute('src', expectedUrl)
  })
})
