import {render} from "@testing-library/react";
import DynamicContentRenderer from "../../../../components/common/bridge/DynamicContentRenderer.jsx";

describe('DynamicContentRenderer', () => {
  it('should render correctly', async () => {
    // Prepare the HTML content
    const html = `
    <div class="outer">
      <div class="inner">
        Test
      </div>
    </div>
    `

    const css = [
        "/one.css",
    ]

    const js = [
        "/two.js",
    ]
    const { container } = render(
        <DynamicContentRenderer
            html={html}
            css={css}
            js={js} />
    )

    // Ensure that the provided data is rendered semantically-verbatim into the
    // container. We cannot use `toContainHTML` directly as the HTML content
    // may be restyled by the renderer, so we'll be using precise selectors for
    // the exact data that we are expecting

    // First test HTML
    const inner = container.querySelector('div.outer > div.inner')
    expect(inner).toHaveTextContent('Test')

    // Then CSS
    const link = container.querySelector(
        `link[type="text/css"][href="${css[0]}"]`
    )
    expect(link).toBeInTheDocument()

    // Then JS
    const script = container.querySelector(`script[src="${js[0]}"]`)
    expect(script).toBeInTheDocument()
  })

  it('should render correctly with Shadow DOM', async () => {
    const html = `
    <div class="inside-shadow-dom">
      We r in!!1
    </div>
    `

    const css = [
        "/shadow/one.css",
    ]

    const js = [
        "/shadow/two.mjs",
    ]

    const { container } = render(
        <DynamicContentRenderer
            html={html}
            css={css}
            js={js}
            useShadowDOM={true}
            className="outside-shadow-dom" />
    )

    // Because the shadow DOM implies certain encapsulation, we do not have
    // direct access to it from the light DOM
    const root = container.querySelector('.outside-shadow-dom')
    expect(root).toBeInTheDocument()

    const insideFromOutside = root.querySelector('.inside-shadow-dom')
    expect(insideFromOutside).toBeNull()

    // Now that we verified that the inner contents do not exist outside, we
    // can access the shadow DOM through the `shadowRoot` property
    const shadowContainer = container.querySelector(".outside-shadow-dom")
    expect(shadowContainer.shadowRoot).not.toBeNull()

    const shadowRoot = shadowContainer.shadowRoot

    // And again, check HTML
    const insideFromInside = shadowRoot.querySelector('.inside-shadow-dom')
    expect(insideFromInside).toHaveTextContent('We r in!!1')

        // Then CSS
    const link = shadowRoot.querySelector(
        `link[type="text/css"][href="${css[0]}"]`
    )
    expect(link).toBeInTheDocument()

    // Then JS
    const script = shadowRoot.querySelector(
        `script[type="module"][src="${js[0]}"]`
    )
    expect(script).toBeInTheDocument()
  })
})
