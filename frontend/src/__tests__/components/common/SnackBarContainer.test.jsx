import {act, render} from "@testing-library/react";
import * as toastMethods from "../../../stores/toasts";
import SnackBarContainer from "../../../components/common/SnackBarContainer";

describe('SnackBarContainer', () => {
  beforeEach(() => {
    vi.useFakeTimers()
  })

  afterEach(() => {
    vi.useRealTimers()
  })

  it('should show SnackBars for all toasts', async () => {
    act(() => {
      toastMethods.toastSuccess({message: 'Success message'})
      toastMethods.toastError({message: 'Error message'})
      // Default timeout is 3s, test one with 7s
      toastMethods.toastInfo({message: 'Info message', timeout: 7})
    })

    const {container} = render(<SnackBarContainer />)

    // When initially rendered, all three should toasts should be visible
    const snackBarsInitial = await container.querySelectorAll('.snackbar')
    expect(snackBarsInitial).toHaveLength(3)

    // After 1s, all of them should still exist
    act(() => vi.advanceTimersByTime(1000))
    const snackBarsAfter1 = await container.querySelectorAll('.snackbar')
    expect(snackBarsAfter1).toHaveLength(3)

    // After 3s total, the first two should disappear
    act(() => vi.advanceTimersByTime(3000))
    const snackBarsAfter3 = await container.querySelectorAll('.snackbar')
    expect(snackBarsAfter3).toHaveLength(1)

    // After 7s total, all should be gone
    act(() => vi.advanceTimersByTime(4000))
    const snackBarsAfter7 = await container.querySelectorAll('.snackbar')
    expect(snackBarsAfter7).toHaveLength(0)
  })
})
