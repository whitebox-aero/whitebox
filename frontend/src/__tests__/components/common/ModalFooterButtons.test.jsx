import {render, screen} from "@testing-library/react";
import ModalFooterButtons from "../../../components/common/ModalFooterButtons";

describe('ModalFooterButtons', () => {
  it('should render with only forward button', () => {
    const buttonConfig = {
      text: 'Execute order 66',
      // onClick: () => console.log('kaboom'),
    }

    render(<ModalFooterButtons fwdBtn={buttonConfig} />)
    expect(screen.getByText(buttonConfig.text)).toBeInTheDocument()
    const buttons = screen.getAllByRole('button')
    expect(buttons).toHaveLength(1)
  })

  it('should render with only back button', () => {
    const buttonConfig = {
      text: 'Cancel order 66',
    }

    render(<ModalFooterButtons bckBtn={buttonConfig} />)
    expect(screen.getByText(buttonConfig.text)).toBeInTheDocument()
    const buttons = screen.getAllByRole('button')
    expect(buttons).toHaveLength(1)
  })
})
