// This file was initially created from an example in the Zustand documentation:
// https://github.com/pmndrs/zustand/blob/v5.0.0/docs/guides/testing.md
// Some local changes apply. For more info, refer to comments & Whitebox Git history.

// The example was written by multiple authors, which you can find in the
// Zustand's source's git history, available on the above URL.

// The example source code is licensed under the MIT License, which is copied
// below, and also available here: https://github.com/pmndrs/zustand/blob/v5.0.0/LICENSE

/*
MIT License

Copyright (c) 2019 Paul Henschel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

import { cleanup } from '@testing-library/react';
import type * as ZustandExportedTypes from 'zustand'
export * from 'zustand'

const { create: actualCreate, createStore: actualCreateStore } =
  await vi.importActual<typeof ZustandExportedTypes>('zustand')

// a variable to hold reset functions for all stores declared in the app
export const storeResetFns = new Set<() => void>()

const createUncurried = <T>(
  stateCreator: ZustandExportedTypes.StateCreator<T>,
) => {
  const store = actualCreate(stateCreator)
  const initialState = store.getInitialState()
  storeResetFns.add(() => {
    store.setState(initialState, true)
  })
  return store
}

// when creating a store, we get its initial state, create a reset function and add it in the set
export const create = (<T>(
  stateCreator: ZustandExportedTypes.StateCreator<T>,
) => {
  // to support curried version of create
  return typeof stateCreator === 'function'
    ? createUncurried(stateCreator)
    : createUncurried
}) as typeof ZustandExportedTypes.create

const createStoreUncurried = <T>(
  stateCreator: ZustandExportedTypes.StateCreator<T>,
) => {
  const store = actualCreateStore(stateCreator)
  const initialState = store.getInitialState()
  storeResetFns.add(() => {
    store.setState(initialState, true)
  })
  return store
}

// when creating a store, we get its initial state, create a reset function and add it in the set
export const createStore = (<T>(
  stateCreator: ZustandExportedTypes.StateCreator<T>,
) => {
  // to support curried version of createStore
  return typeof stateCreator === 'function'
    ? createStoreUncurried(stateCreator)
    : createStoreUncurried
}) as typeof ZustandExportedTypes.createStore


// reset all stores after each test run
afterEach(() => {
  // Invoking `cleanup()` here is Whitebox's local change to the Zustand's
  // example, mentioned in the license above.
  //
  // `cleanup()` is a function that unmounts React components rendered by
  // React's `render(Component)` calls within tests. Project was initially
  // written on Create React App, which was automatically performing the
  // cleanup after tests, but the project was migrated to Vite, which does
  // not do the cleanup automatically.
  //
  // I have tried to do it in a different way (e.g. calling `cleanup` from
  // another `afterEach` hook in the test setup file), but it did not work.
  // It was hard for me to debug this and I wasted many hours on it, so do
  // not remove it unless you know exactly what the effects of it would be.
  cleanup()
  // The cleanup above HAS TO be called before resetting the stores below

  storeResetFns.forEach((resetFn) => {
    resetFn()
  })
})
