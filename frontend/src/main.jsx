import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider
} from "react-router";
import WizardScreen from "./components/wizard/WizardScreen";
import DashboardScreen from "./components/dashboard/DashboardScreen";

// region bridge
import WhiteboxAPI from "./bridge/whitebox_api.js";
import WhiteboxExtensionAPI from "./bridge/extensions.js";
// endregion bridge


const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<App />}>
          <Route path="/" element={<WizardScreen />} />
          <Route path="/dashboard" element={<DashboardScreen />} />
        </Route>
    ),
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
      <RouterProvider router={router} />
    </React.StrictMode>
);

// Attach the Whitebox API objects to `window` so that the plugins can
// interface with them
window.Whitebox = WhiteboxAPI
window.WhiteboxExtensionAPI = WhiteboxExtensionAPI

// Woohoo
console.log('Welcome to Whitebox!');
