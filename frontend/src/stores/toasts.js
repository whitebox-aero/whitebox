import {create} from "zustand";

const toastsStore = (set, get) => ({
  toasts: [],

  addToast: (toast) => {
    // Ensure unique ID
    toast.id = Date.now() + Math.random()

    // Default timeout of 3 seconds
    const timeout = toast.timeout ?? 3

    set((state) => {
      return {toasts: [...state.toasts, toast]}
    })

    // Auto-remove after 3 seconds
    setTimeout(() => {
      get().removeToast(toast.id);
    }, timeout * 1000);
  },

  removeToast: (id) => {
    set((state) => {
      return {toasts: state.toasts.filter((toast) => toast.id !== id)}
    })
  }
})

const useToastsStore = create(toastsStore)

const toastSuccess = ({...props}) =>
  useToastsStore.getState().addToast({type: "success", ...props})

const toastError = ({...props}) =>
  useToastsStore.getState().addToast({type: "error", ...props})

const toastInfo = ({...props}) =>
  useToastsStore.getState().addToast({type: "info", ...props})

export {
  toastSuccess,
  toastError,
  toastInfo,
}

export default useToastsStore
