import {create} from "zustand";
import API from "../services/api";

const devicesStore = (set) => ({
  fetchState: 'initial',
  devices: null,

  fetchDevices: async () => {
    let data

    try {
      const response = await API.devices.getDevices()
      data = await response.data
    } catch {
      set({fetchState: 'error'})
      return false
    }

    set({
      devices: data,
      fetchState: 'loaded',
    })
    return true
  },
})

const useDevicesStore = create(devicesStore)

export default useDevicesStore
