import API from "../services/api.js";

const placeholderReplacements = {
  '%API_URL%': API.baseUrl,
}

const replacePlaceholdersInString = (html) => {
  let newHtml = html

  for (const [key, value] of Object.entries(placeholderReplacements)) {
    newHtml = newHtml.replace(new RegExp(key, 'g'), value)
  }

  return newHtml
}

export {
  replacePlaceholdersInString,
}
