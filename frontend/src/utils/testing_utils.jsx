import {render} from "@testing-library/react";
import {BrowserRouter} from "react-router";

const BrowserRouterWithFlags = ({children}) => {
  return (
    <BrowserRouter future={{
      v7_relativeSplatPath: true,
      v7_startTransition: true,
    }}>
      {children}
    </BrowserRouter>
  )
}

const renderWithRouter = (ui, {route = '/'} = {}) => {
  window.history.pushState({}, 'Test page', route)
  return render(ui, {wrapper: BrowserRouterWithFlags})
}

export {
  renderWithRouter,
}
