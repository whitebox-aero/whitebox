const getClasses = (...classes) => {
  return classes
      .filter(Boolean)
      .map((c) => c.trim())
      .join(' ')
}

export {
  getClasses,
}
