const isAssetVideo = (filepath) => {
  return filepath.endsWith('.mp4')
}

export {
  isAssetVideo,
}
