const config = {
  // Environment configuration from the build takes precedence
  ...import.meta.env,
  // and environment configuration from the window object is used as an override
  ...window.env,
}

export {
  config,
}
