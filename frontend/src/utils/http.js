import API from "../services/api";

const buildStaticUrl = (path) => {
  return `${API.baseUrl}${path}`
}

export {
  buildStaticUrl,
}
