import API from "../services/api.js";

const WhiteboxPlugins = {
  plugins: [],

  registerPlugin(module) {
    const name = module.name

    if (this.getPluginByName(name)) {
      throw new Error(`Plugin ${name} already registered`)
    }

    this.plugins.push(module)
    console.debug(`Plugin "${name}" registered successfully!`)

    if (module.init) {
      module.init({
        sockets: window.whiteboxSockets
      })
    }
  },

  getPluginByName(name) {
    return this.plugins.find((plugin) => plugin.name === name)
  },
}

const WhiteboxSockets = {
  // Socket management
  sockets: {},

  getSocket(socketName, create = true) {
    if (!this.sockets[socketName]) {
      if (!create) {
        throw new Error(`Socket ${socketName} not found`)
      }

      console.log('Creating Whitebox socket', socketName)
      const socketUrl = API.getUrl(`/ws/${socketName}/`)
      this.sockets[socketName] = new WebSocket(socketUrl)
    }

    return this.sockets[socketName]
  },

  addEventListener(socketName, eventName, callback) {
    const socket = this.getSocket(socketName)
    socket.addEventListener(eventName, callback)
  }
}

const WhiteboxExtensions = {
  extensions: [],

  register(extension) {
    const name = extension.name

    if (this.getByName(name)) {
      throw new Error(`Extension ${name} already registered`)
    }

    this.extensions.push(extension)
    console.debug(`Extension "${name}" registered successfully!`)
  },

  getByName(name) {
    return this.extensions.find((extension) => extension.name === name)
  },
}

const WhiteboxAPI = {
  plugins: WhiteboxPlugins,
  sockets: WhiteboxSockets,
  extensions: WhiteboxExtensions,

  apiUrl: API.baseUrl,
}

export default WhiteboxAPI
