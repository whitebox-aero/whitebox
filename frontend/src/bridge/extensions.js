class NotImplementedError extends Error {
  constructor(methodName) {
    super('Method not implemented: ' + methodName);
  }
}

class BaseExtension {
  constructor(name) {
    this.name = name;
  }
}

// eslint configuration will raise errors for the following class because it is
// not used in this codebase, but rather in external plugins that extend it
/* eslint-disable no-unused-vars */
class MapExtension extends BaseExtension {
  constructor(name) {
    super(name);
  }

  render({ mapContainer }) {
    throw new NotImplementedError('render');
  }

  setWhiteboxMarker({ lat, lon }) {
    throw new NotImplementedError('setWhiteboxMarker');
  }

  setWhiteboxLocation({ lat, lon }) {
    throw new NotImplementedError('updateGPSLocation');
  }
}

export default {
  BaseExtension,
  MapExtension,
  NotImplementedError,
}
