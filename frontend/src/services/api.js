import axios from "axios";
import {config} from "../utils/config";

const hostname =
    config.VITE_API_HOST || window.location.hostname

const port =
    config.VITE_API_PORT || 8000

const baseUrl = 'http://' + hostname + ':' + port

const client = axios.create({
  baseURL: baseUrl,
})

// region helpers

const getUrl = (path) => `${baseUrl}${path}`

const isServerError = (error) =>
    error.code === 'ERR_NETWORK' || error.response.status >= 500

// endregion helpers

// region devices

const getDevices = () => {
  return client.get('/api/devices/');
}

const addDevice = (device) => {
  return client.post('/api/devices/', device);
}

const getSupportedDevices = () => {
  return client.get('/api/devices/supported-devices/');
}

// endregion devices

// region providers

const getProvider = (capability) => {
  const url = getUrl(`/providers/${capability}/`)
  return client.get(url)
}

// endregion providers

export default {
  baseUrl,
  client,

  getUrl,
  isServerError,

  devices: {
    getDevices,
    addDevice,
    getSupportedDevices,
  },

  providers: {
    getProvider,
  },
}
