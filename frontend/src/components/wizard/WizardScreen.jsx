import {useEffect} from "react";
import useDevicesStore from "../../stores/devices";
import useDeviceWizardStore from "../../stores/device_wizard";
import WizardPageDeviceList from "./WizardPageDeviceList";
import WizardPageDeviceSelection from "./WizardPageDeviceSelection";
import WizardPageDeviceConnection from "./WizardPageDeviceConnection";

const Wizard = () => {
  // Use a renderTarget to prevent the wizard screen re-rendering in case
  // a page transition keeps the same parent wizard component in use
  const renderTarget = useDeviceWizardStore((state) => state.getRenderTarget())

  if (renderTarget === 'device-selection')
    return <WizardPageDeviceSelection />

  if (renderTarget === 'device-connection')
    return <WizardPageDeviceConnection />

  return <WizardPageDeviceList />
}

const WizardScreen = () => {
  const fetchDevices = useDevicesStore((state) => state.fetchDevices)
  const fetchSupportedDevices =
      useDeviceWizardStore((state) => state.fetchSupportedDevices)

  useEffect(() => {
    fetchSupportedDevices()
    fetchDevices()
  })

  return (
      <Wizard />
  )
}

export default WizardScreen
