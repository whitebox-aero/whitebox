import ProviderRenderer from "../common/bridge/ProviderRenderer.jsx";

const Map = () => {
  return (
      <ProviderRenderer
          targetCapability="map"
          className="c_map w-dvw h-full" />
  )
}

export default Map
