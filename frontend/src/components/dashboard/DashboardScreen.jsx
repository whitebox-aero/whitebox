import TopNav from "../common/scaffolding/TopNav";
import Map from "./Map";

const MapArea = () => {
  return (
      <div className="c_map_area w-dvw h-full">
        <Map />
      </div>
  )
}

const Screen = () => {
  return (
      <div className="h-dvh flex flex-col">
        <TopNav />
        <MapArea />
      </div>
  )
}

const DashboardScreen = () => {
  // Initialize any screen-wide states & effects here
  return (
      <Screen />
  )
}

export default DashboardScreen
