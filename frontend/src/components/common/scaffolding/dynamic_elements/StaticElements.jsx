import {getGenericClassNames} from "./utils";
import {buildStaticUrl} from "../../../../utils/http";
import {getClasses} from "../../../../utils/components";
import {ControlBar, LoadingSpinner, Player} from "video-react";

const DEText = ({config, ...props}) => {
  const classes = getGenericClassNames({config, props})

  return (
      <p className={getClasses(...classes)}>
        {config.text}
      </p>
  )
}

const DEImage = ({config, ...props}) => {
  const classes = getGenericClassNames({config, props})

  const outerClassName = getClasses(
    'wizard-media-container',
    ...classes,
  )

  const src = buildStaticUrl(config.url)

  return (
      <div className={outerClassName}>
        <img src={src} alt="" />
      </div>
  )
}


const DEVideo = ({config, ...props}) => {
  const classes = getGenericClassNames({config, props})

  const src = buildStaticUrl(config.url)

  const outerClassName = getClasses(
    'wizard-media-container',
    ...classes,
  )

  return (
      <div key={src}
           className={outerClassName}>
        <Player autoPlay={true}
                muted={true}
                isActive={false}
                loop={true}
            /* Make sure we don't re-render when a page changes, but the
               video stays the same */
                key={src}
        >
          <source src={src}
                  type="video/mp4" />

          <LoadingSpinner />
          <ControlBar autoHide={true} autoHideTime={2000} />
        </Player>
      </div>
  )
}

const staticType2ComponentMap = {
  text: DEText,
  image: DEImage,
  video: DEVideo,
}

export {
  DEText,
  DEImage,
  DEVideo,
  staticType2ComponentMap,
}
