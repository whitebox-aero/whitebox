import {staticType2ComponentMap} from "./StaticElements";
import {genericType2ComponentMap} from "./GenericElements";
import {formType2ComponentMap} from "./FieldElements";
// These may invoke anything from the above, so make sure they are always
// imported last to prevent circular dependencies
import {wizardType2ComponentMap} from "./WizardElements";

const type2ComponentMap = {
  // Default
  ...genericType2ComponentMap,
  ...staticType2ComponentMap,
  ...formType2ComponentMap,
  // Feature-specific
  ...wizardType2ComponentMap,
}

export default type2ComponentMap
