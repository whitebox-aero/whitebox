import type2ComponentMap from "./type2ComponentMap";

const getGenericClassNames = ({config, props = null}) => {
  const classes = [
      'c_generic_element',
  ]

  if (config?.style?.color)
    classes.push(`text-${config.style.color}`)

  if (config?.style?.size)
    classes.push(`text-${config.style.size}`)

  if (config?.style?.font_weight)
    classes.push(`font-${config.style.font_weight}`)

  if (config?.style?.font_height)
    classes.push(`leading-${config.style.font_height}`)

  if (config?.style?.classes)
    classes.push(...config.style.classes)

  if (props && props.className)
    classes.push(props.className)

  return classes
}

const generateChildren = (parentConfig) => {
  const childrenConfig = parentConfig?.config?.children

  if (!childrenConfig)
    return

  return childrenConfig.map((element, index) => {
    const Component = type2ComponentMap[element.type]
    return <Component key={index} config={element.config} />
  })
}

const generateDynamicElement = ({config, index, ...props}) => {
  const children = generateChildren(config)

  const Component = type2ComponentMap[config.type]

  return (
      <Component key={index} config={config.config} {...props}>
        {children}
      </Component>
  )
}

export {
  getGenericClassNames,
  generateDynamicElement,
}
