import {getGenericClassNames} from "./utils";
import {getClasses} from "../../../../utils/components";

const DEBlock = ({ config, children, ...props }) => {
  const classes = getGenericClassNames({config, props})

  return (
      <div className={getClasses(...classes)}>
        {children}
      </div>
  )
}

const genericType2ComponentMap = {
  block: DEBlock,
}

export {
  DEBlock,
  genericType2ComponentMap,
}
