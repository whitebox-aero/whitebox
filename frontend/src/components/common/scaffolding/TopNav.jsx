import {useNavigate} from "react-router";
import {PrimaryButton, TertiaryButton} from "../Button";
import Logo from "../Logo";

const TopNavActions  = () => {
  const navigate = useNavigate()
  const deviceWizardButton = (
      <TertiaryButton text="Device wizard"
                      onClick={() => navigate('../')} />
  )

  const startFlightButton = (
      <PrimaryButton text="Start flight"
                     className="font-semibold" />
  )

  return (
      <div className="flex items-center gap-6">
        {deviceWizardButton}
        {startFlightButton}
      </div>
  )
}

const TopNav = () => {
  return (
      <div className="c_topnav
                      flex py-4 px-6 justify-between items-center
                      border-b border-borders-default">
        <Logo />
        <TopNavActions />
      </div>
  )
}

export default TopNav
