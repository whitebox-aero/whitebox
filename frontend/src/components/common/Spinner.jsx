import IconSpinner from "../../assets/icons/spinner.svg?react";
import {getClasses} from "../../utils/components.js";

const Spinner = ({className, ...props}) => {
  const classes = getClasses('animate-spin', className)
  return (
      <IconSpinner
          className={classes}
          {...props} />
  )
}

export default Spinner
