import {useEffect, useState} from "react";
import API from "../../../services/api.js";
import Spinner from "../Spinner.jsx";
import DynamicContentRenderer from "./DynamicContentRenderer.jsx";

const ProviderRenderer = (
    {
      targetCapability,
      tag: Tag = "div",
      noReplace = false,
      includeSlots = false,
      ...props
    }
) => {
  const [provider, setProvider] = useState(null)

  // Kick off provider info fetching
  useEffect(() => {
    const fetchProvider = async () => {
      const provider = await API.providers.getProvider(targetCapability)
      const providerData = provider.data

      setProvider(providerData)
    }
    fetchProvider()
  }, [targetCapability])

  if (provider === null) {
    return <Spinner />
  }

  const elementId = `slot-${targetCapability}`

  return (
      <DynamicContentRenderer
          html={provider.template}
          css={provider.css}
          js={provider.js}
          tag={Tag}
          noReplace={noReplace}
          includeSlots={includeSlots}
          id={elementId}
          {...props}
      />
  )
}

export default ProviderRenderer
