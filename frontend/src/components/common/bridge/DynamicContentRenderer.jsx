import {useEffect, useRef, useState} from "react";
import {createPortal} from "react-dom";
import whiteboxCssLocation from '../../../index.css?url';
import {replacePlaceholdersInString} from "../../../utils/bridge.js";
import {generateDynamicElement} from "../scaffolding/dynamic_elements/utils.jsx";

const prepareHTML = (html, css, noReplace, isShadowDOM = false) => {
  // Prepare CSS if provided
  const prepend = []

  // In case when the Shadow DOM is used, we need to prepend the global styling
  // to the HTML. This is necessary because the Shadow DOM is isolated from the
  // rest of the page, and the global styling would not be applied otherwise.
  //
  // `whiteboxCssLocation` may be empty during test runs, so we're adding a
  // check here to prevent the error
  if (isShadowDOM && whiteboxCssLocation) {
    prepend.push(
        `<link rel="stylesheet" type="text/css" href="${whiteboxCssLocation}">`
    )
  }

  css?.forEach((css) => {
    prepend.push(`<link rel="stylesheet" type="text/css" href="${css}">`)
  })

  // Prepend the CSS to the HTML
  html = prepend.join('') + html

  // Replace the placeholders in the resulting HTML (i.e. URL hosts)
  if (!noReplace) {
    html = replacePlaceholdersInString(html)
  }

  return html
}

const loadScripts = (rootElement, js) => {
  if (!rootElement || !js) return

  const jsToLoad = [...js]

  const loadNextJs = () => {
    if (jsToLoad.length === 0) return

    const nextSrc = jsToLoad.shift()
    const preparedSrc = replacePlaceholdersInString(nextSrc)

    const script = document.createElement("script")
    if (preparedSrc.endsWith(".mjs")) {
      script.type = "module"
    }
    script.src = preparedSrc

    script.onload = loadNextJs
    script.onerror = () => console.error(`Failed to load: ${preparedSrc}`)

    rootElement.appendChild(script)
  }

  loadNextJs()
}

const DynamicContentRenderer = (
    {
      html,
      css = null,
      js = null,
      tag: Tag = "div",
      noReplace = false,
      includeSlots = false,
      useShadowDOM = false,
      ...props
    }
) => {
  const containerRef = useRef(null)
  const shadowRootRef = useRef(null)
  // Depending on the `useShadowDOM` prop, we'll either render the content
  // directly into the container or into a Shadow DOM. The `actualRef` will
  // always point to the actual container element, that will be the root of the
  // rendered component (either the container itself or the Shadow DOM root)
  const [actualRef, setActualRef] = useState(null)
  const [slotElements, setSlotElements] = useState({})

  // Prepare the container that'll be used for rendering the content
  useEffect(() => {
    // If the Shadow DOM is not used, we'll be rendering the content directly
    // into the container
    if (!useShadowDOM) {
      setActualRef(containerRef)
      return
    }

    // Otherwise, create the Shadow DOM and set the actual ref to the Shadow DOM
    setActualRef(shadowRootRef)

    if (containerRef.current && !shadowRootRef.current) {
      shadowRootRef.current = containerRef.current.attachShadow({ mode: "open" })
    }
  }, [useShadowDOM])

  useEffect(() => {
    if (!(actualRef?.current)) {
      // The actual ref is not ready yet
      return
    }

    // Prepare the HTML content and inject it.
    //
    // If you are making other adjustments here, make sure to test it thoroughly
    // with slots & portals. We are using `innerHTML` here to render the page,
    // which is technically a bypass of the React's rendering system.
    //
    // Because of the `dangerouslySetInnerHTML`, React should already be aware
    // and should not try to re-render, but it's still possible that the content
    // re-render is triggered by other means (e.g. by changing the state).
    //
    // e.g. slots could render properly, then this runs again and removes
    // them (by inserting the default HTML here). When testing this, test it
    // with both the development server (runs all effects twice, by design) and
    // with the production build (runs all effects just once)
    actualRef.current.innerHTML = prepareHTML(
        html,
        css,
        noReplace,
        useShadowDOM,
      )

    // Script are however loaded here, in all the cases
    loadScripts(actualRef.current, js)
  }, [html, css, js, noReplace, useShadowDOM, actualRef])

  // If there are any slots, create their elements to be injected
  useEffect(() => {
    if (!includeSlots || !actualRef?.current) return

    const slots = actualRef.current.querySelectorAll("slot")

    setSlotElements((prev) => {
      const updatedSlots = { ...prev }

      slots.forEach((slot) => {
        const slotName = slot.getAttribute("name")

        if (!updatedSlots[slotName]) {
          updatedSlots[slotName] = generateDynamicElement({
            config: { type: slotName },
          })
        }
      })

      return updatedSlots
    })

    // Clear slotElements on unmount to prevent stale portal references
    return () => setSlotElements({})
  }, [html, includeSlots, actualRef])

  const dynamicElementPortals = Object.entries(slotElements).map(
      ([slotName, element]) => {
        const slotTarget = actualRef?.current?.querySelector(
            `slot[name="${slotName}"]`
        )

        return slotTarget && createPortal(element, slotTarget)
      }
  )

  // As we need to inject and later change the HTML content directly, we need to
  // use `dangerouslySetInnerHTML` to tell React not to try re-rendering the
  // content. Initially, it is just rendering the empty string, and the content
  // is injected through the `useEffect` hook above
  return (
      <>
        <Tag ref={containerRef}
             dangerouslySetInnerHTML={{ __html: '' }}
             {...props} />
        {dynamicElementPortals}
      </>
  )
}

export default DynamicContentRenderer
