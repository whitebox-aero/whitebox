const Logo = () => {
  return (
      <div className="c_logo
                      inline-flex h-9 py-2 px-3 items-center gap-1
                      flex-shrink-0 bg-surface-primary rounded-xl text-white">
        <p className="text-[1.625rem] font-bold">
          Whitebox
        </p>
      </div>
  )
}

export default Logo
