import IconCancel from "../../assets/icons/cancel.svg?react";
import IconInfo from "../../assets/icons/info.svg?react";
import IconCheckCircle from "../../assets/icons/check_circle.svg?react";
import {getClasses} from "../../utils/components";

const SnackBar = (
    {
      leftIcon = null,
      message,
      stretch = false,
      ...props
    }
) => {
  const classes = getClasses(
    'snackbar',
    stretch && 'self-stretch',
    props.className,
  )

  return (
      <div className={classes}>
        {leftIcon}
        <p className="flex-1 text-white-full-emphasis text-sm font-semibold">
          {message}
        </p>
      </div>
  )
}

const SnackBarSuccess = ({ className, ...props }) =>
    <SnackBar className={getClasses(className, "snackbar-success")}
              leftIcon={
                <IconCheckCircle />
              }
              {...props} />

const SnackBarError = ({ className, ...props }) =>
    <SnackBar className={getClasses(className, "snackbar-error")}
              leftIcon={
                <IconCancel />
              }
              {...props} />

const SnackBarInfo = ({ className, ...props }) =>
    <SnackBar className={getClasses(className, "snackbar-info")}
              leftIcon={
                <IconInfo className="fill-white-full-emphasis" />
              }
              {...props} />

export {
    SnackBar,
    SnackBarSuccess,
    SnackBarError,
    SnackBarInfo,
}
