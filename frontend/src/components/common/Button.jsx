import {getClasses} from "../../utils/components";
import Spinner from "./Spinner.jsx";

const Button = (
    {
      typeClass,
      text = null,
      onClick = null,
      leftIcon = null,
      rightIcon = null,
      className = null,
      isLoading = false,
      ...props
    }
) => {
  let paddingClasses = 'px-6 py-3'
  if ((leftIcon || rightIcon) && !text) {
    paddingClasses = 'px-3 py-3'
  }

  const computedClassName = getClasses(
    'btn',
    typeClass,
    className,
    paddingClasses,
  )

  return (
      <button
          className={computedClassName}
          onClick={onClick}
          {...props}>

        <Spinner className={'absolute origin-center'
                            + (isLoading ? ' block' : ' hidden')} />

        <div className={isLoading ? 'invisible' : undefined}>
          {leftIcon &&
            <div className="icon-container">
              {leftIcon}
            </div>
          }

          {text}

          {rightIcon &&
            <div className="icon-container">
              {rightIcon}
            </div>
          }
        </div>
      </button>
  )
}

const PrimaryButton = (props) =>
    <Button typeClass="btn-primary" {...props} />

const SecondaryButton = (props) =>
    <Button typeClass="btn-secondary" {...props} />

const TertiaryButton = (props) =>
    <Button typeClass="btn-tertiary" {...props} />

export {
  Button,
  PrimaryButton,
  SecondaryButton,
  TertiaryButton,
}
