import useToastsStore from "../../stores/toasts";
import {SnackBarError, SnackBarInfo, SnackBarSuccess} from "./SnackBar";

const toastType2snackBarType = {
  success: SnackBarSuccess,
  error: SnackBarError,
  info: SnackBarInfo,
}

const Toast = ({toast}) => {
  const TargetSnackBar = toastType2snackBarType[toast.type]

  return (
    <TargetSnackBar message={toast.message} />
  )
}

const SnackBarContainer = () => {
  const toasts = useToastsStore((state) => state.toasts)

  return (
    <div className="c_toast_container">
      {toasts.map((toast) => (
        <Toast key={toast.id} toast={toast} />
      ))}
    </div>
  )
}

export default SnackBarContainer
