import { defineConfig, devices } from '@playwright/test';

const frontendBaseUrl = process.env.E2E_TEST_URL || 'http://localhost:3000'

export default defineConfig({
  name: 'End-to-end test suite',
  testDir: 'tests/e2e',

  // E2E test suite will be changing data in the database, and it will be
  // running against a running server (and later, device), which makes any
  // test parallelization complicated. For this reason, we will run the tests
  // sequentially. This setting will not affect the BrowserStack runs
  fullyParallel: false,
  workers: 1,

  // These will be used only when running locally, when running on BrowserStack,
  // the device configuration from `browserstack.yml` will prevail
  projects: [
    {
      name: 'Desktop Chrome',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'Desktop Firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'Desktop Safari',
      use: { ...devices['Desktop Safari'] },
    },
  ],

  use: {
    baseURL: frontendBaseUrl,
    // You can uncomment this to run tests in headful mode
    // headless: false,
  },
});
