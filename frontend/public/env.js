// This file exists only to ensure there are no network-loading errors during
// development. A replacement file will be generated as an entrypoint step when
// a frontend Docker container is launched.
window.env = {}
