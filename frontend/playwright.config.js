import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  name: 'Frontend integration test suite',
  testDir: 'tests/integration',
  projects: [
    {
      name: 'Desktop Chrome',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'Desktop Firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'Desktop Safari',
      use: { ...devices['Desktop Safari'] },
    },
  ],
  use: {
    baseURL: 'http://localhost:3000',
    // You can uncomment this to run tests in headful mode
    // headless: false,
  },
  webServer: {
    command: 'npm run preview:ci',
    url: 'http://127.0.0.1:3000',
    reuseExistingServer: !process.env.CI,
  },
});
