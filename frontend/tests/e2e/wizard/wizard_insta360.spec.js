import {test, expect} from '@playwright/test';

test.describe('Wizard - Insta360', () => {
  test('should be able to add a device', async ({page}) => {
    // We have to ensure that the response has arrived before we count the
    // initial number of devices
    const responseSentinelBefore = page.waitForResponse('**/api/devices/')

    // Open Wizard
    await page.goto('/')

    // Ensure we're on Wizard screen
    const wizard = await page.locator('div.c_modal_page')
    await expect(wizard).toContainText(/Let's get you set up/)

    // Wait for the response to arrive & be rendered
    await responseSentinelBefore
    await page.waitForTimeout(100)

    // Note down current number of connected devices
    // (it will have + 2, the ones indicating whitebox itself & the user)
    const initialDeviceCount = await page.locator('.c_device_connection').count()

    // Click the add device button
    const addDeviceButton = wizard.getByRole('button', {name: 'Add device'})
    await addDeviceButton.click()

    // Ensure we're on Device selection
    await expect(wizard).toContainText(/Select a device/)

    // Select Insta360 X4
    const x4 = await wizard
        .locator('div.c_device_option')
        .filter({hasText: 'Insta360 X4'})

    await x4.click()

    // Ensure we're on Device connection
    await expect(wizard).toContainText(/Device connection checklist/)

    // Iterate through the steps with `Next` button
    const stepCount = await wizard.locator('.c_wizard_step_indicator_item').count()
    const stepsToComplete = stepCount - 1

    for (let i = 0; i < stepsToComplete; i++) {
      const nextButton = wizard.locator('button.btn-primary')
      await nextButton.click()
    }

    // Ensure we're on the page with connection form
    const connectionForm = await wizard.locator('.connection_form')
    await expect(connectionForm).toHaveCount(1)

    // Fill the form
    const testData = {
      'ssid': 'hello world',
      'password': '1337-super-secure',
    }
    for (const [key, value] of Object.entries(testData)) {
      const input = await connectionForm.locator(`input[name="${key}"]`)
      await input.fill(value)
      await input.blur()
    }

    // Prepare sentinel for after we create a new connection, to ensure that
    // it gets added to the wizard's list
    const responseSentinelCreate = page.waitForResponse(response =>
        /.*\/api\/devices\/$/.test(response.url())
        && response.request().method() === 'POST'
    )

    const responseSentinelAfter = page.waitForResponse(response =>
        /.*\/api\/devices\/$/.test(response.url())
        && response.request().method() === 'GET'
    )
    // const responseSentinelAfter = page.waitForResponse('**/api/devices/')

    // Submit the form
    const connectButton = await wizard.locator('button.btn-primary')
    await connectButton.click()

    // Wait for device to be created
    const responseCreate = await responseSentinelCreate
    const responseData = await responseCreate.json()
    const createdDeviceName = responseData.name

    // Wait until we're back on the wizard home screen
    await expect(wizard).toContainText(/Let's get you set up/)

    // Wait for the list response to arrive & be rendered
    await responseSentinelAfter
    await page.waitForTimeout(100)

    // Ensure we have one more device connected
    const currentDeviceCount = await page.locator('.c_device_connection').count()
    // As multiple tests can run in parallel, we can't be sure about the exact
    // number of devices, but we can be sure that it's at least one more than
    // the initial count
    await expect(currentDeviceCount).toBeGreaterThan(initialDeviceCount)

    // Ensure the new device is in the list
    const newDevice = await page.locator(
        `.c_device_connection:has-text("${createdDeviceName}")`
    )
    await expect(newDevice).toBeVisible()
  })
})
