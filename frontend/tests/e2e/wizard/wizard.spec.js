import {test, expect} from '@playwright/test';

test.describe('Wizard', () => {
  let wizard
  let responseSupportedDevicesPromise

  test.beforeEach(async ({page}) => {
    // Intercept supported devices request
    responseSupportedDevicesPromise = page.waitForResponse(
        '**/api/devices/supported-devices/'
    )

    // Load wizard
    await page.goto('/')

    wizard = await page.locator('div.c_modal_page')
  })

  test('first page should be loaded with the correct content', async ({page}) => {
    await responseSupportedDevicesPromise

    // Check if the page title is correct
    await expect(page).toHaveTitle(/Whitebox/);
    await expect(wizard).toContainText(/Let's get you set up/)

    // Check if the buttons are present
    const nextButton = wizard.getByRole('button', {name: 'Add device'})
    const skipButton = wizard.getByRole('button', {name: 'Skip'})
    await expect(nextButton).toHaveCount(1)
    await expect(skipButton).toHaveCount(1)
  })

  test.describe('Device selection', () => {
    let supportedDevices

    test.beforeEach(async () => {
      // Load the response so we can use it in the tests
      const responseSupportedDevices = await responseSupportedDevicesPromise
      expect(responseSupportedDevices.ok()).toBeTruthy()

      const supportedDevicesJson = await responseSupportedDevices.json()
      supportedDevices = supportedDevicesJson.supported_devices

      // Navigate to device selection page
      const nextButton = wizard.getByRole('button', {name: 'Add device'})
      await nextButton.click()

      await expect(wizard).toContainText(/Select a device/)
    })

    test(
        'should allow user to search for a device',
        async () => {
          const searchInput = wizard.getByPlaceholder('Search for devices...')
          await expect(searchInput).toHaveCount(1)

          const deviceOptions = wizard.locator('div.c_device_option')
          const initialDeviceCount = await deviceOptions.count()
          expect(initialDeviceCount).toEqual(supportedDevices.length)

          const firstDeviceName = supportedDevices[0].device_name

          // Check if filtering works
          await searchInput.fill(firstDeviceName)
          await expect(deviceOptions).toHaveCount(1)

          // Check if clearing the filter works
          await searchInput.fill('')
          await expect(deviceOptions).toHaveCount(initialDeviceCount)

          // Check if invalid search shows no devices
          await searchInput.fill('==!invalid098712387651097823')
          await expect(deviceOptions).toHaveCount(0)
    })

    test(
        'should allow user to select a device',
        async () => {
          const deviceOptions = wizard.locator('div.c_device_option')

          let x4option

          // Find the X4 device option
          for (const option of await deviceOptions.all()) {
            const deviceName = await option.locator('p').textContent()

            if (/X4/.test(deviceName)) {
              x4option = option
              break
            }
          }

          await expect(x4option).toBeDefined()
          await x4option.click()

          // Ensure we were navigated to Device connection
          await expect(deviceOptions).toHaveCount(0)
          await expect(wizard).toContainText(/Device connection checklist/)
    })
  })

  test.describe('Device connection', () => {
    let supportedDevices
    let testDeviceTarget

    test.beforeEach(async () => {
      const supportedDevicesResponse = await responseSupportedDevicesPromise
      const supportedDevicesJson = await supportedDevicesResponse.json()
      supportedDevices = supportedDevicesJson.supported_devices

      // Navigate to device selection page
      const nextButton = wizard.getByRole('button', {name: 'Add device'})
      await nextButton.click()

      // Select first device from the list
      const firstDevice = supportedDevices[0]
      const firstDeviceName = firstDevice.device_name
      testDeviceTarget = firstDevice

      const deviceOptions = wizard.locator('div.c_device_option')
      for (const option of await deviceOptions.all()) {
        const deviceName = await option.locator('p').textContent()

        if (deviceName === firstDeviceName) {
          await option.click()
          break
        }
      }

      await expect(wizard).toContainText(/Device connection checklist/)
      await expect(wizard).toContainText(/Complete the checklist/)
    })

    test('should display step indicator', async () => {
      const stepIndicator = await wizard.locator('.c_wizard_step_indicator')
      const indicatorItems = await stepIndicator.locator(
          '.c_wizard_step_indicator_item',
      )

      const expectedStepCount = testDeviceTarget.wizard_steps.length
      await expect(indicatorItems).toHaveCount(expectedStepCount)
    })

    test('should be able to navigate through steps', async () => {
      const stepIndicator = await wizard.locator('.c_wizard_step_indicator')
      const indicatorItems = await stepIndicator.locator(
          '.c_wizard_step_indicator_item',
      )

      const expectedStepCount = testDeviceTarget.wizard_steps.length
      for (let i = 0; i < expectedStepCount; i++) {
        const targetItem = await indicatorItems.nth(i)
        await targetItem.click()

        const afterClickSteps = await wizard.locator('.c_wizard_step_indicator_item')

        for (let j = 0; j < afterClickSteps.length; j++) {
          const specificStep = await afterClickSteps.nth(j)

          if (j === i) {
            await expect(specificStep).toHaveClass('cursor-pointer')
          } else {
            await expect(specificStep).not.toHaveClass('cursor-pointer')
          }
        }
      }
    })

    test('should display the connection form', async () => {
      // First go to the last step
      const stepIndicator = await wizard.locator('.c_wizard_step_indicator')
      await stepIndicator
          .locator('.c_wizard_step_indicator_item')
          .last()
          .click()

      // Check if all the fields are present
      const connectionForm = await wizard.locator('.connection_form')
      await expect(connectionForm).toHaveCount(1)

      // Check if all the fields are present
      const firstConnectionConfig =
          Object.values(testDeviceTarget.connection_types)[0]

      const expectedFields = firstConnectionConfig.fields

      // Check if all the fields are present and render correctly
      for (const [fieldName, fieldConfig] of Object.entries(expectedFields)) {
        const fieldElement =
            await connectionForm.locator(`input[name="${fieldName}"]`)

        await expect(fieldElement).toHaveCount(1)
        await expect(fieldElement).toHaveAttribute('type', fieldConfig.type)
        await expect(fieldElement).toHaveAttribute('placeholder', fieldConfig.name)
      }
    })
  })
})
