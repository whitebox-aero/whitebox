const fixtureProviderMap = {
    "template": "<link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.9.4/dist/leaflet.css\" />\n<script src=\"https://unpkg.com/leaflet@1.9.4/dist/leaflet.js\"></script>\n\n\n\n<div id=\"map\" class=\"embedded\"></div>\n",
    "css": [
        "%API_URL%/static/whitebox_plugin_gps_display/whitebox_plugin_gps_display.css"
    ],
    "js": [
        "https://unpkg.com/leaflet@1.9.4/dist/leaflet.js",
        "https://cdn.jsdelivr.net/npm/leaflet-rotatedmarker@0.2.0/leaflet.rotatedMarker.min.js",
        "%API_URL%/static/whitebox_plugin_gps_display/whitebox_plugin_gps_display.mjs"
    ]
}

const mockProviderMap = async (page) => {
  await page.route('**/providers/map/', route => {
    route.fulfill({
      status: 200,
      body: JSON.stringify(fixtureProviderMap)
    });
  })
}

export {
  fixtureProviderMap,
  mockProviderMap,
}
