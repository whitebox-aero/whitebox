import {test, expect} from '@playwright/test';

test('should load the homepage and display the correct content', async ({page}) => {
  await page.goto('/');

  // Check if the page title is correct
  await expect(page).toHaveTitle(/Whitebox/);
});
