import {test, expect} from '@playwright/test';
import * as deviceFixtures from "../../../__fixtures__/devices";

test.describe('Wizard Device Selection', () => {
  let page
  let wizard

  test.beforeEach(async ({page: newPage}) => {
    page = newPage

    await deviceFixtures.mockSupportedDevices(page)
    await deviceFixtures.mockDevicesListMultipleDevices(page)

    await page.goto('/')
    wizard = await page.locator('div.c_modal_page')
    await expect(wizard).toHaveCount(1)

    // First, ensure we navigated to device selection page
    const nextButton = wizard.getByRole('button', {name: 'Add device'})
    await expect(nextButton).toHaveCount(1)
    await nextButton.click()
  })

  test('should display the device selection page', async () => {
    await expect(wizard).toContainText(/Select a device/)
    await expect(wizard)
        .toContainText(/Select the type of device you want to connect/)
  })

  test('should display the all devices obtained from backend', async () => {
    const devices = await wizard.locator('div.c_device_option')

    const expectedDeviceCount =
        deviceFixtures
            .fixtureSupportedDevices
            .supported_devices
            .length

    await expect(devices).toHaveCount(expectedDeviceCount)
  })

  test('should allow searching for devices', async () => {
    const searchInput = await wizard.locator('input')

    const supportedDevices =
        deviceFixtures.fixtureSupportedDevices.supported_devices

    // Test with lowercase to ensure filtering is case-insensitive
    const targetDeviceName = supportedDevices[0].device_name.toLowerCase()

    // Check whether inputting the device name filters the devices
    await searchInput.fill(targetDeviceName)
    const filteredDevices = await wizard.locator('div.c_device_option')
    await expect(filteredDevices).toHaveCount(1)

    // Check whether clearing the input displays all the devices again
    await searchInput.clear('')
    const unfilteredDevices = await wizard.locator('div.c_device_option')
    await expect(unfilteredDevices).toHaveCount(supportedDevices.length)
  })
})
