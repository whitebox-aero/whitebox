import {test, expect} from '@playwright/test';
import * as deviceFixtures from "../../../__fixtures__/devices";

test.describe('Wizard Device List', () => {
  let page
  let wizard

  test.beforeEach(async ({page: newPage}) => {
    page = newPage
  });

  test.describe('always', () => {
    test.beforeEach(async () => {
      await page.goto('/');
      wizard = await page.locator('div.c_modal_page')
    })

    test('should display host devices', async () => {

      const deviceConnections =
          await wizard.locator('div.c_device_connection')

      const userDevice = await deviceConnections.nth(0)
      const hostDevice = await deviceConnections.nth(1)

      await expect(userDevice).toContainText("The screen you're looking at")
      await expect(hostDevice).toContainText("Whitebox #")
    });

    test('should display the buttons', async () => {
      const footer = await wizard.locator('div.c_modal_footer_buttons')
      const skip = await footer.locator('button').nth(0)
      const addDevice = await footer.locator('button').nth(1)

      await expect(skip).toContainText("Skip")
      await expect(addDevice).toContainText("Add device")
    })
  })

  test.describe('no devices', () => {
    test.beforeEach(async () => {
      // Ensure the API returns no devices for these, as there is no mechanism
      // for resetting the database
      await deviceFixtures.mockDevicesListNoDevices(page)

      await page.goto('/');
      wizard = await page.locator('div.c_modal_page')
    })

    test('should render the no devices banner', async () => {
      await expect(wizard).toHaveText(/No installed devices/)
      await expect(wizard).toHaveText(/Bringing devices onboard: What to know/)
    })
  })

  test.describe('with devices', () => {
    test.beforeEach(async () => {
      // Ensure the API returns multiple devices for these, as there is no mechanism
      // for resetting the database
      await deviceFixtures.mockDevicesListMultipleDevices(page)

      await page.goto('/');
      wizard = await page.locator('div.c_modal_page')
    })

    test('should render the devices', async () => {
      await expect(wizard).not.toHaveText(/No installed devices/)
      await expect(wizard).not.toHaveText(/Bringing devices onboard: What to know/)

      const deviceConnections =
          await wizard.locator('div.c_device_connection')

      const deviceCount = await deviceConnections.count()
      const listedDeviceCount = deviceCount - 2

      const expectedDeviceCount = deviceFixtures.fixtureMultipleDevices.length
      await expect(listedDeviceCount).toEqual(expectedDeviceCount)

      const expectedDeviceCountText =
          `Installed devices (${listedDeviceCount}/10)`
      await expect(wizard).toContainText(expectedDeviceCountText)
    })
  })
});
