import {test, expect} from '@playwright/test';
import * as deviceFixtures from "../../../__fixtures__/devices";
import {fixtureSupportedDeviceName2, fixtureSupportedDevices} from "../../../__fixtures__/devices";

test.describe('Wizard Device Connection', () => {
  let page
  let wizard

  const pushFooterButton = async (n) => {
    const wizardFooter = await wizard.locator('.c_modal_footer_buttons')
    const wizardFooterButtons = await wizardFooter.locator('button')
    const targetButton = await wizardFooterButtons.nth(n)
    await targetButton.click()
    await page.waitForTimeout(100)
  }

  const deviceTarget =
      fixtureSupportedDevices
          .supported_devices[0]

  const wizardSteps = deviceTarget.wizard_steps

  test.beforeEach(async ({page: newPage}) => {
    page = newPage

    await deviceFixtures.mockDevicesListMultipleDevices(page)
    await deviceFixtures.mockSupportedDevices(page)

    await page.goto('/')
    wizard = await page.locator('div.c_modal_page')
    await expect(wizard).toHaveCount(1)

    // First, ensure we navigated to device selection page
    const nextButton = wizard.getByRole('button', {name: 'Add device'})
    await expect(nextButton).toHaveCount(1)
    await nextButton.click()

    // and then to have selected a device
    const searchInput = await wizard.locator('input')
    await searchInput.fill(fixtureSupportedDeviceName2)

    const deviceOption = await wizard.locator('div.c_device_option')
    await expect(deviceOption).toHaveCount(1)
    await deviceOption.click()
  })

  test('should display the device connection page', async () => {
    await expect(wizard).toContainText(/Device connection checklist/)
    await expect(wizard)
        .toContainText(/Complete the checklist/)
  });

  // Run for every wizard step index separately
  Array.from(Array(wizardSteps.length - 1).keys()).forEach(targetStep => {
    test(`should change steps on step indicator item click (step ${targetStep})`, async () => {
      const stepIndicator = await wizard.locator('.c_wizard_step_indicator')
      const indicatorItems = await stepIndicator.locator(
          '.c_wizard_step_indicator_item',
      )

      const targetItem = await indicatorItems.nth(targetStep)

      await targetItem.click()

      const afterClickSteps = await wizard.locator('.c_wizard_step_indicator_item')

      for (let i = 0; i < afterClickSteps.length; i++) {
        const specificStep = await afterClickSteps.nth(i)

        if (i === targetStep) {
          await expect(specificStep).toHaveClass('cursor-pointer')
        } else {
          await expect(specificStep).not.toHaveClass('cursor-pointer')
        }
      }
    })
  })

  // TODO: TEST generating default fields
  //            connecting to device and it being displayed on page 1

  test.describe('connectivity', () => {
    test.beforeEach(async () => {
      // Navigate to last page
      for (let i = 0; i < 4; i++) {
        await pushFooterButton(1)
      }

      // Ensure we're on last step
      const renderedHeader = await wizard.locator('p.text-2xl.font-semibold')
      await expect(renderedHeader).toContainText('Wi-Fi: Enter details')
    })

    test('should contain a form with all the device-defined fields', async () => {
      const fieldDescriptors =
          deviceFixtures
              .fixtureSupportedDevices
              .supported_devices[0]
              .connection_types
              .wifi
              .fields

      const form = await wizard.locator('.connection_form')
      const fields = await form.locator('input')

      const expectedFieldCount = Object.keys(fieldDescriptors).length
      await expect(fields).toHaveCount(expectedFieldCount)

      for (let i = 0; i < expectedFieldCount; i++) {
        const field = await fields.nth(i)

        const targetDescriptor = Object.entries(fieldDescriptors)[i]
        const [fieldName, fieldInfo] = targetDescriptor

        await expect(field).toHaveAttribute('name', fieldName)
        await expect(field).toHaveAttribute('placeholder', fieldInfo.name)
      }
    })
  })
})
