import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import svgr from "vite-plugin-svgr";

// https://vite.dev/config/
export default defineConfig({
  plugins: [svgr(), react()],
  resolve: {
    alias: {
      '@': '/src',
    },
  },
  server: {
    port: 3000,
    sourcemap: true,
  },
  preview: {
    port: 3000,
  },
  test: {
    // mockReset: false,
    globals: true,
    environment: 'jsdom',
    setupFiles: './src/setupTests.js',
    sequence: {
      shuffle: false, // Ensures tests run in the order they are defined
      concurrent: false, // Disables parallel test execution
    },
    coverage: {
      reporter: ['text', 'text-summary'],
      thresholds: {
        lines: 90,
        functions: 90,
        branches: 90,
        statements: 90,
      },
      exclude: [
        '**/__mocks__/**',
        'App.jsx',
        'main.jsx',
        'reportWebVitals.js',
      ],
    },
  },
})
