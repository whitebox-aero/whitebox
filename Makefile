.PHONY: setup-udev setup-wifi

setup-udev:
	@echo "Copying udev rules to /etc/udev/rules.d"
	sudo curl -L -o /etc/udev/rules.d/99-uavionix.rules https://raw.githubusercontent.com/b3nn0/stratux/refs/heads/master/image/99-uavionix.rules
	sudo curl -L -o /etc/udev/rules.d/10-stratux.rules https://raw.githubusercontent.com/b3nn0/stratux/refs/heads/master/image/10-stratux.rules

	@echo "Reload udev rules"
	sudo udevadm control --reload-rules
	sudo udevadm trigger

setup-wifi:
	./bin/setup_wifi.sh
