from typing import Callable

import importlib
import threading
import logging
import os
from importlib import metadata

from django.conf import settings

from devices.manager import device_manager
from plugin.asset_manager import asset_manager
from plugin.exceptions import PluginError
from whitebox.events import event_emitter


logger = logging.getLogger(__name__)


class PluginManager:
    """
    Singleton class for managing plugins.

    Attributes:
        plugins: A list of loaded plugins.
        previously_discovered_plugins: A dictionary of previously discovered plugins.
        _lock: A lock to ensure thread safety.
    """

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(PluginManager, cls).__new__(cls)
            cls._instance.plugins = []
            cls._instance.previously_discovered_plugins = {}
            cls._instance._lock = threading.Lock()
        return cls._instance

    def _get_plugin_path(self, module_name):
        """
        Get the path of an installed plugin package.
        """

        try:
            module = importlib.import_module(module_name)
            return os.path.dirname(module.__file__)
        except ImportError:
            logger.error(f"Failed to import module {module_name}")
            return None

    def _get_discovered_plugins(self) -> dict:
        """
        Discover all plugins in the plugin directory.
        """

        discovered_plugins = {}

        for distribution in metadata.distributions():
            if distribution.metadata["Name"].startswith("whitebox-plugin-"):
                plugin_name = distribution.metadata["Name"].replace("-", "_")
                try:
                    module = importlib.import_module(f"{plugin_name}.{plugin_name}")
                    discovered_plugins[plugin_name] = module
                except ImportError as e:
                    logger.exception(
                        f"Failed to import plugin '{plugin_name}'. Error: {e}"
                    )

        return discovered_plugins

    def _load_plugin_template(self, module_name) -> None:
        """
        Add the plugin's template directory to the list of template directories.

        Parameters:
            module_name: The name of the plugin.
        """

        plugin_path = self._get_plugin_path(module_name)
        template_dir = os.path.join(plugin_path, "templates")
        if (
            os.path.exists(template_dir)
            and template_dir not in settings.TEMPLATES[0]["DIRS"]
        ):
            settings.TEMPLATES[0]["DIRS"].append(template_dir)

    def _unload_plugin_template(self, module_name) -> None:
        """
        Remove the plugin's template directory from the list of template directories.

        Parameters:
            module_name: The name of the plugin.
        """

        plugin_path = self._get_plugin_path(module_name)
        template_dir = os.path.join(plugin_path, "templates")
        if template_dir in settings.TEMPLATES[0]["DIRS"]:
            settings.TEMPLATES[0]["DIRS"].remove(template_dir)

    def _load_plugin_static(self, module_name) -> None:
        """
        Add the plugin's static directory to the list of static directories.

        Parameters:
            module_name: The name of the plugin.
        """

        plugin_path = self._get_plugin_path(module_name)
        static_dir = os.path.join(plugin_path, "static")
        if os.path.exists(static_dir) and static_dir not in settings.STATICFILES_DIRS:
            settings.STATICFILES_DIRS.append(static_dir)

    def _unload_plugin_static(self, module_name) -> None:
        """
        Remove the plugin's static directory from the list of static directories.

        Parameters:
            module_name: The name of the plugin.
        """

        plugin_path = self._get_plugin_path(module_name)
        static_dir = os.path.join(plugin_path, "static")
        if static_dir in settings.STATICFILES_DIRS:
            settings.STATICFILES_DIRS.remove(static_dir)

    def _load_plugin_device_classes(self, module) -> None:
        """
        Add the plugin's devices to the device manager.

        Parameters:
            module: The module containing the plugin class.
        """

        plugin_class = getattr(module, "plugin_class")
        plugin_instance = plugin_class()

        for device in plugin_instance.get_device_classes():
            device_manager.register_device(device.codename, device)

    def _unload_plugin_device_classes(self, module) -> None:
        """
        Remove the plugin's devices from the device manager.

        Parameters:
            module: The module containing the plugin class.
        """

        plugin_class = getattr(module, "plugin_class")
        plugin_instance = plugin_class()

        for device in plugin_instance.get_device_classes():
            device_manager.unregister_device(device.codename)

    def _load_plugin_class(self, module) -> None:
        """
        Load the plugin class from the module and add it to the list of loaded plugins.

        Parameters:
            module: The module containing the plugin class.
        """

        plugin_class = getattr(module, "plugin_class")
        plugin_instance = plugin_class()

        if not any(
            isinstance(plugin, type(plugin_instance)) for plugin in self.plugins
        ):
            self.plugins.append(plugin_instance)

    def _unload_plugin_class(self, module) -> None:
        """
        Remove the plugin class from the list of loaded plugins.

        Parameters:
            module: The module containing the plugin class.
        """

        plugin_class = getattr(module, "plugin_class")
        plugin_instance = plugin_class()
        remove_plugin = None

        for plugin in self.plugins:
            if isinstance(plugin, type(plugin_instance)):
                remove_plugin = plugin
                break

        if remove_plugin:
            self.plugins.remove(remove_plugin)

    def _load_plugin(self, module_name, module) -> None:
        """
        Load a plugin into the server.

        Parameters:
            module_name: The name of the plugin.
            module: The module containing the plugin class.
        """

        self._load_plugin_class(module)
        self._load_plugin_template(module_name)
        self._load_plugin_static(module_name)
        self._load_plugin_device_classes(module)

    def _unload_plugin(self, module_name, module) -> None:
        """
        Unload a plugin from the server.

        Parameters:
            module_name: The name of the plugin.
            module: The module containing the plugin class.
        """

        self._unload_plugin_class(module)
        self._unload_plugin_template(module_name)
        self._unload_plugin_static(module_name)
        self._unload_plugin_device_classes(module)

    def _get_plugins_to_unload(self, discovered_plugins) -> set:
        """
        Get a list of plugins that need to be unloaded.

        Parameters:
            discovered_plugins: A dictionary of newly discovered plugins.
        """

        discovered_plugin_names = set(discovered_plugins.keys())
        previously_discovered_plugin_names = set(
            self.previously_discovered_plugins.keys()
        )
        plugins_to_unload = previously_discovered_plugin_names - discovered_plugin_names

        return plugins_to_unload

    def discover_plugins(self) -> None:
        """
        Unload or load plugins based on the discovered plugins.
        """

        discovered_plugins = self._get_discovered_plugins()
        plugins_to_unload = self._get_plugins_to_unload(discovered_plugins)

        for plugin_name in plugins_to_unload:
            self._unload_plugin(
                plugin_name, self.previously_discovered_plugins[plugin_name]
            )

        for module_name, module in discovered_plugins.items():
            try:
                self._load_plugin(module_name, module)
            except (PluginError, Exception) as e:
                # TODO: Separate PluginError from other exceptions and emit
                #       messages to frontend
                logger.exception(f"Failed to load plugin: {module_name}")

        self.previously_discovered_plugins = discovered_plugins

    def get_plugins(self) -> list["Plugin"]:
        """
        Get all loaded plugins.
        """

        with self._lock:
            return self.plugins

    def get_plugin_names(self) -> list[str]:
        """
        Get the names of all loaded plugins.
        """

        with self._lock:
            return [plugin.name or plugin.__class__.__name__ for plugin in self.plugins]

    def get_plugin_for_capability(self, capability) -> "Plugin":
        """
        Get the plugin that provides the given capability.
        """

        for plugin in self.get_plugins():
            if capability in plugin.provides_capabilities:
                return plugin

        return None

    def register_event_callback(self, event_type: str, callback: Callable) -> None:
        """
        Register a callback for a specific event type.

        Parameters:
            event_type: The type of event to register the callback for.
            callback: The callback function to be called when the event is emitted.
        """

        event_emitter.append(event_type, callback)

    def unregister_event_callback(self, event_type: str, callback: Callable) -> None:
        """
        Unregister a callback for a specific event type.

        Parameters:
            event_type: The type of event to unregister the callback for.
            callback: The callback function to be removed.
        """

        event_emitter.remove(event_type, callback)

    async def emit_event(self, event_type: str, data: dict = None) -> None:
        """
        Propagate an event to all registered event callbacks.

        Parameters:
            event_type: The type of event to emit.
            data: The data associated with the event.
        """

        await event_emitter.emit_event(event_type, data)


plugin_manager = PluginManager()
