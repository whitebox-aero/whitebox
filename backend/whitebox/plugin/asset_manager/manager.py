import asyncio
import json
import logging
import threading
import time
from collections import defaultdict
from importlib import resources
from pathlib import Path

import aiohttp
from channels.layers import get_channel_layer
from django.conf import settings

from ..exceptions import AssetManifestError, AssetManifestParseError
from .spec import AssetFileSpec, AssetFileSpecList


logger = logging.getLogger(__name__)


class DownloadTracker:
    def __init__(self, plugin_name):
        self.plugin_name = plugin_name
        self.total_files = 0
        self.files_completed = 0

        self.expected_size = 0
        self.downloaded_size = 0

        self.progress = 0

    def add_files(self, file_specs, file_sizes):
        """
        Register files that need to be downloaded.

        Parameters:
            file_specs: List of AssetFileSpec objects
            file_sizes: List of file sizes (can contain None values)
        """

        self.total_files = len(file_specs)

        # Calculate total size only from files with known sizes
        for size in file_sizes:
            if size is not None:
                self.expected_size += size

    async def _emit(self, status, **kwargs):
        if not asset_manager._emit_status:
            return

        await asset_manager._channel_layer.group_send(
            "management",
            {
                "type": "plugin.download",
                "plugin_name": self.plugin_name,
                "status": status,
                **kwargs,
            },
        )

    async def emit_started(self):
        await self._emit("started")

    async def emit_downloading(self):
        await self._emit(
            "downloading",
            total_size=self.expected_size,
            total_files=self.total_files,
        )

    async def emit_finished(self):
        await self._emit("finished")

    async def emit_failed(self, error_count):
        await self._emit("failed", files_failed_to_download=error_count)

    async def emit_update(self):
        await self._emit(
            "update",
            progress=self.progress,
            downloaded_size=self.downloaded_size,
            total_size=self.expected_size,
            files_completed=self.files_completed,
            total_files=self.total_files,
        )

    async def update(self, downloaded_size):
        """
        Update the tracker with new download chunk information.
        """

        self.downloaded_size += downloaded_size
        self._calculate_progress()
        await self.emit_update()

    async def file_completed(self):
        """
        Mark a file as completed.
        """

        self.files_completed += 1
        self._calculate_progress()
        await self.emit_update()

    def _calculate_progress(self):
        """
        Calculate the current progress percentage using a weighted approach.
        """

        # Use two progress components:
        # 1. Files completed / total files
        # 2. Size downloaded / expected size (if known)

        # Always include files completed component
        file_progress = (
            (self.files_completed / self.total_files) * 100
            if self.total_files > 0
            else 0
        )

        # Only include size component if we have expected size
        if self.expected_size > 0:
            # If we've downloaded more than expected, adjust expected size to match reality
            if self.downloaded_size > self.expected_size:
                self.expected_size = self.downloaded_size

            size_progress = (self.downloaded_size / self.expected_size) * 100

            # Weight the components - give more weight to file completion as size can be unreliable
            if self.files_completed == self.total_files:
                # If all files completed, we're at 100%
                self.progress = 100
            else:
                # Balanced weighting between file count and size
                self.progress = (file_progress * 0.6) + (size_progress * 0.4)
        else:
            # No size info, use file progress only
            self.progress = file_progress

        # Ensure progress is never above 100%
        self.progress = min(100, self.progress)


class AssetManager:
    """
    Singleton class for managing plugin assets.

    Attributes:
        manifest_file_name: Name of the manifest file to look for in plugins.
        _semaphore: A semaphore used to limit the number of parallel downloads.
        _lock: A lock to ensure thread safety when communicating with the
               AssetManager's resources.
        _async_lock: A lock to ensure concurrency safety during asset downloads.
        _event_loop: A separate event loop used for downloading assets.
        _loop_thread: A thread to run the event loop.
    """

    manifest_file_name = "external-asset-manifest.json"
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            instance = cls._instance = super().__new__(cls)

            instance._lock = threading.Lock()
            instance._async_lock = asyncio.Lock()
            # TODO: Increase this when we have proper downloading, and impose
            #       per-domain limitations (e.g. downloads fail when we try a
            #       lot of parallel downloads from gitlab.com)
            instance._semaphore = asyncio.Semaphore(2)

            # Setup event loop and its thread
            event_loop = asyncio.new_event_loop()
            instance._event_loop = event_loop
            instance._loop_thread = threading.Thread(
                target=instance._run_event_loop,
                daemon=True,
            )

            instance._download_locks = defaultdict(asyncio.Lock)
            instance._download_status = {}
            instance._download_progress = {}

            instance._emit_status = False
            instance._channel_layer = None

        return cls._instance

    def _run_event_loop(self):
        asyncio.set_event_loop(self._event_loop)
        self._event_loop.run_forever()

    def _ensure_loop_started(self):
        if not self._loop_thread.is_alive():
            self._loop_thread.start()

    def _run_coro_on_loop(self, coro):
        with self._lock:
            self._ensure_loop_started()
            asyncio.run_coroutine_threadsafe(coro, self._event_loop)

    def enable_emit_status(self):
        with self._lock:
            self._emit_status = True
            self._channel_layer = get_channel_layer()

    def disable_emit_status(self):
        with self._lock:
            self._emit_status = False
            self._channel_layer = None

    def build_asset_path(self, module_name, target_path) -> Path:
        """
        Build the path to an asset file.

        Parameters:
            module_name: The name of the plugin module.
            target_path: The target path of the asset file.

        Returns:
            str: The path to the asset file.
        """
        return settings.MANAGED_ASSETS_ROOT / module_name / target_path

    def build_asset_path_for_file_spec(self, file_spec):
        return self.build_asset_path(
            file_spec.module_name,
            file_spec.target_path,
        )

    def _manifest_file_exists(self, module_name):
        has_manifest = resources.is_resource(
            module_name,
            self.manifest_file_name,
        )
        return has_manifest

    def _load_manifest_json(self, module_name):
        with resources.open_text(module_name, self.manifest_file_name) as f:
            try:
                return json.loads(f.read())
            except json.JSONDecodeError:
                logger.error(
                    "Failed to decode JSON data in manifest file for plugin "
                    f"{module_name}",
                )
                raise AssetManifestParseError

    def _load_manifest_data(self, manifest_json, module_name):
        try:
            return AssetFileSpecList.from_dict(manifest_json, module_name)
        except KeyError:
            logger.error("Manifest file is missing required fields")
            raise AssetManifestError

    def _calculate_file_hash(self, hashing_class, fd):
        hash_ = hashing_class()

        chunk = fd.read(4096)
        while chunk:
            hash_.update(chunk)
            chunk = fd.read(4096)

        return hash_.hexdigest()

    def _check_asset_file_status(self, file_spec: AssetFileSpec) -> bool:
        expected_path = self.build_asset_path_for_file_spec(file_spec)

        if not expected_path.exists():
            return False

        # If the file exists, but integrity hash does not exist, nothing to do
        # here, we'll consider that the file is valid.
        if not file_spec.integrity:
            return True

        with expected_path.open("rb") as f:
            file_hash = self._calculate_file_hash(
                file_spec.get_hashing_class(),
                f,
            )

        hashes_match = file_hash == file_spec.get_expected_file_hash()

        # If the hash of the file we have locally matches the one in the plugin
        # manifest, then the file is valid
        return hashes_match

    def load_manifest_for_plugin(self, module_name):
        """
        Load the manifest file for a plugin.

        Parameters:
            module_name: The name of the plugin module.

        Returns:
            AssetFileSpecList: The asset file specification list.
        """
        has_manifest = self._manifest_file_exists(module_name)
        if not has_manifest:
            return AssetFileSpecList(sources=[], module_name=module_name)

        manifest_json = self._load_manifest_json(module_name)
        return self._load_manifest_data(manifest_json, module_name)

    def check_plugin_asset_status(self, module_name) -> tuple[bool, list[str]]:
        """
        Check the status of the plugin assets.

        Parameters:
            module_name: The name of the plugin module.

        Returns:
            tuple[bool, list[str]]: A tuple containing a boolean indicating
                whether the assets are valid, and a list of invalid files.
        """
        has_manifest = self._manifest_file_exists(module_name)
        if not has_manifest:
            # No manifest file, so no external assets exist, and any potentially
            # bundled static file is present within the package itself.
            return True, []

        manifest_json = self._load_manifest_json(module_name)
        manifest_data = self._load_manifest_data(manifest_json, module_name)

        missing_files = []

        for asset_file in manifest_data.sources:
            file_status = self._check_asset_file_status(asset_file)
            if not file_status:
                missing_files.append(asset_file)

        return (not missing_files), missing_files

    def schedule_missing_plugin_assets_download(self, module_name):
        synced, missing = self.check_plugin_asset_status(module_name)

        if not synced:
            self.schedule_asset_download(
                module_name=module_name,
                asset_file_spec_list=missing,
            )
            return True
        else:
            return False

    # region core
    def _load_core_manifest(self) -> AssetFileSpecList:
        """
        Load the core manifest file from BASE_DIR.

        Returns:
            AssetFileSpecList: The asset file specification list for core assets.
        """

        manifest_path = settings.BASE_DIR / self.manifest_file_name

        if not manifest_path.exists():
            return AssetFileSpecList(sources=[], module_name="core")

        try:
            with manifest_path.open("r") as f:
                manifest_json = json.loads(f.read())
        except json.JSONDecodeError:
            logger.error("Failed to decode JSON data in core manifest file")
            raise AssetManifestParseError

        try:
            return self._load_manifest_data(manifest_json, "core")
        except KeyError:
            logger.error("Core manifest file is missing required fields")
            raise AssetManifestError

    def check_core_external_assets_status(self) -> tuple[bool, list[str]]:
        """
        Check the status of the core external assets.

        Returns:
            tuple[bool, list[str]]: A tuple containing a boolean indicating
                whether the assets are valid, and a list of invalid files.
        """
        try:
            manifest_data = self._load_core_manifest()
        except (AssetManifestError, AssetManifestParseError):
            return False, []

        missing_files = []

        for asset_file in manifest_data.sources:
            file_status = self._check_asset_file_status(asset_file)
            if not file_status:
                missing_files.append(asset_file)

        return (not missing_files), missing_files

    def schedule_missing_core_assets_download(self) -> bool:
        """
        Schedule download of missing core assets.
        """
        synced, missing = self.check_core_external_assets_status()

        if not synced:
            self.schedule_asset_download(
                module_name="core",
                asset_file_spec_list=missing,
            )
            return True
        else:
            return False

    def verify_file_integrity(self, file_spec: AssetFileSpec, file_path: Path) -> bool:
        """
        Verify the integrity of a downloaded file against its manifest hash.

        Parameters:
            file_spec: The asset file specification
            file_path: Path to the downloaded file

        Returns:
            bool: True if integrity check passes, False otherwise
        """
        if not file_spec.integrity:
            return True

        try:
            with file_path.open("rb") as f:
                file_hash = self._calculate_file_hash(
                    file_spec.get_hashing_class(),
                    f,
                )

            expected_hash = file_spec.get_expected_file_hash()
            hashes_match = file_hash == expected_hash

            return hashes_match
        except Exception:
            logger.exception(f"Error verifying file integrity")
            return False

    # endregion core

    # region downloading
    # These methods are used to download asset files from the internet. They
    # might seem a bit convoluted, but it's made in this way so that we have
    # absolute control over the download process and file's life-cycle

    async def _get_file_size(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.head(url) as response:
                response.raise_for_status()
                headers = response.headers

        try:
            return int(headers["Content-Length"])
        except KeyError:
            # `Content-Length` header is missing, so we don't know the file size
            return None
        except ValueError:
            logger.error("Failed to parse file size from headers")
            return None

    async def _get_file_sizes(self, asset_file_spec_list):
        file_size_tasks = [
            self._get_file_size(file_spec.url) for file_spec in asset_file_spec_list
        ]
        file_sizes = await asyncio.gather(*file_size_tasks)

        return file_sizes

    def schedule_asset_download(
        self,
        module_name: str,
        asset_file_spec_list: list[AssetFileSpec],
    ):
        coro = self.perform_asset_download(
            module_name,
            asset_file_spec_list,
        )
        self._run_coro_on_loop(coro)

    async def perform_asset_download(
        self,
        module_name: str,
        asset_file_spec_list: list[AssetFileSpec],
    ):
        async with self._download_locks[module_name]:
            tracker = DownloadTracker(module_name)
            self._download_status[module_name] = tracker

            await tracker.emit_started()

            # Get file sizes where possible
            file_sizes = await self._get_file_sizes(asset_file_spec_list)

            # Register all files with their sizes
            tracker.add_files(asset_file_spec_list, file_sizes)

            # Count files with known and unknown sizes for logging
            files_with_known_size = sum(1 for size in file_sizes if size is not None)
            files_with_unknown_size = len(file_sizes) - files_with_known_size
            total_known_size = sum(size for size in file_sizes if size is not None)

            logger.info(
                f"Downloading {len(asset_file_spec_list)} files for module {module_name}: "
                f"{files_with_known_size} with known size ({total_known_size} bytes), "
                f"{files_with_unknown_size} with unknown size"
            )

            await tracker.emit_downloading()

            error_count = await self.download_asset_files(asset_file_spec_list)

            if error_count:
                await tracker.emit_failed(error_count)
            else:
                await tracker.emit_finished()

    async def _stream_download(
        self,
        file_spec: AssetFileSpec,
        _retry_count: int = 3,  # default retry count
    ):
        async with aiohttp.ClientSession() as session:
            async with session.get(file_spec.url) as response:
                response.raise_for_status()

                async for chunk in response.content.iter_any():
                    yield chunk

    async def _download_to_file(
        self,
        file_spec: AssetFileSpec,
        download_path: Path,
    ):
        download_status = self._download_status[file_spec.module_name]
        total_downloaded_size = 0

        try:
            with download_path.open("wb") as f:
                async for chunk in self._stream_download(file_spec):
                    downloaded_size = len(chunk)
                    f.write(chunk)

                    await download_status.update(downloaded_size)
                    total_downloaded_size += downloaded_size

                f.flush()
        except:
            # In case the file failed to download, retries will be attempted,
            # but in order for frontend to be accurately updated, we need to
            # update download status as if this file was not downloaded
            download_status.update(-total_downloaded_size)
            raise

    async def _try_downloading_file(
        self,
        file_spec: AssetFileSpec,
        download_path: Path,
        max_retries=3,
        _current_retry_count=1,
    ) -> bool:
        try:
            await self._download_to_file(file_spec, download_path)
            return True
        except Exception as e:
            should_retry = _current_retry_count < max_retries

            if should_retry:
                retry_log_message = (
                    f" Retrying... ({_current_retry_count + 1}/{max_retries})"
                )

            else:
                retry_log_message = f" Aborting after {max_retries} failed attempts."

            logger.exception(
                f'Failed to download asset file "{file_spec.url}" for plugin '
                f'"{file_spec.module_name}". {retry_log_message}',
            )

            if should_retry:
                return await self._try_downloading_file(
                    file_spec,
                    download_path,
                    max_retries,
                    _current_retry_count=_current_retry_count + 1,
                )

            return False

    async def download_asset_files(self, file_specs: list[AssetFileSpec]) -> int:
        download_tasks = [
            self.download_asset_file(file_spec) for file_spec in file_specs
        ]
        results = await asyncio.gather(
            *download_tasks,
            return_exceptions=True,
        )

        error_count = 0

        for i, result in enumerate(results):
            if isinstance(result, Exception):
                error_count += 1
                file_spec = file_specs[i]
                logger.error(
                    f'Error downloading asset file "{file_spec.url}" for plugin "{file_spec.module_name}": {result}',
                    exc_info=result,
                )

        return error_count

    async def download_asset_file(self, file_spec: AssetFileSpec):
        logger.info(
            'Downloading asset file "{}" for module "{}"'.format(
                file_spec.target_path,
                file_spec.module_name,
            )
        )
        t_start = time.monotonic()

        target_path = self.build_asset_path_for_file_spec(file_spec)

        temp_suffix = "{}.download".format(target_path.suffix)
        temp_path = target_path.with_suffix(temp_suffix)

        download_dir = target_path.parent
        download_dir.mkdir(parents=True, exist_ok=True)

        success = await self._try_downloading_file(file_spec, temp_path)
        if not success:
            return

        # After download is complete, move the file to its final location.
        # If a previous version of the file exists, it will be overwritten.
        if target_path.exists():
            logger.debug(
                'Replacing existing file "{}" with a newly downloaded one...'.format(
                    target_path,
                )
            )
            target_path.unlink()
        temp_path.rename(target_path)

        # Mark this file as completed in the tracker
        if file_spec.module_name in self._download_status:
            await self._download_status[file_spec.module_name].file_completed()

        t_elapsed = time.monotonic() - t_start
        logger.info(
            'Downloaded asset file "{}" for module "{}" in {:.2f}s'.format(
                file_spec.target_path,
                file_spec.module_name,
                t_elapsed,
            )
        )

        # Verify integrity hash
        integrity_verified = self.verify_file_integrity(file_spec, target_path)
        if not integrity_verified:
            logger.warning(
                f'Hash mismatch for file "{file_spec.target_path}" in module "{file_spec.module_name}"'
            )

    # endregion downloading


asset_manager = AssetManager()
