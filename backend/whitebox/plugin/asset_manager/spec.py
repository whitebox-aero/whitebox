import hashlib
from dataclasses import dataclass


supported_hashing_algorithms = {
    "sha1": hashlib.sha1,
    "sha256": hashlib.sha256,
}


@dataclass
class AssetFileSpec:
    module_name: str
    url: str
    integrity: str | None
    target_path: str

    @classmethod
    def from_dict(cls, data: dict, module_name: str):
        return cls(
            module_name=module_name,
            url=data["url"],
            target_path=data["target_path"],
            integrity=data.get("integrity"),
        )

    def get_hashing_class(self):
        try:
            algorithm = self.integrity.split("-")[0]
        except (AttributeError, IndexError):
            return None

        return supported_hashing_algorithms.get(algorithm)

    def get_expected_file_hash(self):
        try:
            return self.integrity.split("-")[1]
        except (AttributeError, IndexError):
            return None


@dataclass
class AssetFileSpecList:
    module_name: str
    sources: list[AssetFileSpec]

    @classmethod
    def from_dict(cls, data: dict, module_name: str):
        return cls(
            module_name=module_name,
            sources=[
                AssetFileSpec.from_dict(source, module_name)
                for source in data["sources"]
            ],
        )
