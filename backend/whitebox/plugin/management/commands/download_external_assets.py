import threading
import asyncio
import time

from django.core.management.base import BaseCommand, CommandError
from dataclasses import dataclass
from typing import Optional
from tqdm import tqdm

from plugin.manager import plugin_manager
from plugin.asset_manager import asset_manager


@dataclass
class DownloadActivityMonitor:
    """
    Monitor download activity to detect stalled or slow downloads.
    """

    # Minimum speed threshold in bytes per second (e.g., 1 KB/s)
    min_speed_threshold: int = 1024

    # Inactivity timeout in seconds - how long to wait with zero bytes downloaded
    inactivity_timeout: int = 30

    # Period over which to monitor the slow connection in seconds
    slow_connection_period: int = 60

    # Last byte count and timestamp
    last_byte_count: int = 0
    last_timestamp: float = 0

    # Historical byte readings for speed calculation
    history_timestamps: list = None
    history_byte_counts: list = None

    # Status
    is_started: bool = False

    def __post_init__(self):
        self.history_timestamps = []
        self.history_byte_counts = []

    def update(self, total_downloaded: int) -> tuple[bool, Optional[str]]:
        """
        Update the monitor with the latest download progress.

        Parameters:
            total_downloaded: Current total bytes downloaded

        Returns:
            tuple: (is_timeout, reason)
                - is_timeout: True if the download should time out
                - reason: Reason for timeout if is_timeout is True, None otherwise
        """

        now = time.time()

        if not self.is_started:
            self.last_timestamp = now
            self.last_byte_count = total_downloaded
            self.is_started = True
            return False, None

        # Calculate bytes transferred since last update
        bytes_delta = total_downloaded - self.last_byte_count
        time_delta = now - self.last_timestamp

        # Update historical data
        self.history_timestamps.append(now)
        self.history_byte_counts.append(total_downloaded)

        # Remove old history entries beyond our monitoring period
        while (
            self.history_timestamps
            and (now - self.history_timestamps[0]) > self.slow_connection_period
        ):
            self.history_timestamps.pop(0)
            self.history_byte_counts.pop(0)

        # Check for inactivity timeout (no new bytes downloaded)
        if bytes_delta == 0 and time_delta > self.inactivity_timeout:
            return True, f"Download stalled for {time_delta:.1f} seconds"

        # Check for slow connection (if we have enough history data)
        if len(self.history_timestamps) >= 2:
            period_time = now - self.history_timestamps[0]
            period_bytes = total_downloaded - self.history_byte_counts[0]

            # Only check slow connection if we have sufficient history (at least 30 seconds)
            if period_time >= 30 and period_time <= self.slow_connection_period:
                avg_speed = period_bytes / period_time

                if avg_speed < self.min_speed_threshold:
                    return (
                        True,
                        f"Download speed too slow: {avg_speed:.1f} B/s (below {self.min_speed_threshold} B/s threshold)",
                    )

        # Update for next check
        self.last_byte_count = total_downloaded
        self.last_timestamp = now

        return False, None


class Command(BaseCommand):
    help = "Download all external assets as specified in external-asset-manifest.json for core and plugins"

    def __init__(self):
        super().__init__()
        self.download_complete_event = threading.Event()
        self.download_failed_event = threading.Event()
        self.download_errors = []
        self.track_downloads_for = []
        self.check_download_status_every = 0.1
        self.download_monitors = {}
        self.progress_bars = {}
        self.wait_for_downloads = False

        # Configure tqdm to write to stdout with appropriate settings
        self.tqdm_kwargs = {
            "unit": "B",
            "unit_scale": True,
            "unit_divisor": 1024,
            "bar_format": "{l_bar}{bar}| {n_fmt}/{total_fmt} [{elapsed}<{remaining}, {rate_fmt}{postfix}]",
        }

    def add_arguments(self, parser):
        parser.add_argument(
            "--core",
            action="store_true",
            required=False,
            help="Download core external assets",
        )
        parser.add_argument(
            "--plugins",
            required=False,
            nargs="*",
            help="Download external assets for specific plugins. Pass 'all' to download for all plugins",
        )

    def _get_message(self, message):
        return f"[{time.strftime('%H:%M:%S')}] {message}"

    def _log_success(self, message):
        self.stdout.write(self.style.SUCCESS(self._get_message(message)))

    def _log_error(self, message):
        self.stderr.write(self.style.ERROR(self._get_message(message)))

    def _log(self, message):
        self.stdout.write(self._get_message(message))

    def _initialize_progress_bars(self):
        """
        Initialize tqdm progress bars for each module being tracked.
        """

        for module_name in self.track_downloads_for:
            if module_name in asset_manager._download_status:
                tracker = asset_manager._download_status[module_name]

                # Create a progress bar for this module
                self.progress_bars[module_name] = tqdm(
                    desc=f"{module_name}",
                    total=tracker.expected_size or 0,
                    initial=0,
                    **self.tqdm_kwargs,
                )

                # If size is unknown, display indeterminate progress
                if tracker.expected_size == 0:
                    self.progress_bars[module_name].total = 1
                    self.progress_bars[module_name].set_postfix(status="Size unknown")

    async def _track_downloads(self):
        """
        Track the download status of all plugins.
        """

        # Initialize progress bars for each module being tracked
        self._initialize_progress_bars()

        while True:
            all_complete = True
            active_downloads = False

            for module_name in self.track_downloads_for:
                if module_name in asset_manager._download_status:
                    tracker = asset_manager._download_status[module_name]
                    active_downloads = True

                    if tracker.progress is not None:
                        # Create monitor if it doesn't exist yet
                        if module_name not in self.download_monitors:
                            self.download_monitors[module_name] = (
                                DownloadActivityMonitor()
                            )

                        # Update progress bar
                        progress_bar = self.progress_bars.get(module_name)
                        if progress_bar:
                            # Update progress bar with current values
                            new_value = tracker.downloaded_size
                            old_value = progress_bar.n

                            # Only update if there's a change to avoid unnecessary refreshes
                            if new_value != old_value:
                                # Update and set postfix with percentage
                                progress_bar.update(new_value - old_value)
                                progress_bar.set_postfix(
                                    percentage=f"{tracker.progress:.1f}%"
                                )

                        if tracker.progress < 100:
                            all_complete = False

                            # Check for timeout conditions
                            is_timeout, reason = self.download_monitors[
                                module_name
                            ].update(tracker.downloaded_size)
                            if is_timeout:
                                self.download_errors.append(
                                    f"Download timeout for {module_name}: {reason}"
                                )
                                self.download_failed_event.set()
                                break
                        elif (
                            module_name in self.progress_bars
                            and not self.progress_bars[module_name].disable
                        ):
                            # Mark complete
                            if not progress_bar.disable:
                                progress_bar.set_description(
                                    f"{module_name} [COMPLETE]"
                                )
                                progress_bar.refresh()
                    else:
                        self.download_errors.append(
                            f"Download failed for plugin: {module_name}"
                        )

                        # Update progress bar to show the error
                        if module_name in self.progress_bars:
                            self.progress_bars[module_name].set_description(
                                f"{module_name} [FAILED]"
                            )

            if active_downloads and all_complete:
                self.download_complete_event.set()
                break
            elif not active_downloads:
                await asyncio.sleep(self.check_download_status_every)
            else:
                await asyncio.sleep(self.check_download_status_every)

    def _add_to_track(self, module_name):
        self.wait_for_downloads = True
        self.track_downloads_for.append(module_name)

    def _download_core_external_assets(self):
        scheduled = asset_manager.schedule_missing_core_assets_download()

        if scheduled:
            self._add_to_track("core")

    def _download_plugin_external_assets(self, plugin_names):
        discovered_plugins = plugin_manager._get_discovered_plugins()

        for module_name, _ in discovered_plugins.items():
            if "all" in plugin_names or module_name in plugin_names:
                scheduled = asset_manager.schedule_missing_plugin_assets_download(
                    module_name
                )

                if scheduled:
                    self._add_to_track(module_name)

    def _wait_for_downloads(self):
        try:
            while (
                not self.download_complete_event.is_set()
                and not self.download_failed_event.is_set()
            ):
                time.sleep(self.check_download_status_every)
        finally:
            for progress_bar in self.progress_bars.values():
                progress_bar.close()

        # Report results
        if self.download_errors:
            for error in self.download_errors:
                self._log_error(error)
        else:
            self._log_success("All asset downloads completed successfully")

    def handle(self, *args, **options):
        if not options["core"] and not options["plugins"]:
            raise CommandError("Please specify --core or --plugins")

        if options["core"]:
            self._log("Downloading core external assets...")
            self._download_core_external_assets()

        if options["plugins"]:
            self._log("Downloading plugin external assets...")
            cleaned_plugin_names = [x.replace("-", "_") for x in options["plugins"]]
            self._download_plugin_external_assets(cleaned_plugin_names)

        if self.wait_for_downloads:
            # Setup tracking coroutine
            tracking_coro = self._track_downloads()
            asset_manager._run_coro_on_loop(tracking_coro)

            # Wait for downloads to complete
            self._wait_for_downloads()
        else:
            self._log_success("No additional assets to download")
