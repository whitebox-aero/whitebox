import json
from importlib import resources

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Verify external-asset-manifest.json file"

    def add_arguments(self, parser):
        parser.add_argument(
            "TARGET_FILE",
            type=str,
            nargs="?",
            help="The target file to verify. Mutually exclusive with --module_name",
        )
        parser.add_argument(
            "--module-name",
            type=str,
            required=False,
            help="The module name to verify. " "Mutually exclusive with TARGET_FILE",
        )

    def _load_file(self, options):
        try:
            if options["TARGET_FILE"]:
                f = open(options["TARGET_FILE"], "r")
            else:
                f = resources.open_text(
                    options["module_name"],
                    "external-asset-manifest.json",
                )
        except FileNotFoundError:
            raise CommandError("File not found")
        except ModuleNotFoundError:
            raise CommandError(f"Module {options['module_name']} not found")

        try:
            return f.read()
        finally:
            f.close()

    def _verify_sources(self, data):
        try:
            sources = data["sources"]
        except KeyError:
            raise CommandError(
                'Manifest file is missing required root field: "source"',
            )

        for index, source in enumerate(sources):
            try:
                source["url"]
                source["integrity"]
                source["target_path"]
            except KeyError as e:
                field_name = str(e)

                raise CommandError(
                    f"Manifest file is missing required field {field_name} "
                    f"in source with index {index}",
                )

    def handle(self, *args, **options):
        if options["TARGET_FILE"] and options["module_name"]:
            raise CommandError(
                "TARGET_FILE and --module-name are mutually exclusive",
            )

        if not options["TARGET_FILE"] and not options["module_name"]:
            raise CommandError(
                "Either TARGET_FILE or --module-name must be provided",
            )

        file = self._load_file(options)

        try:
            data = json.loads(file)
        except json.JSONDecodeError:
            raise CommandError("Manifest file is not a valid JSON file!")

        self._verify_sources(data)

        self.stdout.write(self.style.SUCCESS("Manifest file is valid!"))
