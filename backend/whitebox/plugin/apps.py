from django.apps import AppConfig
from django.conf import settings


class PluginConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "plugin"

    def _discover_plugins(self):
        if settings.IS_DJANGO_AUTORELOAD_PROCESS:
            return

        from .manager import plugin_manager

        plugin_manager.discover_plugins()

    def ready(self):
        self._discover_plugins()
