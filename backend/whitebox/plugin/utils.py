from typing import (
    Optional,
    List,
    Type,
    Protocol,
    Callable,
    runtime_checkable,
)

from devices.base import Device
from whitebox.api import API


class WhiteboxStandardAPI:
    """
    This class provides a standard API for plugins to interact with whitebox.

    Attributes:
        api: Instance of the standard API provided by whitebox.

    Methods:
        register_event_callback: Register an event callback for a specific event type.
    """

    api = API

    @staticmethod
    def register_event_callback(event_type: str, callback: Callable) -> None:
        """
        Register an event callback for a specific event type.
        This allows a plugin to listen for events triggered by the

        Parameters:
            event_type: The type of event to listen for.
            callback: The function to call when the event is triggered.

        Example:
            ```python
            import whitebox

            class MyPlugin(whitebox.Plugin):
                def __init__(self):
                    self.whitebox.register_event_callback("location_update", self.on_location_update)

                def on_location_update(data):
                    print(f"location_update event received: {data}")
            ```
        """

        from .manager import plugin_manager

        plugin_manager.register_event_callback(event_type, callback)

    def unregister_event_callback(self, event_type: str, callback: Callable) -> None:
        """
        Unregister an event callback for a specific event type.

        Parameters:
            event_type: The type of event to unregister the callback for.
            callback: The callback function to be removed.

        Example:
            ```python
            import whitebox

            class MyPlugin(whitebox.Plugin):
                def __init__(self):
                    self.whitebox.unregister_event_callback("location_update", self.on_location_update)

                def on_location_update(data):
                    print(f"location_update event received: {data}")
            ```
        """

        from .manager import plugin_manager

        plugin_manager.unregister_event_callback(event_type, callback)

    async def emit_event(self, event_type: str, data: dict = None) -> None:
        """
        Emit an event to all registered listeners.

        Parameters:
            event_type: The type of event to emit.
            data: The data to send with the event.

        Example:
            ```python
            import whitebox

            class MyPlugin(whitebox.Plugin):
                def some_method(self):
                    self.whitebox.emit_event("my_event", {"key": "value"})
            ```
        """

        from .manager import plugin_manager

        await plugin_manager.emit_event(event_type, data)


@runtime_checkable
class Plugin(Protocol):
    """
    This is the base class for all plugins.
    All plugins should adhere to this interface to ensure compatibility with whitebox.

    Attributes:
        whitebox: Instance of the standard API wrapper provided by whitebox.
        plugin_template: Path to the template file for the plugin.
        plugin_css: List of paths to the CSS file(s) for the plugin.
        plugin_js: List of paths to the JavaScript file(s) for the plugin.
        devices: List of device classes that the plugin contributes to the
                 device list.
    """

    whitebox = WhiteboxStandardAPI()

    plugin_template: Optional[str] = None
    plugin_css: List[str] = []
    plugin_js: List[str] = []

    device_classes: List[Type[Device]] = []

    provides_capabilities = []
    provider_templates: dict[str, str] = {}

    augments_plugin: Optional[str] = None

    def get_provider_template(self, capability: str) -> str | None:
        """
        Return the path to the HTML template file for the provider of a
        specific capability, if it exists.
        """
        return self.provider_templates.get(capability)

    def get_provider_template_context(self, capability: str) -> dict:
        """
        Return the context to be passed to the provider template renderer.
        """
        return {}

    def get_css(self) -> list:
        """
        Return the path to the CSS file(s) for the plugin.
        """

        return self.plugin_css.copy()

    def get_js(self) -> list:
        """
        Return the path to the JavaScript file(s) for the plugin.
        """

        return self.plugin_js.copy()

    def get_device_classes(self) -> list:
        """
        Return the list of device classes that the plugin contributes to the
        device list.
        """

        return self.device_classes.copy()
