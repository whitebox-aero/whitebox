from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .manager import plugin_manager


@csrf_exempt
async def refresh_plugins(request):
    plugin_manager.discover_plugins()
    return JsonResponse({"status": "success"})
