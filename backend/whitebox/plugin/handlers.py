import logging

from utils.human_readable import human_file_size
from whitebox.utils import EventHandler


logger = logging.getLogger(__name__)


class PluginDownloadHandler(EventHandler):
    """
    Event handler for plugin download events.
    """

    async def _handle_status_empty(self, data):
        return {}

    handle_status_started = _handle_status_empty
    handle_status_finished = _handle_status_empty

    async def handle_status_downloading(self, data):
        return {
            "total_size": data["total_size"],
        }

    async def handle_status_failed(self, data):
        return {
            "files_failed_to_download": data["files_failed_to_download"],
        }

    async def handle_status_update(self, data):
        downloaded_size = data["downloaded_size"]
        total_size = data["total_size"]

        progress = data["progress"]
        if progress is not None:
            progress = round(progress, 2)

        h_downloaded_size = human_file_size(downloaded_size)
        h_total_size = human_file_size(total_size)

        return {
            "downloaded_size": h_downloaded_size,
            "total_size": h_total_size,
            "progress": progress,
        }

    async def handle(self, data):
        status = data["status"]
        handler_name = f"handle_status_{status}"
        handler = getattr(self, handler_name, None)

        payload = {
            "type": "plugin_download",
            "plugin_name": data["plugin_name"],
            "status": status,
        }

        if not handler:
            # Do not add anything to the payload if the status is unknown,
            # to avoid leaking sensitive information
            logger.warning(f"Received unknown `status`: {status}")
        else:
            handler_data = await handler(data)
            payload.update(handler_data)

        return payload
