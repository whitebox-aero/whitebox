class PluginError(Exception):
    pass


class AssetError(PluginError):
    pass


class AssetManifestError(AssetError):
    pass


class AssetManifestParseError(AssetManifestError):
    pass
