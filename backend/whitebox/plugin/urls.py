from django.urls import path
from . import views


urlpatterns = [
    path("plugins/refresh/", views.refresh_plugins, name="refresh_plugins"),
]
