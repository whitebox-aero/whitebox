from django.db import models
from reversion import register


@register()
class Event(models.Model):
    timestamp = models.DateTimeField()

    class Meta:
        app_label = "event"
        db_table = "event"

    def __str__(self):
        return f"Event at {self.timestamp}"
