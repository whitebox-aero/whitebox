from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import healthcheck
from .devices.views import DeviceConnectionViewSet


router = SimpleRouter()


router.register(
    r"devices",
    DeviceConnectionViewSet,
    basename="device",
)


app_name = "api"

urlpatterns = [
    *router.urls,
    path("healthcheck/", healthcheck, name="healthcheck"),
]
