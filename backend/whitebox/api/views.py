import random

from django.http import JsonResponse


healthcheck_messages = [
    "Safe flight!",
    "Ready to fly!",
    "All systems go!",
    "Ready for takeoff!",
    "Green lights!",
]


def healthcheck(request):
    message = random.choice(healthcheck_messages)

    response = {
        "status": "ok",
        "message": message,
    }
    return JsonResponse(response, status=200)
