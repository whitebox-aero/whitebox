import reversion

from typing import Type
from datetime import datetime
from asgiref.sync import sync_to_async
from channels.layers import get_channel_layer

from .models import Location
from event.models import Event


class LocationService:
    """
    Service class for handling location related operations.
    """

    channel_layer = get_channel_layer()

    @classmethod
    @sync_to_async
    def update_location(
        cls: Type[Location],
        latitude: float,
        longitude: float,
        altitude: float,
        timestamp: datetime,
    ) -> Location:
        """
        Update the location in the database.

        Parameters:
            latitude: The latitude of the location.
            longitude: The longitude of the location.
            altitude: The altitude of the location.
            timestamp: The timestamp of the location.

        Returns:
            The Location object that was created.
        """

        with reversion.create_revision():
            event = Event.objects.create(timestamp=timestamp)

            location = Location.objects.create(
                latitude=latitude,
                longitude=longitude,
                altitude=altitude,
                event=event,
            )
            reversion.set_comment("Location updated")
        return location

    @classmethod
    @sync_to_async
    def get_latest_location(cls: Type[Location]) -> Location:
        """
        Get the latest location from the database.
        """

        return Location.objects.latest("event__timestamp")

    @classmethod
    async def emit_location_update(
        cls: Type[Location], latitude: float, longitude: float, altitude: float
    ) -> None:
        """
        Emit a location update event to all connected clients and plugins who are listening for location updates.

        Parameters:
            latitude: The latitude of the location.
            longitude: The longitude of the location.
            altitude: The altitude of the location.
        """

        await cls.channel_layer.group_send(
            "flight",
            {
                "type": "location_update",
                "latitude": latitude,
                "longitude": longitude,
                "altitude": altitude,
            },
        )
