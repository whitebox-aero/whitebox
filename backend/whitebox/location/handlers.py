from django.utils import timezone

from whitebox.utils import EventHandler
from location.services import LocationService
from whitebox.events import event_emitter


class LocationUpdateHandler(EventHandler):
    """
    Event handler for location update events.
    """

    async def handle(self, data):
        event_type = data["type"]
        latitude = data["latitude"]
        longitude = data["longitude"]
        altitude = data["altitude"]
        gps_timestamp = timezone.now()

        data = {
            "latitude": latitude,
            "longitude": longitude,
            "altitude": altitude,
            "gps_timestamp": gps_timestamp,
        }
        payload = {"type": event_type, **data}

        await LocationService.update_location(
            latitude, longitude, altitude, gps_timestamp
        )
        await event_emitter.emit_event("location_update", payload)

        payload["gps_timestamp"] = gps_timestamp.isoformat()

        return payload
