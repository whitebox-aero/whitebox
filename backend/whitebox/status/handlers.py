from whitebox.utils import EventHandler
from whitebox.events import event_emitter


class StatusUpdateHandler(EventHandler):
    """
    Event handler for status update events.
    """

    async def handle(self, data):
        event_type = data["type"]
        status_data = data["data"]

        await event_emitter.emit_event("status_update", status_data)

        return {"type": event_type, **status_data}
