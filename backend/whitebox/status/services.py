from channels.layers import get_channel_layer


class StatusService:
    """
    Service class for handling status related operations.
    """

    channel_layer = get_channel_layer()

    @classmethod
    async def emit_status_update(cls, status_data: dict):
        """
        Emit a status update event to the frontend.

        Parameters:
            status_data: The status data to send.
        """

        await cls.channel_layer.group_send(
            "flight",
            {
                "type": "status_update",
                "data": status_data,
            },
        )
