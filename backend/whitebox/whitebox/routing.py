from django.urls import re_path, path

from . import consumers


websocket_urlpatterns = [
    path("ws/management/", consumers.ManagementConsumer.as_asgi()),
    path("ws/flight/", consumers.FlightConsumer.as_asgi()),
]
