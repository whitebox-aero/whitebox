from location.services import LocationService
from traffic.services import TrafficService
from status.services import StatusService


class API:
    """
    Standarized API of whitebox for plugins to interact with the core system.

    Attributes:
        location: LocationService instance for interacting with the location service
        traffic: TrafficService instance for interacting with the traffic service
        status: StatusService instance for interacting with the status service
    """

    location = LocationService()
    traffic = TrafficService()
    status = StatusService()
