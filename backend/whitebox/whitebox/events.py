import asyncio
from typing import Callable, Dict, List, Any


class EventEmitter:
    def __init__(self):
        self._listeners: Dict[str, List[Callable]] = {}

    def append(self, event_type: str, callback: Callable):
        if event_type not in self._listeners:
            self._listeners[event_type] = []
        self._listeners[event_type].append(callback)

    def remove(self, event_type: str, callback: Callable):
        if event_type in self._listeners:
            self._listeners[event_type].remove(callback)

    async def emit_event(self, event_type: str, data: Any = None):
        if event_type in self._listeners:
            for callback in self._listeners[event_type]:
                await asyncio.create_task(callback(data))


event_emitter = EventEmitter()
