from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from plugin.utils import Plugin
    from devices.base import Device, DeviceWizard
    from devices.consts import DeviceType


def setup():
    from plugin.utils import Plugin
    from devices.base import Device, DeviceWizard
    from devices.consts import DeviceType

    # Define exports
    exports = {
        "Plugin": Plugin,
        "Device": Device,
        "DeviceWizard": DeviceWizard,
        "DeviceType": DeviceType,
    }

    # Add everything to __all__
    __all__ = list(exports.keys())

    # Update the global namespace
    globals().update(exports)
