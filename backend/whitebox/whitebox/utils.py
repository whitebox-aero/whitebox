from abc import ABC, abstractmethod


class EventHandler(ABC):
    """
    Abstract class for handling events.
    Each event handler should implement this class.
    """

    @abstractmethod
    async def handle(self, data):
        pass
