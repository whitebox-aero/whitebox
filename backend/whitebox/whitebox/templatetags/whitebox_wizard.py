from django import template
from django.template import loader
from django.utils.safestring import mark_safe

from .whitebox import tagged_static


register = template.Library()


@register.simple_tag
def wizard_video(static_path):
    html = loader.render_to_string(
        "bridge/widgets/wizard_video.html",
        context={
            "url": tagged_static(static_path),
        },
    )
    return mark_safe(html)


@register.simple_tag
def wizard_image(static_path):
    html = loader.render_to_string(
        "bridge/widgets/wizard_image.html",
        context={
            "url": tagged_static(static_path),
        },
    )
    return mark_safe(html)
