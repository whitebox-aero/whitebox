from django import template
from django.templatetags.static import static


register = template.Library()


@register.simple_tag
def tagged_url(path):
    return "%API_URL%" + path


@register.simple_tag
def tagged_static(path):
    static_path = static(path)
    tagged = tagged_url(static_path)
    return tagged
