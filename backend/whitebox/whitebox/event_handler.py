from location.handlers import LocationUpdateHandler
from traffic.handlers import TrafficUpdateHandler
from status.handlers import StatusUpdateHandler
from plugin.handlers import PluginDownloadHandler
from .handlers import FlightStartHandler, FlightEndHandler
from .utils import EventHandler


class EventHandlerFactory:
    """
    Factory class for creating event handlers.
    """

    _handlers = {}

    @classmethod
    def create_handler(cls, event_type: str) -> EventHandler:
        """
        Create an event handler based on the event type.
        """

        if event_type not in cls._handlers:
            if event_type == "flight_start":
                cls._handlers[event_type] = FlightStartHandler()
            elif event_type == "flight_end":
                cls._handlers[event_type] = FlightEndHandler()
            elif event_type == "location_update":
                cls._handlers[event_type] = LocationUpdateHandler()
            elif event_type == "traffic_update":
                cls._handlers[event_type] = TrafficUpdateHandler()
            elif event_type == "status_update":
                cls._handlers[event_type] = StatusUpdateHandler()
            elif event_type == "plugin_download":
                cls._handlers[event_type] = PluginDownloadHandler()
            else:
                raise ValueError(f"Unknown event type: {event_type}")

        return cls._handlers[event_type]
