from .utils import EventHandler
from .events import event_emitter


class FlightStartHandler(EventHandler):
    """
    Handler for handling the `flight_start` event.
    """

    async def handle(self, data):
        await event_emitter.emit_event("flight_start")
        return {"type": "message", "message": "Flight started"}


class FlightEndHandler(EventHandler):
    """
    Handler for handling the `flight_end` event.
    """

    async def handle(self, data):
        await event_emitter.emit_event("flight_end")
        return {"type": "message", "message": "Flight ended"}
