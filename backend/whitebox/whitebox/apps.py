from django.apps import AppConfig


class WhiteboxConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "whitebox"

    def ready(self):
        from . import setup

        setup()
