import json

from channels.generic.websocket import AsyncWebsocketConsumer

from .event_handler import EventHandlerFactory


class BaseWebsocketConsumer(AsyncWebsocketConsumer):
    """
    This consumer handles WebSocket connections for whitebox.
    """

    @property
    def consumer_group_name(self):
        raise NotImplementedError

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def connect(self) -> None:
        """
        Called when client connects to the WebSocket.
        """

        await self.channel_layer.group_add(
            self.consumer_group_name,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, close_code) -> None:
        """
        Called when client disconnects from the WebSocket.
        """

        await self.channel_layer.group_discard(
            self.consumer_group_name,
            self.channel_name,
        )

    async def receive(self, text_data: str) -> None:
        """
        Called when client sends a message to the WebSocket.

        Parameters:
            text_data: The message sent by the client.
        """

        data = json.loads(text_data)
        event_type = data["type"]

        try:
            handler = EventHandlerFactory.create_handler(event_type)
            result = await handler.handle(data)
            await self.send(text_data=json.dumps(result))
        except ValueError as e:
            await self.send(text_data=json.dumps({"type": "error", "message": str(e)}))


class FlightConsumer(BaseWebsocketConsumer):
    consumer_group_name = "flight"

    async def location_update(self, data: dict) -> None:
        """
        Called when the core system emits a location update event.

        Parameters:
            data: The data associated with the location update event.
        """

        handler = EventHandlerFactory.create_handler("location_update")
        result = await handler.handle(data)
        await self.send(text_data=json.dumps(result))

    async def traffic_update(self, data: dict) -> None:
        """
        Called when the core system emits a traffic update event.

        Parameters:
            data: The data associated with the traffic update event.
        """

        handler = EventHandlerFactory.create_handler("traffic_update")
        result = await handler.handle(data)
        await self.send(text_data=json.dumps(result))

    async def status_update(self, data: dict) -> None:
        """
        Called when the core system emits a status update event.

        Parameters:
            data: The data associated with the status update event.
        """

        handler = EventHandlerFactory.create_handler("status_update")
        result = await handler.handle(data)
        await self.send(text_data=json.dumps(result))


class ManagementConsumer(BaseWebsocketConsumer):
    consumer_group_name = "management"

    async def plugin_download(self, data: dict) -> None:
        handler = EventHandlerFactory.create_handler("plugin_download")
        result = await handler.handle(data)
        await self.send(text_data=json.dumps(result))
