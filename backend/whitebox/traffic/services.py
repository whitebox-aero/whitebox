from channels.layers import get_channel_layer


class TrafficService:
    """
    Service class for handling traffic related operations.
    """

    channel_layer = get_channel_layer()

    @classmethod
    async def emit_traffic_update(cls, traffic_data: dict):
        """
        Emit a traffic update event to the frontend.

        Parameters:
            traffic_data: The traffic data to send.
        """

        await cls.channel_layer.group_send(
            "flight",
            {
                "type": "traffic_update",
                "data": traffic_data,
            },
        )
