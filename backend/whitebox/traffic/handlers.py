from whitebox.utils import EventHandler
from whitebox.events import event_emitter


class TrafficUpdateHandler(EventHandler):
    """
    Event handler for traffic update events.
    """

    async def handle(self, data):
        event_type = data["type"]
        traffic_data = data["data"]

        await event_emitter.emit_event("traffic_update", traffic_data)

        return {"type": event_type, **traffic_data}
