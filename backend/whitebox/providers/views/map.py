from .base import BaseEmbeddableProviderCapabilityView


class MapProviderView(BaseEmbeddableProviderCapabilityView):
    plugin_capability_lookup = "map"
