from functools import cached_property

from django.template import loader
from django.http import HttpResponseBadRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView

from whitebox.templatetags.whitebox import tagged_url
from plugin.manager import plugin_manager
from plugin.utils import Plugin


class ProviderCapabilityViewMixin:
    plugin_capability_lookup = None

    @cached_property
    def provider_plugin(self) -> Plugin | None:
        if not self.plugin_capability_lookup:
            raise ValueError(
                "You need to either override the `plugin_capability_lookup` "
                "attribute or implement the `provider_plugin` property."
            )

        plugin = plugin_manager.get_plugin_for_capability(
            self.plugin_capability_lookup,
        )
        return plugin


class BaseEmbeddableProviderCapabilityView(ProviderCapabilityViewMixin, TemplateView):
    def get_template_name(self):
        template_name = self.provider_plugin.get_provider_template(
            self.plugin_capability_lookup,
        )
        return template_name

    def get_template_context(self, **kwargs):
        context = super().get_context_data(**kwargs)

        provider_context = self.provider_plugin.get_provider_template_context(
            self.plugin_capability_lookup,
        )
        context.update(provider_context)

        return context

    def render_template(self):
        template_name = self.get_template_name()
        context = self.get_template_context()

        rendered = loader.render_to_string(template_name, context)
        return rendered

    def tag_files(self, file_list):
        ignored_prefixes = ["http://", "https://", "ftp://"]
        tagged_files = []

        for file_path in file_list:
            ignored = any(file_path.startswith(prefix) for prefix in ignored_prefixes)
            if not ignored:
                file_path = tagged_url(file_path)

            tagged_files.append(file_path)

        return tagged_files

    def _get_css_files(self, provider):
        files = provider.get_css()

        for plugin in plugin_manager.get_plugins():
            if plugin.augments_plugin == provider.name:
                files += plugin.get_css()

        return files

    def _get_js_files(self, provider):
        files = provider.get_js()

        for plugin in plugin_manager.get_plugins():
            if plugin.augments_plugin == provider.name:
                files += plugin.get_js()

        return files

    def get(self, request, *args, **kwargs):
        provider = self.provider_plugin
        if not provider:
            return HttpResponseBadRequest("Provider not found.")

        css_files = self._get_css_files(provider)
        js_files = self._get_js_files(provider)

        response = JsonResponse(
            {
                "template": self.render_template(),
                "css": self.tag_files(css_files),
                "js": self.tag_files(js_files),
            }
        )
        return response
