from django.urls import path

from .views import provider_views


def build_provider_view_urlpatterns():
    urlpatterns = []

    for provider_name, view_class in provider_views.items():
        url_path = "{}/".format(provider_name)
        prepared_view = view_class.as_view()
        url_name = "provider-{}".format(provider_name)

        pattern = path(url_path, prepared_view, name=url_name)
        urlpatterns.append(pattern)

    return urlpatterns


app_name = "providers"

urlpatterns = build_provider_view_urlpatterns()
