from copy import deepcopy

from rest_framework import serializers


field_class_map = {
    "text": serializers.CharField,
    "password": serializers.CharField,
    "ip_address": serializers.IPAddressField,
}

default_kwargs_map = {
    "password": {
        "write_only": True,
    },
}


def get_default_kwargs_for_type(type_):
    if type_ not in default_kwargs_map:
        return {}

    return deepcopy(default_kwargs_map[type_])


def generate_dynamic_fields(configuration) -> dict:
    """
    Generate fields for a serializer based on a configuration.

    Parameters:
        configuration: The configuration for the fields.

    Returns:
        A dict of fields for the serializer.
    """
    generated = {}

    for field_name, field_config in configuration.items():
        field_class = field_class_map[field_config["type"]]
        field_kwargs = {
            **get_default_kwargs_for_type(field_config["type"]),
            "label": field_config["name"],
        }

        # These two are mutually exclusive, so they're assigned manually.
        # TODO: Add a check for this in tests for all the integrations
        if "required" in field_config:
            field_kwargs["required"] = field_config["required"]

        if "default" in field_config:
            field_kwargs["default"] = field_config["default"]

        field_instance = field_class(**field_kwargs)
        generated[field_name] = field_instance

    return generated
