import re


class URLString(str):
    pass


def format_url_quoted_string(s: str, format_strings: dict):
    for fmt, value in format_strings.items():
        s = re.sub(
            # Match case-insensitive %7b & %7d - the quoted version of { and }
            r"%7[bB]{fmt}%7[dD]".format(fmt=fmt),
            value,
            s,
        )

    return s


def deep_format(o, format_strings: dict):
    """
    Recursively format an object using format strings.

    Parameters:
        o: The object to format.
        format_strings: A mapping of format strings to use.

    Returns:
        The formatted object.
    """
    if isinstance(o, dict):
        # If data is a dictionary, recursively format each value
        return {k: deep_format(v, format_strings) for k, v in o.items()}
    elif isinstance(o, URLString):
        # Apply string formatting if the value is a URLString
        return format_url_quoted_string(o, format_strings)
    elif isinstance(o, str):
        # Apply string formatting if the value is a string
        return o.format(**format_strings)
    elif isinstance(o, list):
        # If data is a list, apply formatting to each element
        return [deep_format(item, format_strings) for item in o]
    else:
        # For other data types, return the value as is
        return o
