class SerializersActionMapMixin:
    """
    Mixin that provides a mapping between a serializer class and a model class.
    """

    serializers_action_map = {}

    def get_serializer_class(self):
        """
        Get the serializer class for the current action.

        Returns:
            The serializer class for the current action.
        """
        return self.serializers_action_map.get(
            self.action,
            self.serializer_class,
        )
