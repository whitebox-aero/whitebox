def human_file_size(num):
    prefix = ""
    units = ("K", "M", "G")

    for unit in units:
        if num < 1024.0:
            break

        prefix = unit
        num /= 1024.0

    return f"{num:.2f} {prefix}B"
