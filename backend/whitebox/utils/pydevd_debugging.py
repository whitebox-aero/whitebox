import os
import sys

# When Django is run with development server, with autoreload enabled, the
# autoreloader spawns a new process that Django (from my current understanding,
# could be wrong), uses for file change observation. The forking process seems
# to cause some confusion on pydevd's end, where it loses track of the original
# process. Somehow, this causes the server not to start at all, and neither of
# the processes do anything at this point.
#
# The flag below, IS_DJANGO_AUTORELOAD_PROCESS, is used to prevent pydevd from
# attaching to the autoreload process, and only attempting to do it in the main
# process. Considering that this is invoked in manage.py, it gets invoked before
# Django settings are loaded, so the flag definition below is a duplicate of the
# one in `settings.py`.
#
# This whole approach is a workaround to allow for PyCharm debugging of the
# process running within the dev containers, as PyCharm only supports the pydevd
# remote debugger at this point. It would be ideal to migrate to `debugpy`,
# which is what we're using for VSCode, but PyCharm does not support it at this
# point, and the PyCharm team hasn't given a confirmation on if/when this would
# be supported.
#
# More information on the issue can be found here:
# https://youtrack.jetbrains.com/issue/PY-63403


IS_DJANGO_AUTORELOAD_PROCESS = (
    "runserver" in sys.argv
    and "--noreload" not in sys.argv
    and os.environ.get("RUN_MAIN", None) != "true"
)


def attach_pydevd():
    if not os.environ.get("PYDEVD_ENABLED") == "1":
        return

    try:
        import pydevd_pycharm
    except ImportError as e:
        error_message = (
            "pydevd-pycharm is not installed. "
            "Check the README for instructions on how to install it."
        )
        raise ImportError(error_message) from e

    if IS_DJANGO_AUTORELOAD_PROCESS:
        print("Skipping pydevd attachment in autoreload process")
        return

    debug_server = os.environ.get("PYDEVD_SERVER") or "host.docker.internal"
    debug_port = os.environ.get("PYDEVD_PORT") or 56789

    pydevd_pycharm.settrace(
        debug_server,
        port=int(debug_port),
        stdoutToServer=True,
        stderrToServer=True,
    )
