class DeviceException(Exception):
    pass


class DeviceConnectionException(DeviceException):
    pass
