from unittest.mock import patch
from django.test import TestCase

from status.services import StatusService


class TestStatusService(TestCase):
    @patch("status.services.StatusService.channel_layer.group_send")
    async def test_emit_status_update(self, mock_group_send):
        await StatusService.emit_status_update(
            {
                "GPS_connected": True,
                "GPS_solution": "No Fix",
                "GPS_detected_type": 25,
            }
        )

        mock_group_send.assert_called_once_with(
            "flight",
            {
                "type": "status_update",
                "data": {
                    "GPS_connected": True,
                    "GPS_solution": "No Fix",
                    "GPS_detected_type": 25,
                },
            },
        )
