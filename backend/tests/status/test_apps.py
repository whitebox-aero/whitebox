from django.test import TestCase
from django.apps import apps

from status.apps import StatusConfig


class TestStatusConfig(TestCase):
    def test_app_name(self):
        self.assertEqual(StatusConfig.name, "status")

    def test_default_auto_field(self):
        self.assertEqual(
            StatusConfig.default_auto_field, "django.db.models.BigAutoField"
        )

    def test_app_config_type(self):
        app_config = apps.get_app_config("status")
        self.assertIsInstance(app_config, StatusConfig)

    def test_app_config_label(self):
        app_config = apps.get_app_config("status")
        self.assertEqual(app_config.label, "status")

    def test_app_config_models_module(self):
        app_config = apps.get_app_config("status")
        self.assertIsNone(app_config.models_module)
