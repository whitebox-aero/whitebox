from django.test import TestCase
from unittest.mock import patch, AsyncMock

from status.handlers import StatusUpdateHandler


class TestHandlers(TestCase):
    @patch("whitebox.events.event_emitter.emit_event", new_callable=AsyncMock)
    async def test_handle(self, mock_emit_traffic_update):
        data = {
            "type": "status_update",
            "data": {
                "GPS_connected": True,
                "GPS_solution": "No Fix",
                "GPS_detected_type": 25,
            },
        }

        handler = StatusUpdateHandler()
        response = await handler.handle(data)

        self.assertEqual(response["type"], "status_update")
        self.assertEqual(response["GPS_connected"], True)
        self.assertEqual(response["GPS_solution"], "No Fix")
        self.assertEqual(response["GPS_detected_type"], 25)

        mock_emit_traffic_update.assert_awaited_once_with(
            "status_update",
            {
                "GPS_connected": True,
                "GPS_solution": "No Fix",
                "GPS_detected_type": 25,
            },
        )
