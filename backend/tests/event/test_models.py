import reversion

from django.test import TestCase
from django.db import models
from django.utils import timezone
from reversion.models import Version
from datetime import timedelta

from event.models import Event


class TestEventModel(TestCase):
    def setUp(self):
        self.event_time = timezone.now()
        with reversion.create_revision():
            self.event = Event.objects.create(timestamp=self.event_time)

    def test_event_creation(self):
        self.assertTrue(isinstance(self.event, Event))
        self.assertEqual(self.event.__str__(), f"Event at {self.event_time}")

    def test_timestamp_field(self):
        field = Event._meta.get_field("timestamp")
        self.assertTrue(isinstance(field, models.DateTimeField))

    def test_meta_attributes(self):
        self.assertEqual(Event._meta.app_label, "event")
        self.assertEqual(Event._meta.db_table, "event")

    def test_reversion_registration(self):
        self.assertTrue(reversion.is_registered(Event))

    def test_event_update(self):
        new_time = timezone.now()
        with reversion.create_revision():
            self.event.timestamp = new_time
            self.event.save()

        updated_event = Event.objects.get(id=self.event.id)
        self.assertEqual(updated_event.timestamp, new_time)

    def test_reversion_history(self):
        new_time = timezone.now()
        with reversion.create_revision():
            self.event.timestamp = new_time
            self.event.save()

        versions = Version.objects.get_for_object(self.event)
        self.assertEqual(versions.count(), 2)  # Original version + 1 update
        self.assertAlmostEqual(
            versions[0].field_dict["timestamp"], new_time, delta=timedelta(seconds=1)
        )
        self.assertAlmostEqual(
            versions[1].field_dict["timestamp"],
            self.event_time,
            delta=timedelta(seconds=1),
        )

    def test_event_deletion(self):
        event_id = self.event.id
        self.event.delete()
        with self.assertRaises(Event.DoesNotExist):
            Event.objects.get(id=event_id)
