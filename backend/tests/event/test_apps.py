from django.test import TestCase
from django.apps import apps

from event.apps import EventConfig


class TestEventConfig(TestCase):
    def test_app_name(self):
        self.assertEqual(EventConfig.name, "event")

    def test_default_auto_field(self):
        self.assertEqual(
            EventConfig.default_auto_field, "django.db.models.BigAutoField"
        )

    def test_app_config_type(self):
        app_config = apps.get_app_config("event")
        self.assertIsInstance(app_config, EventConfig)

    def test_app_config_label(self):
        app_config = apps.get_app_config("event")
        self.assertEqual(app_config.label, "event")

    def test_app_config_models_module(self):
        app_config = apps.get_app_config("event")
        self.assertIsNotNone(app_config.models_module)
