from django.test import TestCase, Client
from django.urls import reverse
from unittest.mock import patch
from django.http import JsonResponse
from plugin.manager import plugin_manager


class TestRefreshPluginsView(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse("refresh_plugins")

    @patch.object(plugin_manager, "discover_plugins")
    def test_refresh_plugins_success(self, mock_discover_plugins):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.json(), {"status": "success"})
        mock_discover_plugins.assert_called_once()
