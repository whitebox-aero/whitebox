import json
from io import StringIO
from unittest.mock import (
    patch,
    mock_open,
    MagicMock,
)

from django.test import TestCase
from django.core.management import call_command, CommandError


valid_manifest = json.dumps(
    {
        "sources": [
            {
                "url": "http://example.com/file1",
                "integrity": "sha256-123456",
                "target_path": "path/to/file1",
            },
            {
                "url": "http://example.com/file2",
                "integrity": "sha256-7890ab",
                "target_path": "path/to/file2",
            },
        ]
    }
)

invalid_manifest_missing_field = json.dumps(
    {"sources": [{"url": "http://example.com/file1", "target_path": "path/to/file1"}]}
)

invalid_json = "{ invalid json }"


class TestVerifyExternalAssetManifestCommand(TestCase):
    def _call(self, *args, **kwargs):
        return call_command("verify_external_asset_manifest", *args, **kwargs)

    @patch("builtins.open", new_callable=mock_open, read_data="")
    def test_supplied_both_target_file_and_module_name(self, mock_open_):
        with self.assertRaises(CommandError) as cm:
            self._call(TARGET_FILE="asdf", module_name="asdf")

        self.assertEqual(
            "TARGET_FILE and --module-name are mutually exclusive",
            str(cm.exception),
        )

    @patch("builtins.open", new_callable=mock_open, read_data="")
    def test_missing_target_file_and_module_name(self, mock_open_):
        with self.assertRaises(CommandError) as cm:
            self._call()

        self.assertEqual(
            "Either TARGET_FILE or --module-name must be provided",
            str(cm.exception),
        )

    @patch("builtins.open", new_callable=mock_open, read_data="")
    def test_target_file_not_found(self, mock_open_):
        mock_open_.side_effect = FileNotFoundError

        with self.assertRaises(CommandError) as cm:
            self._call(TARGET_FILE="missing.json")

        self.assertEqual("File not found", str(cm.exception))

    @patch("importlib.resources.open_text")
    def test_module_not_found(self, mock_open_text):
        mock_open_text.side_effect = ModuleNotFoundError

        with self.assertRaises(CommandError) as cm:
            self._call(module_name="nonexistent_module")

        self.assertEqual(
            "Module nonexistent_module not found",
            str(cm.exception),
        )

    @patch("builtins.open", new_callable=mock_open, read_data="{}")
    def test_manifest_missing_sources(self, mock_open_):
        with self.assertRaises(CommandError) as cm:
            self._call(TARGET_FILE="manifest.json")

        self.assertEqual(
            'Manifest file is missing required root field: "source"',
            str(cm.exception),
        )

    @patch(
        "builtins.open",
        new_callable=mock_open,
        read_data=invalid_manifest_missing_field,
    )
    def test_manifest_missing_fields_in_source(self, mock_open_):
        with self.assertRaises(CommandError) as cm:
            self._call(TARGET_FILE="manifest.json")

        self.assertIn(
            "Manifest file is missing required field 'integrity' in source with index 0",
            str(cm.exception),
        )

    @patch("builtins.open", new_callable=mock_open, read_data=invalid_json)
    def test_manifest_invalid_json(self, mock_open_):
        with self.assertRaises(CommandError) as cm:
            self._call(TARGET_FILE="manifest.json")

        self.assertEqual(
            "Manifest file is not a valid JSON file!",
            str(cm.exception),
        )

    @patch("builtins.open", new_callable=mock_open, read_data=valid_manifest)
    def test_valid_manifest(self, mock_open_):
        with patch("sys.stdout", new_callable=StringIO) as mock_stdout:
            self._call(TARGET_FILE="manifest.json")

        self.assertIn("Manifest file is valid!", mock_stdout.getvalue())
