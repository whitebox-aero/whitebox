import time
import asyncio

from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch, MagicMock, call
from io import StringIO
from django.test import TestCase
from django.core.management import call_command
from django.core.management.base import CommandError

from plugin.management.commands.download_external_assets import (
    Command,
    DownloadActivityMonitor,
)
from plugin.asset_manager.manager import DownloadTracker


class TestDownloadActivityMonitorDetailed(TestCase):
    def setUp(self):
        self.monitor = DownloadActivityMonitor()

    def test_monitor_initialization_with_custom_values(self):
        custom_monitor = DownloadActivityMonitor(
            min_speed_threshold=2048, inactivity_timeout=60, slow_connection_period=120
        )

        self.assertEqual(custom_monitor.min_speed_threshold, 2048)
        self.assertEqual(custom_monitor.inactivity_timeout, 60)
        self.assertEqual(custom_monitor.slow_connection_period, 120)
        self.assertEqual(custom_monitor.last_byte_count, 0)
        self.assertFalse(custom_monitor.is_started)
        self.assertEqual(custom_monitor.history_timestamps, [])
        self.assertEqual(custom_monitor.history_byte_counts, [])

    def test_monitor_update_sequence(self):
        # First update (initialization)
        now = time.time()

        with patch("time.time", return_value=now):
            is_timeout, reason = self.monitor.update(1000)
            self.assertFalse(is_timeout)
            self.assertIsNone(reason)
            self.assertTrue(self.monitor.is_started)
            self.assertEqual(self.monitor.last_byte_count, 1000)

        # Second update (10 seconds later, good progress)
        with patch("time.time", return_value=now + 10):
            is_timeout, reason = self.monitor.update(5000)
            self.assertFalse(is_timeout)
            self.assertIsNone(reason)
            self.assertEqual(self.monitor.last_byte_count, 5000)

        # Third update (20 seconds later, good progress)
        with patch("time.time", return_value=now + 30):
            is_timeout, reason = self.monitor.update(10000)
            self.assertFalse(is_timeout)
            self.assertIsNone(reason)
            self.assertEqual(self.monitor.last_byte_count, 10000)

    def test_monitor_slow_connection_not_enough_history(self):
        # First update
        now = time.time()
        with patch("time.time", return_value=now):
            self.monitor.update(1000)

        # Second update with slow progress but not enough history time (only 10 seconds)
        with patch("time.time", return_value=now + 10):
            is_timeout, reason = self.monitor.update(
                1100
            )  # Only 100 bytes in 10 seconds

            # Should not timeout because we need at least 30 seconds of history
            self.assertFalse(is_timeout)
            self.assertIsNone(reason)

    def test_monitor_history_pruning(self):
        now = time.time()

        # Populate history with entries from various times
        self.monitor.history_timestamps = [
            now - 200,  # 200 seconds ago (will be removed)
            now - 150,  # 150 seconds ago (will be removed)
            now - 100,  # 100 seconds ago (will be removed)
            now - 50,  # 50 seconds ago (within period, will be kept)
            now - 30,  # 30 seconds ago (within period, will be kept)
        ]
        self.monitor.history_byte_counts = [100, 200, 300, 400, 500]
        self.monitor.is_started = True

        # Update monitor
        with patch("time.time", return_value=now):
            self.monitor.update(600)

            # Only entries within the monitoring period (60s) should remain
            self.assertEqual(len(self.monitor.history_timestamps), 3)
            self.assertEqual(self.monitor.history_byte_counts, [400, 500, 600])

    def test_borderline_slow_connection(self):
        # Setup history with download speed just at threshold
        now = time.time()
        seconds_ago = 50
        bytes_downloaded = self.monitor.min_speed_threshold * seconds_ago

        self.monitor.history_timestamps = [now - seconds_ago]
        self.monitor.history_byte_counts = [0]
        self.monitor.last_timestamp = now - seconds_ago
        self.monitor.last_byte_count = 0
        self.monitor.is_started = True

        # Update with bytes that make speed exactly at threshold
        with patch("time.time", return_value=now):
            is_timeout, reason = self.monitor.update(bytes_downloaded)

            # Should not timeout since speed is at threshold
            self.assertFalse(is_timeout)
            self.assertIsNone(reason)


class TestDownloadExternalAssetsCommand(TestCase):
    def setUp(self):
        self.command = Command()
        self.command.download_timeout = 2
        self.command.check_download_status_every = 0.1

    def _call(self, *args, **kwargs):
        return call_command("download_external_assets", *args, **kwargs)

    @patch("time.strftime")
    def test_get_message(self, mock_strftime):
        mock_strftime.return_value = "12:34:56"
        message = self.command._get_message("test message")
        self.assertEqual(message, "[12:34:56] test message")
        mock_strftime.assert_called_once_with("%H:%M:%S")

    def test_log_success(self):
        out = StringIO()
        self.command.stdout = out

        mock_style = MagicMock()
        mock_style.SUCCESS.side_effect = lambda x: f"{x}"
        self.command.style = mock_style

        with patch("time.strftime", return_value="12:34:56"):
            self.command._log_success("well done")

        expected = "[12:34:56] well done"
        self.assertEqual(out.getvalue(), expected)
        mock_style.SUCCESS.assert_called_once_with("[12:34:56] well done")

    def test_log_error(self):
        out = StringIO()
        self.command.stderr = out

        mock_style = MagicMock()
        mock_style.ERROR.side_effect = lambda x: f"{x}"
        self.command.style = mock_style

        with patch("time.strftime", return_value="12:34:56"):
            self.command._log_error("oh no")

        expected = "[12:34:56] oh no"
        self.assertEqual(out.getvalue(), expected)
        mock_style.ERROR.assert_called_once_with("[12:34:56] oh no")

    def test_log(self):
        out = StringIO()
        self.command.stdout = out

        with patch("time.strftime", return_value="12:34:56"):
            self.command._log("hello")

        expected = "[12:34:56] hello"
        self.assertEqual(out.getvalue(), expected)

    def test_command_requires_arguments(self):
        with self.assertRaises(CommandError) as ctx:
            self._call()
        self.assertIn("Please specify --core or --plugins", str(ctx.exception))

    @patch(
        "plugin.asset_manager.manager.AssetManager.schedule_missing_core_assets_download"
    )
    def test_download_core_assets(self, mock_schedule):
        def mock_download_complete(*args, **kwargs):
            self.command.download_complete_event.set()

        mock_schedule.side_effect = mock_download_complete
        self.command.handle(core=True, plugins=None)
        mock_schedule.assert_called_once()

    @patch("plugin.manager.plugin_manager._get_discovered_plugins")
    @patch(
        "plugin.asset_manager.manager.AssetManager.schedule_missing_plugin_assets_download"
    )
    def test_download_specific_plugins(self, mock_schedule, mock_get_plugins):
        mock_get_plugins.return_value = {
            "plugin_one": MagicMock(),
            "plugin_two": MagicMock(),
        }

        def mock_download_complete(*args, **kwargs):
            self.command.download_complete_event.set()

        mock_schedule.side_effect = mock_download_complete

        self.command.handle(core=False, plugins=["plugin_one"])

        mock_schedule.assert_called_once_with("plugin_one")
        mock_get_plugins.assert_called_once()

    @patch("plugin.manager.plugin_manager._get_discovered_plugins")
    @patch(
        "plugin.asset_manager.manager.AssetManager.schedule_missing_plugin_assets_download"
    )
    def test_download_all_plugins(self, mock_schedule, mock_get_plugins):
        mock_get_plugins.return_value = {
            "plugin_one": MagicMock(),
            "plugin_two": MagicMock(),
        }

        def mock_download_complete(*args, **kwargs):
            self.command.download_complete_event.set()

        mock_schedule.side_effect = mock_download_complete
        self.command.handle(core=False, plugins=["all"])
        self.assertEqual(mock_schedule.call_count, 2)
        mock_get_plugins.assert_called_once()

    @patch(
        "plugin.management.commands.download_external_assets.Command._download_plugin_external_assets"
    )
    def test_plugin_name_cleaning(self, mock_download):
        def mock_complete(*args, **kwargs):
            self.command.download_complete_event.set()

        mock_download.side_effect = mock_complete
        self.command.handle(core=False, plugins=["plugin-one"])
        mock_download.assert_called_once_with(["plugin_one"])

    def test_add_to_track(self):
        self.command.track_downloads_for = []
        self.command._add_to_track("module1")
        self.assertEqual(self.command.track_downloads_for, ["module1"])


class TestCommandTrackDownloads(IsolatedAsyncioTestCase):
    async def test_track_downloads_with_timeout(self):
        command = Command()
        command.track_downloads_for = ["test_module"]
        command.check_download_status_every = 0.1

        # Create a tracker with progress that will trigger timeout
        tracker = DownloadTracker("test_module")
        tracker.progress = 50
        tracker.downloaded_size = 1000

        # Mock asset manager
        asset_manager = MagicMock()
        asset_manager._download_status = {"test_module": tracker}

        # Create a monitor that will detect timeout
        monitor = MagicMock()
        monitor.update.return_value = (True, "Test timeout reason")
        command.download_monitors = {"test_module": monitor}

        # Create progress bar
        progress_bar = MagicMock()
        command.progress_bars = {"test_module": progress_bar}

        def set_progress(*args, **kwargs):
            tracker.progress = 100

        with patch("asyncio.sleep", side_effect=set_progress):
            with patch(
                "plugin.management.commands.download_external_assets.asset_manager",
                asset_manager,
            ):
                await command._track_downloads()

                # Should set the download failed event
                self.assertIn("Test timeout reason", command.download_errors[0])
                self.assertTrue(command.download_failed_event.is_set())

    async def test_track_downloads_with_completion(self):
        command = Command()
        command.track_downloads_for = ["test_module"]
        command.check_download_status_every = 0.1

        # Create a tracker with 100% progress
        tracker = DownloadTracker("test_module")
        tracker.progress = 100
        tracker.downloaded_size = 1000

        # Mock asset manager
        asset_manager = MagicMock()
        asset_manager._download_status = {"test_module": tracker}

        # Run the track downloads method
        with patch(
            "plugin.management.commands.download_external_assets.asset_manager",
            asset_manager,
        ):
            await command._track_downloads()

            # Should set the download complete event
            self.assertTrue(command.download_complete_event.is_set())

    async def test_track_downloads_no_progress_info(self):
        command = Command()
        command.track_downloads_for = ["test_module"]
        command.check_download_status_every = 0.1

        # Create a tracker with None progress (indicates failure)
        tracker = DownloadTracker("test_module")
        tracker.progress = None

        # Mock asset manager
        asset_manager = MagicMock()
        asset_manager._download_status = {"test_module": tracker}

        # Run the track downloads method
        with patch(
            "plugin.management.commands.download_external_assets.asset_manager",
            asset_manager,
        ):
            # Need to stop the method after one iteration since it will keep running
            async def stop_after_one_iteration():
                await asyncio.sleep(0.2)
                # Simulate completion of other downloads
                new_tracker = DownloadTracker("other_module")
                new_tracker.progress = 100
                asset_manager._download_status["other_module"] = new_tracker
                command.track_downloads_for.append("other_module")

            await asyncio.gather(command._track_downloads(), stop_after_one_iteration())

            # Should record an error
            self.assertIn("test_module", command.download_errors[0])


class TestInitializeProgressBars(TestCase):
    def test_initialize_with_known_size(self):
        command = Command()
        command.track_downloads_for = ["module1"]
        command.tqdm_kwargs = {"unit": "B"}

        # Create a tracker with known size
        tracker = MagicMock()
        tracker.expected_size = 1000

        # Mock asset manager
        asset_manager = MagicMock()
        asset_manager._download_status = {"module1": tracker}

        with patch(
            "plugin.management.commands.download_external_assets.asset_manager",
            asset_manager,
        ):
            with patch(
                "plugin.management.commands.download_external_assets.tqdm"
            ) as mock_tqdm:
                command._initialize_progress_bars()

                # Should create one progress bar
                mock_tqdm.assert_called_once()

                # Should use the tracker's expected size
                args, kwargs = mock_tqdm.call_args
                self.assertEqual(kwargs["total"], 1000)

    def test_initialize_with_unknown_size(self):
        command = Command()
        command.track_downloads_for = ["module1"]
        command.tqdm_kwargs = {"unit": "B"}

        # Create a tracker with unknown size
        tracker = MagicMock()
        tracker.expected_size = 0

        # Mock asset manager
        asset_manager = MagicMock()
        asset_manager._download_status = {"module1": tracker}

        with patch(
            "plugin.management.commands.download_external_assets.asset_manager",
            asset_manager,
        ):
            with patch(
                "plugin.management.commands.download_external_assets.tqdm"
            ) as mock_tqdm:
                command._initialize_progress_bars()

                # Should create one progress bar
                mock_tqdm.assert_called_once()

                # Should set status to indicate unknown size
                mock_tqdm.return_value.set_postfix.assert_called_with(
                    status="Size unknown"
                )


class TestCommandWaitForDownloads(TestCase):
    def test_wait_for_successful_completion(self):
        command = Command()
        command.check_download_status_every = 0.1
        command.progress_bars = {
            "module1": MagicMock(),
            "module2": MagicMock(),
        }

        # Simulate download complete event
        def set_complete(*args, **kwargs):
            command.download_complete_event.set()

        with patch("time.sleep", side_effect=set_complete):
            with patch.object(command, "_log_success") as mock_log:
                command._wait_for_downloads()

                # Should close all progress bars
                for bar in command.progress_bars.values():
                    bar.close.assert_called_once()

                # Should log success message
                mock_log.assert_called_with(
                    "All asset downloads completed successfully"
                )

    def test_wait_with_download_errors(self):
        """Test waiting for downloads with errors."""
        command = Command()
        command.check_download_status_every = 0.1
        command.progress_bars = {
            "module1": MagicMock(),
        }
        command.download_errors = ["Error 1", "Error 2"]

        # Simulate download complete event
        def set_complete(*args, **kwargs):
            command.download_complete_event.set()

        with patch("time.sleep", side_effect=set_complete):
            with patch.object(command, "_log_error") as mock_log:
                command._wait_for_downloads()

                # Should close all progress bars
                for bar in command.progress_bars.values():
                    bar.close.assert_called_once()

                # Should log each error
                self.assertEqual(mock_log.call_count, 2)
                mock_log.assert_has_calls(
                    [
                        call("Error 1"),
                        call("Error 2"),
                    ]
                )

    def test_wait_with_failure_event(self):
        """Test waiting when download failed event is set."""
        command = Command()
        command.check_download_status_every = 0.1
        command.progress_bars = {
            "module1": MagicMock(),
        }

        # Simulate download failed event
        def set_failed(*args, **kwargs):
            command.download_failed_event.set()

        with patch("time.sleep", side_effect=set_failed):
            command._wait_for_downloads()

            # Should close all progress bars
            for bar in command.progress_bars.values():
                bar.close.assert_called_once()
