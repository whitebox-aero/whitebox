from unittest.mock import patch

from django.test import TestCase

from plugin.handlers import PluginDownloadHandler


class TestPluginDownloadHandler(TestCase):
    def setUp(self):
        self.handler = PluginDownloadHandler()
        self.base_handler_data = {
            "type": "plugin_download",
            "plugin_name": "test",
        }

    async def test_handle_status_started(self):
        data = {
            **self.base_handler_data,
            "status": "started",
        }

        response = await self.handler.handle(data)

        expected = {
            **self.base_handler_data,
            "status": "started",
        }
        self.assertEqual(expected, response)

    async def test_handle_status_finished(self):
        data = {
            **self.base_handler_data,
            "status": "finished",
        }

        response = await self.handler.handle(data)

        expected = {
            **self.base_handler_data,
            "status": "finished",
        }
        self.assertEqual(expected, response)

    async def test_handle_status_downloading(self):
        data = {
            **self.base_handler_data,
            "status": "downloading",
            "total_size": 2048,
        }

        response = await self.handler.handle(data)

        expected = {
            **self.base_handler_data,
            "status": "downloading",
            "total_size": data["total_size"],
        }
        self.assertEqual(expected, response)

    async def test_handle_status_update(self):
        data = {
            **self.base_handler_data,
            "status": "update",
            "downloaded_size": 1024,
            "total_size": 2048,
            "progress": 50.0,
        }

        response = await self.handler.handle(data)

        expected = {
            **self.base_handler_data,
            "status": "update",
            "downloaded_size": "1.00 KB",
            "total_size": "2.00 KB",
            "progress": 50.0,
        }
        self.assertEqual(expected, response)

    async def test_handle_unknown_status(self):
        data = {
            **self.base_handler_data,
            "status": "whatever-else",
        }

        with patch("logging.Logger.warning") as mock_warning:
            response = await self.handler.handle(data)

        mock_warning.assert_called_once_with(
            f"Received unknown `status`: {data['status']}"
        )

        expected = {
            **self.base_handler_data,
            "status": "whatever-else",
        }
        self.assertEqual(expected, response)
