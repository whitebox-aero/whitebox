from django.test import TestCase
from django.apps import apps

from plugin.apps import PluginConfig


class TestPluginConfig(TestCase):
    def test_app_name(self):
        self.assertEqual(PluginConfig.name, "plugin")

    def test_default_auto_field(self):
        self.assertEqual(
            PluginConfig.default_auto_field, "django.db.models.BigAutoField"
        )

    def test_app_config_type(self):
        app_config = apps.get_app_config("plugin")
        self.assertIsInstance(app_config, PluginConfig)

    def test_app_config_label(self):
        app_config = apps.get_app_config("plugin")
        self.assertEqual(app_config.label, "plugin")

    def test_app_config_models_module(self):
        app_config = apps.get_app_config("plugin")
        self.assertIsNone(app_config.models_module)
