from django.test import TestCase
from django.urls import reverse, resolve

from plugin import views


class UrlsTest(TestCase):
    def test_refresh_plugins_url(self):
        url = reverse("refresh_plugins")
        self.assertEqual(url, "/plugins/refresh/")
        resolver = resolve("/plugins/refresh/")
        self.assertEqual(resolver.func, views.refresh_plugins)
