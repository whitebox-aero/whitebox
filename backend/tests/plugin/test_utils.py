import unittest
import asyncio

from unittest.mock import patch, MagicMock, AsyncMock
from typing import Protocol

from plugin.utils import WhiteboxStandardAPI, Plugin
from whitebox.api import API


class TestWhiteboxStandardAPI(unittest.TestCase):
    def setUp(self):
        self.api = WhiteboxStandardAPI()

    def test_api_attribute(self):
        self.assertEqual(self.api.api, API)

    @patch("plugin.manager.plugin_manager")
    def test_register_event_callback(self, mock_plugin_manager):
        event_type = "test_event"
        callback = MagicMock()

        WhiteboxStandardAPI.register_event_callback(event_type, callback)

        mock_plugin_manager.register_event_callback.assert_called_once_with(
            event_type, callback
        )

    @patch("plugin.manager.plugin_manager")
    def test_unregister_event_callback(self, mock_plugin_manager):
        event_type = "test_event"
        callback = MagicMock()

        self.api.unregister_event_callback(event_type, callback)

        mock_plugin_manager.unregister_event_callback.assert_called_once_with(
            event_type, callback
        )

    @patch("plugin.manager.plugin_manager")
    def test_emit_event(self, mock_plugin_manager):
        mock_plugin_manager.emit_event = AsyncMock()
        asyncio.run(self.api.emit_event("test_event"))
        mock_plugin_manager.emit_event.assert_called_once_with("test_event", None)


class TestPlugin(unittest.TestCase):
    def test_plugin_protocol(self):
        # Test that Plugin is a runtime checkable protocol
        self.assertTrue(isinstance(Plugin, type(Protocol)))

    def test_whitebox_attribute(self):
        class TestPlugin(Plugin):
            pass

        plugin = TestPlugin()
        self.assertIsInstance(plugin.whitebox, WhiteboxStandardAPI)

    def test_default_attributes(self):
        class TestPlugin(Plugin):
            pass

        plugin = TestPlugin()
        self.assertEqual(plugin.plugin_css, [])
        self.assertEqual(plugin.plugin_js, [])

    def test_get_css(self):
        class TestPlugin(Plugin):
            plugin_css = ["style1.css", "style2.css"]

        plugin = TestPlugin()
        self.assertEqual(plugin.get_css(), ["style1.css", "style2.css"])

    def test_get_js(self):
        class TestPlugin(Plugin):
            plugin_js = ["script1.js", "script2.js"]

        plugin = TestPlugin()
        self.assertEqual(plugin.get_js(), ["script1.js", "script2.js"])
