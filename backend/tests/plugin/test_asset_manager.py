import hashlib
import time
import asyncio

from dataclasses import replace
from io import StringIO, BytesIO
from pathlib import Path
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import patch, AsyncMock, mock_open, MagicMock, call

from django.test import override_settings
from aioresponses import aioresponses

from plugin.asset_manager import AssetFileSpec
from plugin.asset_manager.manager import DownloadTracker, AssetManager
from plugin.exceptions import AssetManifestParseError, AssetManifestError


class TestAssetFileSpec(TestCase):
    def test_from_dict(self):
        file_spec = AssetFileSpec.from_dict(
            data={
                "target_path": "file.txt",
                "url": "http://example.com/file",
                "integrity": "sha256-123456",
            },
            module_name="plugin",
        )
        self.assertEqual(file_spec.module_name, "plugin")
        self.assertEqual(file_spec.target_path, "file.txt")
        self.assertEqual(file_spec.url, "http://example.com/file")
        self.assertEqual(file_spec.integrity, "sha256-123456")

    def test_get_hashing_class(self):
        kwargs = {
            "module_name": "plugin",
            "target_path": "file.txt",
            "url": "http://example.com/file",
        }
        file_spec_1 = AssetFileSpec(
            **kwargs,
            integrity="sha1-123456",
        )
        file_spec_256 = AssetFileSpec(
            **kwargs,
            integrity="sha256-123456",
        )
        file_spec_unknown = AssetFileSpec(
            **kwargs,
            integrity="random-123456",
        )

        self.assertEqual(file_spec_1.get_hashing_class(), hashlib.sha1)
        self.assertEqual(file_spec_256.get_hashing_class(), hashlib.sha256)
        self.assertEqual(file_spec_unknown.get_hashing_class(), None)

    def test_get_expected_file_hash(self):
        integrity_value = "hashtype-hashvalue"

        file_spec = AssetFileSpec(
            module_name="plugin",
            target_path="file.txt",
            url="http://example.com/file",
            integrity=integrity_value,
        )

        expected_file_hash = integrity_value.split("-")[1]
        obtained_file_hash = file_spec.get_expected_file_hash()

        self.assertEqual(
            expected_file_hash,
            obtained_file_hash,
        )


class TestDownloadTracker(IsolatedAsyncioTestCase):
    def setUp(self):
        self.plugin_name = "test_plugin"
        self.tracker = DownloadTracker(self.plugin_name)

    def test_add_files(self):
        file_specs = [
            AssetFileSpec(
                module_name="test",
                target_path="file1.txt",
                url="http://example.com/file1",
                integrity=None,
            ),
            AssetFileSpec(
                module_name="test",
                target_path="file2.txt",
                url="http://example.com/file2",
                integrity=None,
            ),
        ]
        file_sizes = [1000, 2000]

        self.tracker.add_files(file_specs, file_sizes)

        self.assertEqual(self.tracker.total_files, 2)
        self.assertEqual(self.tracker.expected_size, 3000)

    def test_add_files_with_none_sizes(self):
        file_specs = [
            AssetFileSpec(
                module_name="test",
                target_path="file1.txt",
                url="http://example.com/file1",
                integrity=None,
            ),
            AssetFileSpec(
                module_name="test",
                target_path="file2.txt",
                url="http://example.com/file2",
                integrity=None,
            ),
        ]
        file_sizes = [1000, None]

        self.tracker.add_files(file_specs, file_sizes)

        self.assertEqual(self.tracker.total_files, 2)
        self.assertEqual(self.tracker.expected_size, 1000)

    def test_calculate_progress_file_component_only(self):
        self.tracker.total_files = 10
        self.tracker.files_completed = 5

        self.tracker._calculate_progress()

        self.assertEqual(self.tracker.progress, 50.0)

    def test_calculate_progress_with_size_component(self):
        self.tracker.total_files = 10
        self.tracker.files_completed = 5
        self.tracker.expected_size = 1000
        self.tracker.downloaded_size = 700

        self.tracker._calculate_progress()

        # Progress should be weighted: (50 * 0.6) + (70 * 0.4) = 30 + 28 = 58
        self.assertEqual(self.tracker.progress, 58.0)

    def test_calculate_progress_all_files_completed(self):
        self.tracker.total_files = 10
        self.tracker.files_completed = 10
        self.tracker.expected_size = 1000
        self.tracker.downloaded_size = 900

        self.tracker._calculate_progress()

        # When all files are completed, progress should be 100%
        self.assertEqual(self.tracker.progress, 100.0)

    def test_calculate_progress_downloaded_more_than_expected(self):
        self.tracker.total_files = 10
        self.tracker.files_completed = 5
        self.tracker.expected_size = 1000
        self.tracker.downloaded_size = 1200

        self.tracker._calculate_progress()

        # Downloaded more than expected, progress should adjust expected size
        self.assertEqual(self.tracker.expected_size, 1200)

    async def test_update(self):
        self.tracker.total_files = 1
        self.tracker.expected_size = 1000

        with patch.object(self.tracker, "emit_update", new=AsyncMock()) as mock_emit:
            await self.tracker.update(500)

            self.assertEqual(self.tracker.downloaded_size, 500)
            mock_emit.assert_called_once()

    async def test_file_completed(self):
        self.tracker.total_files = 2
        self.tracker.files_completed = 0

        with patch.object(self.tracker, "emit_update", new=AsyncMock()) as mock_emit:
            await self.tracker.file_completed()

            self.assertEqual(self.tracker.files_completed, 1)
            mock_emit.assert_called_once()

    async def test_emit(self):
        manager = AssetManager()
        manager.enable_emit_status()
        with patch.object(
            manager,
            "_channel_layer",
            new=AsyncMock(),
        ) as mock_channel_layer:
            await self.tracker._emit(
                status="launch",
                good=1,
                evening=2,
                night=3,
                city=4,
            )

        mock_channel_layer.group_send.assert_called_with(
            "management",
            {
                "type": "plugin.download",
                "plugin_name": self.plugin_name,
                "status": "launch",
                "good": 1,
                "evening": 2,
                "night": 3,
                "city": 4,
            },
        )

    async def test_emit_started(self):
        with patch.object(
            self.tracker,
            "_emit",
            new=AsyncMock(),
        ) as mock_emit:
            await self.tracker.emit_started()
            mock_emit.assert_awaited_once_with("started")

    async def test_emit_finished(self):
        with patch.object(
            self.tracker,
            "_emit",
            new=AsyncMock(),
        ) as mock_emit:
            await self.tracker.emit_finished()
            mock_emit.assert_awaited_once_with("finished")

    async def test_emit_failed(self):
        with patch.object(
            self.tracker,
            "_emit",
            new=AsyncMock(),
        ) as mock_emit:
            await self.tracker.emit_failed(1234)
            mock_emit.assert_awaited_once_with(
                "failed",
                files_failed_to_download=1234,
            )

    async def test_emit_downloading(self):
        self.tracker.expected_size = 1000
        self.tracker.total_files = 5

        with patch.object(self.tracker, "_emit", new=AsyncMock()) as mock_emit:
            await self.tracker.emit_downloading()

            mock_emit.assert_awaited_once_with(
                "downloading",
                total_size=1000,
                total_files=5,
            )

    async def test_emit_update(self):
        self.tracker.progress = 75.5
        self.tracker.downloaded_size = 750
        self.tracker.expected_size = 1000
        self.tracker.files_completed = 3
        self.tracker.total_files = 4

        with patch.object(self.tracker, "_emit", new=AsyncMock()) as mock_emit:
            await self.tracker.emit_update()

            mock_emit.assert_awaited_once_with(
                "update",
                progress=75.5,
                downloaded_size=750,
                total_size=1000,
                files_completed=3,
                total_files=4,
            )


class TestAssetManager(TestCase):
    def setUp(self):
        self.manager = AssetManager()

    def test_singleton_behavior(self):
        another_instance = AssetManager()
        self.assertIs(self.manager, another_instance)

    def test_enable_emit_status(self):
        self.manager.enable_emit_status()
        self.assertTrue(self.manager._emit_status)
        self.assertIsNotNone(self.manager._channel_layer)

    def test_disable_emit_status(self):
        self.manager.enable_emit_status()
        self.manager.disable_emit_status()
        self.assertFalse(self.manager._emit_status)
        self.assertIsNone(self.manager._channel_layer)

    def test_build_asset_path(self):
        with override_settings(MANAGED_ASSETS_ROOT=Path("/assets")):
            module_name = "plugin"
            target_path = "file.txt"
            expected_path = Path("/assets/plugin/file.txt")
            result = self.manager.build_asset_path(module_name, target_path)
            self.assertEqual(result, expected_path)

    def test_load_manifest_json(self):
        mock_data = '{"key": "value"}'
        with patch("importlib.resources.open_text", mock_open(read_data=mock_data)):
            result = self.manager._load_manifest_json("plugin")
            self.assertEqual(result, {"key": "value"})

    def test_load_manifest_json_invalid(self):
        mock_data = "{ invalid }"
        with (
            patch(
                "importlib.resources.open_text",
                mock_open(read_data=mock_data),
            ),
            self.assertRaises(AssetManifestParseError),
            patch("logging.Logger.error"),
        ):
            self.manager._load_manifest_json("plugin")

    def test_load_manifest_data(self):
        module_name = "top_sikrit_plugin"
        url = "sftp://admin:admin@example.org/path/to/sikrit/file.mp3"
        manifest_json = {
            "sources": [
                {
                    "url": url,
                    "target_path": "sikrit.mp3",
                },
            ],
        }

        loaded = self.manager._load_manifest_data(manifest_json, module_name)

        self.assertEqual(loaded.module_name, module_name)
        self.assertEqual(len(manifest_json["sources"]), len(loaded.sources))

        for index, source in enumerate(loaded.sources):
            json_s = manifest_json["sources"][index]

            self.assertEqual(json_s.get("url"), source.url)
            self.assertEqual(json_s.get("target_path"), source.target_path)
            self.assertEqual(json_s.get("integrity"), source.integrity)

    def test_load_manifest_data_invalid(self):
        manifest_json = {
            "lol": True,
        }

        with (
            self.assertRaises(AssetManifestError),
            patch("logging.Logger.error"),
        ):
            self.manager._load_manifest_data(manifest_json, "a_valid_module")

    def test_manifest_file_exists(self):
        with patch(
            "importlib.resources.is_resource", return_value=True
        ) as mock_is_resource:
            result = self.manager._manifest_file_exists("test_module")
            self.assertTrue(result)
            mock_is_resource.assert_called_once_with(
                "test_module", self.manager.manifest_file_name
            )

    def test_build_asset_path_for_file_spec(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="path/to/file.txt",
            url="http://example.com",
            integrity=None,
        )

        with patch.object(self.manager, "build_asset_path") as mock_build:
            self.manager.build_asset_path_for_file_spec(file_spec)
            mock_build.assert_called_once_with(
                file_spec.module_name, file_spec.target_path
            )

    def test_load_manifest_for_plugin_no_manifest(self):
        with patch.object(self.manager, "_manifest_file_exists", return_value=False):
            result = self.manager.load_manifest_for_plugin("test_module")
            self.assertEqual(result.sources, [])
            self.assertEqual(result.module_name, "test_module")

    def test_load_manifest_for_plugin_with_manifest(self):
        manifest_json = {
            "sources": [{"url": "http://example.com", "target_path": "file.txt"}]
        }

        with (
            patch.object(self.manager, "_manifest_file_exists", return_value=True),
            patch.object(
                self.manager, "_load_manifest_json", return_value=manifest_json
            ),
        ):
            result = self.manager.load_manifest_for_plugin("test_module")
            self.assertEqual(len(result.sources), 1)
            self.assertEqual(result.module_name, "test_module")

    def test_load_core_manifest_no_file(self):
        with patch("pathlib.Path.exists", return_value=False):
            result = self.manager._load_core_manifest()
            self.assertEqual(result.sources, [])
            self.assertEqual(result.module_name, "core")

    @patch("pathlib.Path.open", new_callable=mock_open, read_data='{"sources": []}')
    def test_load_core_manifest_with_file(self, mock_file):
        with patch("pathlib.Path.exists", return_value=True):
            result = self.manager._load_core_manifest()
            self.assertEqual(result.sources, [])
            self.assertEqual(result.module_name, "core")

    @patch("pathlib.Path.open", new_callable=mock_open, read_data="invalid json")
    def test_load_core_manifest_invalid_json(self, mock_file):
        with (
            patch("pathlib.Path.exists", return_value=True),
            self.assertRaises(AssetManifestParseError),
            patch("logging.Logger.error"),
        ):
            self.manager._load_core_manifest()

    def test_running_coroutines_in_event_loop(self):
        call_sentinel = MagicMock()

        async def test_coroutine():
            call_sentinel()

        self.manager._run_coro_on_loop(test_coroutine())
        self.manager._run_coro_on_loop(test_coroutine())
        # Wait for them to be executed, as they're run on another thread
        time.sleep(0.3)

        self.assertEqual(call_sentinel.call_count, 2)

    def test_schedule_download_sync_to_async(self):
        call_sentinel = MagicMock()

        async def await_sentinel(self, plugin_name, asset_file_spec_list):
            call_sentinel(plugin_name, asset_file_spec_list)

        with patch.object(
            AssetManager,
            "perform_asset_download",
            new=await_sentinel,
        ):
            self.manager.schedule_asset_download("helloworld", [])

        # Let it execute in the event loop thread
        time.sleep(0.2)

        call_sentinel.assert_called_once_with("helloworld", [])

    def test_calculate_file_hash(self):
        fd = BytesIO(b"password")
        hashing_class = hashlib.sha1

        expected_hash = hashing_class(b"password").hexdigest()
        calculated_hash = self.manager._calculate_file_hash(hashing_class, fd)

        self.assertEqual(expected_hash, calculated_hash)

    def test_asset_file_status(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            url="http://invalid",
            target_path="somewhere/here/maybe.txt",
            integrity=None,
        )

        path = self.manager.build_asset_path_for_file_spec(file_spec)
        # Cleanup from previous tests if there are leftovers
        path.unlink(missing_ok=True)

        # First, check the behavior when the file does not exist
        r1 = self.manager._check_asset_file_status(file_spec)
        self.assertFalse(r1)

        # Then, check when a file exists, with no integrity specified
        path.parent.mkdir(parents=True, exist_ok=True)
        path.touch()
        r2 = self.manager._check_asset_file_status(file_spec)
        self.assertTrue(r2)

        # Then, let's test actual content and hashes
        content = b"one two three"
        sha1_hash = hashlib.sha1(content).hexdigest()

        with path.open("wb") as f:
            f.write(content)

        # First, check the invalid integrity result
        file_spec = replace(file_spec, integrity="sha1-nope")
        r3 = self.manager._check_asset_file_status(file_spec)
        self.assertFalse(r3)

        # And finally, test with the file downloaded correctly, hashes in place,
        # everything looking good and ready to roll
        file_spec = replace(file_spec, integrity=f"sha1-{sha1_hash}")
        r4 = self.manager._check_asset_file_status(file_spec)
        self.assertTrue(r4)


class TestAssetManagerDownload(IsolatedAsyncioTestCase):
    def setUp(self):
        self.manager = AssetManager()

    @patch.object(AssetManager, "_try_downloading_file", return_value=True)
    async def test_download_fresh_asset_file(
        self,
        mock_download,
    ):
        file_spec = AssetFileSpec(
            module_name="plugin",
            target_path="file.txt",
            url="http://example.com/file",
            integrity=None,
        )

        target_path = self.manager.build_asset_path(
            file_spec.module_name,
            file_spec.target_path,
        )
        download_path = target_path.with_suffix(
            f"{target_path.suffix}.download",
        )

        target_path.parent.mkdir(parents=True, exist_ok=True)
        # Ensure clean start
        target_path.unlink(missing_ok=True)
        download_path.unlink(missing_ok=True)

        with (
            patch("plugin.asset_manager.manager.logger.info"),
            patch("plugin.asset_manager.manager.logger.debug"),
            patch.object(Path, "rename") as mock_path_rename,
            patch.object(Path, "unlink") as mock_path_unlink,
        ):
            await self.manager.download_asset_file(file_spec)

        mock_download.assert_called_once_with(file_spec, download_path)
        mock_path_rename.assert_called_once_with(target_path)
        mock_path_unlink.assert_not_called()

    @patch.object(AssetManager, "_try_downloading_file", return_value=True)
    async def test_download_fresh_asset_files(
        self,
        mock_download,
    ):
        file_specs = [
            AssetFileSpec(
                module_name="plugin",
                target_path="file1.txt",
                url="http://example.com/file1",
                integrity=None,
            ),
            AssetFileSpec(
                module_name="plugin",
                target_path="file2.txt",
                url="http://example.com/file2",
                integrity=None,
            ),
        ]

        target_paths = [
            self.manager.build_asset_path(
                file_spec.module_name,
                file_spec.target_path,
            )
            for file_spec in file_specs
        ]

        download_paths = [
            target_path.with_suffix(
                f"{target_path.suffix}.download",
            )
            for target_path in target_paths
        ]

        for target_path in target_paths:
            target_path.parent.mkdir(parents=True, exist_ok=True)
            target_path.unlink(missing_ok=True)

        for download_path in download_paths:
            download_path.unlink(missing_ok=True)

        with (
            patch("plugin.asset_manager.manager.logger.info"),
            patch("plugin.asset_manager.manager.logger.debug"),
            patch.object(Path, "rename"),
            patch.object(Path, "unlink"),
        ):
            await self.manager.download_asset_files(file_specs)

        mock_download.assert_called()
        self.assertEqual(mock_download.call_count, 2)

    @patch.object(AssetManager, "_try_downloading_file", return_value=True)
    async def test_download_and_override_existing_asset_file(
        self,
        mock_download,
    ):
        file_spec = AssetFileSpec(
            module_name="plugin",
            target_path="file.txt",
            url="http://example.com/file",
            integrity=None,
        )

        target_path = self.manager.build_asset_path(
            file_spec.module_name,
            file_spec.target_path,
        )
        target_path.parent.mkdir(parents=True, exist_ok=True)
        target_path.touch(exist_ok=True)

        try:
            download_path = target_path.with_suffix(
                f"{target_path.suffix}.download",
            )

            with (
                patch("plugin.asset_manager.manager.logger.info"),
                patch("plugin.asset_manager.manager.logger.debug"),
                patch.object(Path, "rename") as mock_path_rename,
                patch.object(Path, "unlink") as mock_path_unlink,
            ):
                await self.manager.download_asset_file(file_spec)

            mock_download.assert_called_once_with(file_spec, download_path)
            mock_path_rename.assert_called_once_with(target_path)
            mock_path_unlink.assert_called_once()
        finally:
            target_path.unlink(missing_ok=True)

    @aioresponses()
    async def test_get_file_size(self, aio_mock):
        # Test for normal size
        url_1 = "http://example.org/r2d2.txt"
        size_1 = 2048

        # and for unknown size
        url_2 = "http://example.org/c3po.txt"

        # and for malformed size
        url_3 = "http://example.org/bb8.txt"
        size_3 = "asdf"

        aio_mock.head(url_1, status=200, headers={"Content-Length": str(size_1)})
        aio_mock.head(url_2, status=200)
        aio_mock.head(url_3, status=200, headers={"Content-Length": size_3})

        result_1 = await self.manager._get_file_size(url_1)
        result_2 = await self.manager._get_file_size(url_2)
        with patch("logging.Logger.error") as log_err:
            result_3 = await self.manager._get_file_size(url_3)
            log_err.assert_called_once()

        self.assertEqual(result_1, size_1)
        self.assertIsNone(result_2)
        self.assertIsNone(result_3)

    @patch.object(AssetManager, "_download_to_file", side_effect=Exception)
    async def test_try_downloading_file_retrying(self, mock_download):
        file_spec = MagicMock()
        download_path = MagicMock()

        with patch("logging.Logger.exception"):
            r = await self.manager._try_downloading_file(
                file_spec,
                download_path,
            )

        self.assertFalse(r)
        self.assertEqual(mock_download.call_count, 3)

    @aioresponses()
    async def test_stream_download(self, mocked):
        file_spec = AssetFileSpec(
            module_name="test",
            url="http://example.com/file",
            target_path="file.txt",
            integrity=None,
        )

        content = b"test content"
        mocked.get(file_spec.url, body=content)

        chunks = []
        async for chunk in self.manager._stream_download(file_spec):
            chunks.append(chunk)

        self.assertEqual(b"".join(chunks), content)

    @aioresponses()
    async def test_download_to_file_exception(self, mocked):
        """Test downloading to a file when an exception occurs."""
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com/file",
            integrity=None,
        )

        download_path = MagicMock(spec=Path)

        # Mock the download tracker
        self.manager._download_status = {"test_module": MagicMock(spec=DownloadTracker)}
        self.manager._download_status["test_module"].update = AsyncMock()

        # Create an async iterator for the chunks
        async def mock_stream_download(file_spec):
            yield b"chunk1"
            # Simulate failure after first chunk
            raise Exception("Download failed")

        # Mock the stream download method to return our async iterator
        with patch.object(
            self.manager, "_stream_download", side_effect=mock_stream_download
        ):
            with patch.object(Path, "open", mock_open()):
                with self.assertRaises(Exception):
                    await self.manager._download_to_file(file_spec, download_path)

                # Verify download status is updated for rollback
                self.manager._download_status["test_module"].update.assert_called_with(
                    -len(b"chunk1")
                )

    def test_check_plugin_asset_status_no_manifest(self):
        with patch.object(self.manager, "_manifest_file_exists", return_value=False):
            result, missing = self.manager.check_plugin_asset_status("test_module")
            self.assertTrue(result)
            self.assertEqual(missing, [])

    def test_check_plugin_asset_status_all_valid(self):
        manifest_json = {
            "sources": [{"url": "http://example.com", "target_path": "file.txt"}]
        }

        with (
            patch.object(self.manager, "_manifest_file_exists", return_value=True),
            patch.object(
                self.manager, "_load_manifest_json", return_value=manifest_json
            ),
            patch.object(self.manager, "_check_asset_file_status", return_value=True),
        ):
            result, missing = self.manager.check_plugin_asset_status("test_module")
            self.assertTrue(result)
            self.assertEqual(missing, [])

    def test_check_plugin_asset_status_missing_files(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        manifest_json = {
            "sources": [{"url": "http://example.com", "target_path": "file.txt"}]
        }

        with (
            patch.object(self.manager, "_manifest_file_exists", return_value=True),
            patch.object(
                self.manager, "_load_manifest_json", return_value=manifest_json
            ),
            patch.object(
                self.manager,
                "_load_manifest_data",
                return_value=MagicMock(sources=[file_spec]),
            ),
            patch.object(self.manager, "_check_asset_file_status", return_value=False),
        ):
            result, missing = self.manager.check_plugin_asset_status("test_module")
            self.assertFalse(result)
            self.assertEqual(missing, [file_spec])

    def test_schedule_missing_plugin_assets_download_no_missing(self):
        with patch.object(
            self.manager, "check_plugin_asset_status", return_value=(True, [])
        ):
            result = self.manager.schedule_missing_plugin_assets_download("test_module")
            self.assertFalse(result)

    def test_schedule_missing_plugin_assets_download_with_missing(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        with (
            patch.object(
                self.manager,
                "check_plugin_asset_status",
                return_value=(False, [file_spec]),
            ),
            patch.object(self.manager, "schedule_asset_download") as mock_schedule,
        ):
            result = self.manager.schedule_missing_plugin_assets_download("test_module")
            self.assertTrue(result)
            mock_schedule.assert_called_once_with(
                module_name="test_module", asset_file_spec_list=[file_spec]
            )

    def test_check_core_external_assets_status_no_manifest(self):
        with patch("pathlib.Path.exists", return_value=False):
            result, missing = self.manager.check_core_external_assets_status()
            self.assertTrue(result)
            self.assertEqual(missing, [])

    def test_check_core_external_assets_status_parse_error(self):
        with (
            patch("pathlib.Path.exists", return_value=True),
            patch.object(
                self.manager, "_load_core_manifest", side_effect=AssetManifestParseError
            ),
        ):
            result, missing = self.manager.check_core_external_assets_status()
            self.assertFalse(result)
            self.assertEqual(missing, [])

    def test_check_core_external_assets_status_manifest_error(self):
        with (
            patch("pathlib.Path.exists", return_value=True),
            patch.object(
                self.manager, "_load_core_manifest", side_effect=AssetManifestError
            ),
        ):
            result, missing = self.manager.check_core_external_assets_status()
            self.assertFalse(result)
            self.assertEqual(missing, [])

    def test_check_core_external_assets_status_all_valid(self):
        file_spec = AssetFileSpec(
            module_name="core",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        manifest = MagicMock()
        manifest.sources = [file_spec]

        with (
            patch("pathlib.Path.exists", return_value=True),
            patch.object(self.manager, "_load_core_manifest", return_value=manifest),
            patch.object(self.manager, "_check_asset_file_status", return_value=True),
        ):
            result, missing = self.manager.check_core_external_assets_status()
            self.assertTrue(result)
            self.assertEqual(missing, [])

    def test_check_core_external_assets_status_missing_files(self):
        file_spec = AssetFileSpec(
            module_name="core",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        manifest = MagicMock()
        manifest.sources = [file_spec]

        with (
            patch("pathlib.Path.exists", return_value=True),
            patch.object(self.manager, "_load_core_manifest", return_value=manifest),
            patch.object(self.manager, "_check_asset_file_status", return_value=False),
        ):
            result, missing = self.manager.check_core_external_assets_status()
            self.assertFalse(result)
            self.assertEqual(missing, [file_spec])

    def test_schedule_missing_core_assets_download_no_missing(self):
        with patch.object(
            self.manager, "check_core_external_assets_status", return_value=(True, [])
        ):
            result = self.manager.schedule_missing_core_assets_download()
            self.assertFalse(result)

    def test_schedule_missing_core_assets_download_with_missing(self):
        file_spec = AssetFileSpec(
            module_name="core",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        with (
            patch.object(
                self.manager,
                "check_core_external_assets_status",
                return_value=(False, [file_spec]),
            ),
            patch.object(self.manager, "schedule_asset_download") as mock_schedule,
        ):
            result = self.manager.schedule_missing_core_assets_download()
            self.assertTrue(result)
            mock_schedule.assert_called_once_with(
                module_name="core", asset_file_spec_list=[file_spec]
            )

    def test_verify_file_integrity_no_integrity_defined(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity=None,
        )

        file_path = MagicMock(spec=Path)

        result = self.manager.verify_file_integrity(file_spec, file_path)
        self.assertTrue(result)

    def test_verify_file_integrity_valid_hash(self):
        content = b"test data"
        sha1_hash = hashlib.sha1(content).hexdigest()

        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity=f"sha1-{sha1_hash}",
        )

        file_path = MagicMock(spec=Path)
        mock_file = MagicMock()
        mock_file.__enter__.return_value = mock_file
        file_path.open.return_value = mock_file

        with patch.object(self.manager, "_calculate_file_hash", return_value=sha1_hash):
            result = self.manager.verify_file_integrity(file_spec, file_path)
            self.assertTrue(result)

    def test_verify_file_integrity_invalid_hash(self):
        content = b"test data"
        actual_hash = hashlib.sha1(content).hexdigest()
        expected_hash = "invalid_hash"

        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity=f"sha1-{expected_hash}",
        )

        file_path = MagicMock(spec=Path)
        mock_file = MagicMock()
        mock_file.__enter__.return_value = mock_file
        file_path.open.return_value = mock_file

        with patch.object(
            self.manager, "_calculate_file_hash", return_value=actual_hash
        ):
            result = self.manager.verify_file_integrity(file_spec, file_path)
            self.assertFalse(result)

    def test_verify_file_integrity_exception(self):
        file_spec = AssetFileSpec(
            module_name="test_module",
            target_path="file.txt",
            url="http://example.com",
            integrity="sha1-hash",
        )

        file_path = MagicMock(spec=Path)
        file_path.open.side_effect = Exception("Failed to open file")

        with patch("logging.Logger.exception"):
            result = self.manager.verify_file_integrity(file_spec, file_path)
            self.assertFalse(result)
