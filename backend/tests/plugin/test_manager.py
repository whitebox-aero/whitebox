import unittest
import importlib
import os

from unittest.mock import patch, MagicMock
from django.conf import settings

from plugin.manager import PluginManager


class TestPluginManager(unittest.TestCase):
    def setUp(self):
        # Reset the singleton instance before each test
        PluginManager._instance = None
        self.plugin_manager = PluginManager()
        self.plugin_manager.previously_discovered_plugins = {}

        self.plugins_patcher = patch.object(self.plugin_manager, "plugins", new=[])
        self.plugins_patcher.start()

    @patch("importlib.metadata.distributions")
    def test_get_discovered_plugins(self, mock_distributions):
        mock_dist1 = MagicMock()
        mock_dist1.metadata = {"Name": "whitebox-plugin-test1"}
        mock_dist2 = MagicMock()
        mock_dist2.metadata = {"Name": "whitebox-plugin-test2"}
        mock_distributions.return_value = [mock_dist1, mock_dist2]

        def mock_import_side_effect(name):
            if name in [
                "whitebox_plugin_test1.whitebox_plugin_test1",
                "whitebox_plugin_test2.whitebox_plugin_test2",
            ]:
                return MagicMock()
            return importlib.import_module(name)

        with patch("importlib.import_module", side_effect=mock_import_side_effect):
            discovered_plugins = self.plugin_manager._get_discovered_plugins()

        self.assertIn("whitebox_plugin_test1", discovered_plugins)
        self.assertIn("whitebox_plugin_test2", discovered_plugins)

        expected_plugin_count = 2
        self.assertEqual(len(discovered_plugins), expected_plugin_count)

    @patch("os.path.exists")
    def test_load_plugin_template(self, mock_exists):
        mock_exists.return_value = True
        settings.TEMPLATES = [{"DIRS": []}]

        with patch.object(
            self.plugin_manager, "_get_plugin_path", return_value="/path/to/plugin"
        ):
            self.plugin_manager._load_plugin_template("test_plugin")

        expected_template_dir = os.path.join("/path/to/plugin", "templates")
        self.assertIn(expected_template_dir, settings.TEMPLATES[0]["DIRS"])

    @patch("os.path.exists")
    def test_load_plugin_static(self, mock_exists):
        mock_exists.return_value = True
        settings.STATICFILES_DIRS = []

        with patch.object(
            self.plugin_manager, "_get_plugin_path", return_value="/path/to/plugin"
        ):
            self.plugin_manager._load_plugin_static("test_plugin")

        expected_static_dir = os.path.join("/path/to/plugin", "static")
        self.assertIn(expected_static_dir, settings.STATICFILES_DIRS)

    @patch.object(PluginManager, "_load_plugin_class")
    @patch.object(PluginManager, "_load_plugin_template")
    @patch.object(PluginManager, "_load_plugin_static")
    @patch.object(PluginManager, "_load_plugin_device_classes")
    @patch(
        "plugin.manager.asset_manager.check_plugin_asset_status",
        return_value=(True, []),
    )
    def test_load_plugin(
        self,
        mock_asset_manager,
        mock_device_classes,
        mock_static,
        mock_template,
        mock_class,
    ):
        mock_module = MagicMock()
        self.plugin_manager._load_plugin("test_plugin", mock_module)

        mock_class.assert_called_once_with(mock_module)
        mock_template.assert_called_once_with("test_plugin")
        mock_static.assert_called_once_with("test_plugin")
        mock_device_classes.assert_called_once_with(mock_module)

    @patch.object(PluginManager, "_unload_plugin_class")
    @patch.object(PluginManager, "_unload_plugin_template")
    @patch.object(PluginManager, "_unload_plugin_static")
    def test_unload_plugin(self, mock_static, mock_template, mock_class):
        mock_module = MagicMock()
        self.plugin_manager._unload_plugin("test_plugin", mock_module)

        mock_class.assert_called_once_with(mock_module)
        mock_template.assert_called_once_with("test_plugin")
        mock_static.assert_called_once_with("test_plugin")

    @patch.object(PluginManager, "_get_discovered_plugins")
    @patch.object(PluginManager, "_unload_plugin")
    @patch.object(PluginManager, "_load_plugin")
    def test_discover_plugins(self, mock_install, mock_remove, mock_get_discovered):
        mock_old_plugin = MagicMock()
        mock_new_plugin = MagicMock()

        self.plugin_manager.previously_discovered_plugins = {
            "old_plugin": mock_old_plugin
        }
        mock_get_discovered.return_value = {"new_plugin": mock_new_plugin}

        self.plugin_manager.discover_plugins()

        mock_remove.assert_called_once_with("old_plugin", mock_old_plugin)
        mock_install.assert_called_once_with("new_plugin", mock_new_plugin)

        self.assertEqual(
            self.plugin_manager.previously_discovered_plugins,
            {"new_plugin": mock_new_plugin},
        )

    def test_get_plugins(self):
        self.plugin_manager.plugins = [MagicMock(), MagicMock()]
        plugins = self.plugin_manager.get_plugins()
        self.assertEqual(len(plugins), 2)

    def test_get_plugin_names(self):
        mock_plugin1 = MagicMock()
        mock_plugin1.name = "Plugin1"
        mock_plugin2 = MagicMock()
        mock_plugin2.name = None
        mock_plugin2.__class__.__name__ = "Plugin2Class"
        self.plugin_manager.plugins = [mock_plugin1, mock_plugin2]

        names = self.plugin_manager.get_plugin_names()
        self.assertEqual(names, ["Plugin1", "Plugin2Class"])

    def test_get_plugin_for_capability(self):
        mock_plugin1 = MagicMock()
        mock_plugin1.provides_capabilities = ["capability1"]
        mock_plugin2 = MagicMock()
        mock_plugin2.provides_capabilities = ["capability2"]
        self.plugin_manager.plugins = [mock_plugin1, mock_plugin2]

        plugin = self.plugin_manager.get_plugin_for_capability("capability1")
        self.assertEqual(plugin, mock_plugin1)

        plugin = self.plugin_manager.get_plugin_for_capability("capability2")
        self.assertEqual(plugin, mock_plugin2)

        plugin = self.plugin_manager.get_plugin_for_capability("capability3")
        self.assertIsNone(plugin)
