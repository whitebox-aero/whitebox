from django.test import TestCase
from unittest.mock import patch, AsyncMock, ANY

from location.handlers import LocationUpdateHandler


class TestHandlers(TestCase):
    @patch("location.handlers.LocationService.update_location", new_callable=AsyncMock)
    @patch("location.handlers.event_emitter.emit_event", new_callable=AsyncMock)
    async def test_handle(self, mock_emit_event, mock_update_location):
        data = {
            "type": "location_update",
            "latitude": 37.77,
            "longitude": -122.40,
            "altitude": 100,
        }

        handler = LocationUpdateHandler()
        response = await handler.handle(data)

        self.assertEqual(response["type"], "location_update")
        self.assertEqual(response["latitude"], 37.77)
        self.assertEqual(response["longitude"], -122.40)
        self.assertEqual(response["altitude"], 100)
        self.assertIsInstance(response["gps_timestamp"], str)
        self.assertIn("gps_timestamp", response)

        mock_update_location.assert_awaited_once_with(37.77, -122.40, 100, ANY)
        mock_emit_event.assert_awaited_once()
