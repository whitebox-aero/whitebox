from django.test import TestCase
from django.apps import apps

from location.apps import LocationConfig


class TestLocationConfig(TestCase):
    def test_app_name(self):
        self.assertEqual(LocationConfig.name, "location")

    def test_default_auto_field(self):
        self.assertEqual(
            LocationConfig.default_auto_field, "django.db.models.BigAutoField"
        )

    def test_app_config_type(self):
        app_config = apps.get_app_config("location")
        self.assertIsInstance(app_config, LocationConfig)

    def test_app_config_label(self):
        app_config = apps.get_app_config("location")
        self.assertEqual(app_config.label, "location")

    def test_app_config_models_module(self):
        app_config = apps.get_app_config("location")
        self.assertIsNotNone(app_config.models_module)
