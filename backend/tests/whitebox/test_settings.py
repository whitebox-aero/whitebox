import os

from django.test import TestCase
from django.conf import settings
from pathlib import Path


class TestDjangoSettings(TestCase):
    def test_base_dir(self):
        expected_base_dir = Path(__file__).resolve().parent.parent.parent / "whitebox"
        self.assertEqual(settings.BASE_DIR, expected_base_dir)

    def test_plugin_dir(self):
        expected_plugin_dir = settings.BASE_DIR / "plugins"
        self.assertEqual(settings.PLUGIN_DIR, expected_plugin_dir)

    def test_installed_apps(self):
        required_apps = [
            "django.contrib.admin",
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "django.contrib.sessions",
            "django.contrib.messages",
            "daphne",
            "channels",
            "reversion",
            "corsheaders",
            "rest_framework",
            # This one is incompatible with our whitenoise setup
            # "django.contrib.staticfiles",
            "event",
            "location",
            "traffic",
            "status",
            "plugin",
            "whitebox",
        ]
        for app in required_apps:
            self.assertIn(app, settings.INSTALLED_APPS)

    def test_database_config(self):
        self.assertEqual(
            settings.DATABASES["default"]["ENGINE"],
            "django.db.backends.postgresql_psycopg2",
        )
        self.assertIn("whitebox", settings.DATABASES["default"]["NAME"])
        self.assertEqual(
            settings.DATABASES["default"]["USER"],
            os.environ.get("POSTGRES_USER", "whitebox"),
        )
        self.assertEqual(
            settings.DATABASES["default"]["PASSWORD"],
            os.environ.get("POSTGRES_PASSWORD", "whitebox"),
        )
        self.assertEqual(
            settings.DATABASES["default"]["HOST"],
            os.environ.get("POSTGRES_HOST", "localhost"),
        )
        self.assertEqual(
            settings.DATABASES["default"]["PORT"], os.environ.get("POSTGRES_PORT", 5432)
        )

    def test_channel_layers(self):
        self.assertEqual(
            settings.CHANNEL_LAYERS["default"]["BACKEND"],
            "channels_redis.core.RedisChannelLayer",
        )
        self.assertEqual(
            settings.CHANNEL_LAYERS["default"]["CONFIG"]["hosts"][0][0],
            os.environ.get("REDIS_HOST", "localhost"),
        )
        self.assertEqual(
            settings.CHANNEL_LAYERS["default"]["CONFIG"]["hosts"][0][1],
            os.environ.get("REDIS_PORT", 6379),
        )

    def test_middleware(self):
        required_middleware = [
            "django.middleware.security.SecurityMiddleware",
            "django.contrib.sessions.middleware.SessionMiddleware",
            "django.middleware.common.CommonMiddleware",
            "django.middleware.csrf.CsrfViewMiddleware",
            "django.contrib.auth.middleware.AuthenticationMiddleware",
            "django.contrib.messages.middleware.MessageMiddleware",
            "django.middleware.clickjacking.XFrameOptionsMiddleware",
        ]
        for middleware in required_middleware:
            self.assertIn(middleware, settings.MIDDLEWARE)

    def test_time_zone_and_language(self):
        self.assertEqual(settings.TIME_ZONE, "UTC")
        self.assertEqual(settings.LANGUAGE_CODE, "en-us")
        self.assertTrue(settings.USE_I18N)
        self.assertTrue(settings.USE_TZ)

    def test_static_url(self):
        self.assertEqual(settings.STATIC_URL, "/static/")

    def test_default_auto_field(self):
        self.assertEqual(settings.DEFAULT_AUTO_FIELD, "django.db.models.BigAutoField")
