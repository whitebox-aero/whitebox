from django.test import TestCase
from channels.testing import WebsocketCommunicator
from whitebox.routing import consumers


class TestRouting(TestCase):
    async def test_websocket_url(self):
        communicator = WebsocketCommunicator(
            consumers.FlightConsumer.as_asgi(), "/ws/flight/"
        )
        connected, _ = await communicator.connect()
        self.assertTrue(connected)
        await communicator.disconnect()
