from unittest.mock import patch, AsyncMock
from channels.testing import WebsocketCommunicator
from django.test import TestCase
from whitebox.consumers import FlightConsumer, ManagementConsumer
from whitebox.api import API


class TestFlightConsumer(TestCase):
    def setUp(self):
        self.application = FlightConsumer.as_asgi()

    async def connect_and_get_communicator(self):
        communicator = WebsocketCommunicator(self.application, "/ws/flight/")
        connected, _ = await communicator.connect()
        self.assertTrue(connected)
        return communicator

    async def test_connect(self):
        communicator = await self.connect_and_get_communicator()
        await communicator.disconnect()

    @patch("whitebox.event_handler.EventHandlerFactory.create_handler")
    async def test_receive_valid_event(self, mock_create_handler):
        communicator = await self.connect_and_get_communicator()

        mock_handler = AsyncMock()
        mock_handler.handle.return_value = {
            "type": "message",
            "message": "Flight started",
        }
        mock_create_handler.return_value = mock_handler

        await communicator.send_json_to({"type": "flight_start"})
        response = await communicator.receive_json_from()

        mock_create_handler.assert_called_once_with("flight_start")
        mock_handler.handle.assert_called_once_with({"type": "flight_start"})
        self.assertEqual(response, {"type": "message", "message": "Flight started"})

        await communicator.disconnect()

    async def test_receive_invalid_event(self):
        communicator = await self.connect_and_get_communicator()

        await communicator.send_json_to({"type": "invalid_event"})
        response = await communicator.receive_json_from()
        self.assertEqual(response["type"], "error")
        self.assertIn("message", response)

        await communicator.disconnect()

    @patch("whitebox.event_handler.EventHandlerFactory.create_handler")
    async def test_location_update(self, mock_create_handler):
        communicator = await self.connect_and_get_communicator()

        mock_handler = AsyncMock()
        mock_handler.handle.return_value = {
            "type": "location_update",
            "latitude": 37.77,
            "longitude": -122.40,
            "altitude": 10.49,
            "gps_timestamp": "2021-01-01T00:00:00Z",
        }
        mock_create_handler.return_value = mock_handler

        await API.location.emit_location_update(37.77, -122.40, 10.49)

        response = await communicator.receive_json_from()
        mock_create_handler.assert_called_once_with("location_update")
        self.assertEqual(response["type"], "location_update")
        self.assertEqual(response["latitude"], 37.77)
        self.assertEqual(response["longitude"], -122.40)
        self.assertEqual(response["altitude"], 10.49)
        self.assertIn("gps_timestamp", response)

        await communicator.disconnect()

    async def test_traffic_update(self):
        communicator = await self.connect_and_get_communicator()

        await API.traffic.emit_traffic_update(
            {
                "callsign": "UAL123",
                "latitude": 37.77,
                "longitude": -122.40,
                "altitude": 10.49,
            }
        )

        response = await communicator.receive_json_from()
        self.assertEqual(response["type"], "traffic_update")
        self.assertEqual(response["callsign"], "UAL123")
        self.assertEqual(response["latitude"], 37.77)
        self.assertEqual(response["longitude"], -122.40)
        self.assertEqual(response["altitude"], 10.49)

        await communicator.disconnect()

    async def test_status_update(self):
        communicator = await self.connect_and_get_communicator()

        await API.status.emit_status_update(
            {
                "sdr_devices": 2,
                "gps_position_accuracy": 0.5,
            }
        )

        response = await communicator.receive_json_from()
        self.assertEqual(response["type"], "status_update")
        self.assertEqual(response["sdr_devices"], 2)
        self.assertEqual(response["gps_position_accuracy"], 0.5)

        await communicator.disconnect()

    @patch("whitebox.event_handler.EventHandlerFactory.create_handler")
    async def test_multiple_events(self, mock_create_handler):
        communicator = await self.connect_and_get_communicator()

        mock_handler = AsyncMock()
        mock_handler.handle.side_effect = [
            {"type": "message", "message": "Flight started"},
            {"type": "message", "message": "Flight ended"},
        ]
        mock_create_handler.return_value = mock_handler

        await communicator.send_json_to({"type": "flight_start"})
        response1 = await communicator.receive_json_from()
        self.assertEqual(response1, {"type": "message", "message": "Flight started"})

        await communicator.send_json_to({"type": "flight_end"})
        response2 = await communicator.receive_json_from()
        self.assertEqual(response2, {"type": "message", "message": "Flight ended"})

        self.assertEqual(mock_create_handler.call_count, 2)
        self.assertEqual(mock_handler.handle.call_count, 2)

        await communicator.disconnect()


class TestManagementConsumer(TestCase):
    def setUp(self):
        self.application = ManagementConsumer.as_asgi()

    async def connect_and_get_communicator(self):
        communicator = WebsocketCommunicator(self.application, "/ws/management/")
        connected, _ = await communicator.connect()
        self.assertTrue(connected)
        return communicator

    async def test_connect(self):
        communicator = await self.connect_and_get_communicator()
        await communicator.disconnect()

    @patch("whitebox.event_handler.EventHandlerFactory.create_handler")
    async def test_plugin_download(self, mock_create_handler):
        communicator = await self.connect_and_get_communicator()

        mock_handler = AsyncMock()
        mock_handler.handle.return_value = {
            "type": "plugin.download",
            "status": "downloading",
            "plugin_name": "test-plugin",
            "progress": 50,
        }
        mock_create_handler.return_value = mock_handler

        await communicator.send_json_to(
            {
                "type": "plugin.download",
                "method": "plugin_download",
                "plugin_name": "test-plugin",
            }
        )

        response = await communicator.receive_json_from()
        mock_create_handler.assert_called_once_with("plugin.download")
        self.assertEqual(response["type"], "plugin.download")
        self.assertEqual(response["status"], "downloading")
        self.assertEqual(response["progress"], 50)
        self.assertEqual(response["plugin_name"], "test-plugin")

        await communicator.disconnect()
