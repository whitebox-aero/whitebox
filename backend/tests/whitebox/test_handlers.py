from unittest.mock import patch, AsyncMock
from django.test import TestCase

from whitebox.utils import EventHandler
from plugin.manager import plugin_manager
from whitebox.handlers import FlightStartHandler, FlightEndHandler


class TestFlightStartHandler(TestCase):
    @patch("plugin.manager.event_emitter.emit_event")
    async def test_handle(self, mock_emit_event):
        handler = FlightStartHandler()
        data = {}
        response = await handler.handle(data)

        mock_emit_event.assert_called_once_with("flight_start")
        self.assertEqual(response, {"type": "message", "message": "Flight started"})


class TestFlightEndHandler(TestCase):
    @patch("plugin.manager.event_emitter.emit_event")
    async def test_handle(self, mock_emit_event):
        handler = FlightEndHandler()
        data = {}
        response = await handler.handle(data)

        mock_emit_event.assert_called_once_with("flight_end")
        self.assertEqual(response, {"type": "message", "message": "Flight ended"})
