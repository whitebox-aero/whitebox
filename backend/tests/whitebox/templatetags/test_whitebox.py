from unittest import TestCase

from django.templatetags.static import static

from whitebox.templatetags.whitebox import tagged_static


class TestWhitebox(TestCase):
    def test_tagged_static(self):
        # GIVEN a path
        path = "left/then/right/then/left/then/right/then/left"
        static_path = static(path)

        # WHEN generating the tagged static path
        tagged_path = tagged_static(path)

        # THEN the resulting static path should be prepended with the API URL
        # placeholder
        expected_tagged_path = "%API_URL%" + static_path
        self.assertEqual(tagged_path, expected_tagged_path)
