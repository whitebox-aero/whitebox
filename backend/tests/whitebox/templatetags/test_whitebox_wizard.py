from unittest import TestCase
from unittest.mock import patch

from whitebox.templatetags.whitebox_wizard import (
    wizard_image,
    wizard_video,
)


class TestWhiteboxWizard(TestCase):
    @patch("whitebox.templatetags.whitebox_wizard.loader.render_to_string")
    @patch("whitebox.templatetags.whitebox_wizard.tagged_static")
    @patch("whitebox.templatetags.whitebox_wizard.mark_safe")
    def test_wizard_video(
        self,
        mock_mark_safe,
        mock_tagged_static,
        mock_render_to_string,
    ):
        path = "left/then/right/then/left/then/right/then/left"
        rendered = wizard_video(path)

        mock_tagged_static.assert_called_once_with(path)
        mock_render_to_string.assert_called_once_with(
            "bridge/widgets/wizard_video.html",
            context={"url": mock_tagged_static.return_value},
        )
        mock_mark_safe.assert_called_once_with(
            mock_render_to_string.return_value,
        )
        self.assertEqual(rendered, mock_mark_safe.return_value)

    @patch("whitebox.templatetags.whitebox_wizard.loader.render_to_string")
    @patch("whitebox.templatetags.whitebox_wizard.tagged_static")
    @patch("whitebox.templatetags.whitebox_wizard.mark_safe")
    def test_wizard_image(
        self,
        mock_mark_safe,
        mock_tagged_static,
        mock_render_to_string,
    ):
        path = "left/then/right/then/left/then/right/then/left"
        rendered = wizard_image(path)

        mock_tagged_static.assert_called_once_with(path)
        mock_render_to_string.assert_called_once_with(
            "bridge/widgets/wizard_image.html",
            context={"url": mock_tagged_static.return_value},
        )
        mock_mark_safe.assert_called_once_with(
            mock_render_to_string.return_value,
        )
        self.assertEqual(rendered, mock_mark_safe.return_value)
