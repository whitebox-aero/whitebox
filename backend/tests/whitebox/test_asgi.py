import unittest
import os

from unittest.mock import patch
from channels.testing import WebsocketCommunicator

from whitebox.asgi import application


class TestASGIConfig(unittest.IsolatedAsyncioTestCase):
    @patch("whitebox.routing.websocket_urlpatterns", [])
    async def test_websocket_connection(self):
        communicator = WebsocketCommunicator(application, "/ws/flight/")
        connected, _ = await communicator.connect()
        self.assertTrue(connected)
        await communicator.disconnect()

    def test_django_settings_module(self):
        self.assertEqual(os.environ.get("DJANGO_SETTINGS_MODULE"), "whitebox.settings")
