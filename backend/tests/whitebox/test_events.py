import unittest
import asyncio

from unittest.mock import Mock, AsyncMock

from whitebox.events import EventEmitter


class TestEventEmitter(unittest.TestCase):
    def setUp(self):
        self.emitter = EventEmitter()

    def test_append(self):
        callback = Mock()
        self.emitter.append("test_event", callback)
        self.assertIn("test_event", self.emitter._listeners)
        self.assertIn(callback, self.emitter._listeners["test_event"])

    def test_remove(self):
        callback = Mock()
        self.emitter.append("test_event", callback)
        self.emitter.remove("test_event", callback)
        self.assertNotIn(callback, self.emitter._listeners["test_event"])

    def test_remove_nonexistent(self):
        callback = Mock()
        self.emitter.remove("test_event", callback)

    async def async_test_emit_event(self):
        mock_callback1 = AsyncMock()
        mock_callback2 = AsyncMock()
        self.emitter.append("test_event", mock_callback1)
        self.emitter.append("test_event", mock_callback2)

        test_data = {"key": "value"}
        await self.emitter.emit_event("test_event", test_data)

        mock_callback1.assert_called_once_with(test_data)
        mock_callback2.assert_called_once_with(test_data)

    def test_emit_event(self):
        asyncio.run(self.async_test_emit_event())

    async def async_test_emit_event_no_listeners(self):
        await self.emitter.emit_event("nonexistent_event")

    def test_emit_event_no_listeners(self):
        asyncio.run(self.async_test_emit_event_no_listeners())
