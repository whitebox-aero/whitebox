from django.test import TestCase
from unittest.mock import patch


class TestWSGIConfig(TestCase):
    @patch("django.core.wsgi.get_wsgi_application")
    def test_application(self, mock_get_wsgi_application):
        from whitebox.wsgi import application

        self.assertTrue(application)
        mock_get_wsgi_application.assert_called_once()
