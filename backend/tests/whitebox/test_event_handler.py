import unittest
from unittest.mock import patch

from whitebox.event_handler import EventHandlerFactory
from whitebox.handlers import FlightStartHandler, FlightEndHandler
from location.handlers import LocationUpdateHandler
from plugin.handlers import PluginDownloadHandler
from whitebox.utils import EventHandler


class TestEventHandlerFactory(unittest.TestCase):
    def setUp(self):
        # Clear the handlers dictionary before each test
        EventHandlerFactory._handlers = {}

    def test_create_flight_start_handler(self):
        handler = EventHandlerFactory.create_handler("flight_start")
        self.assertIsInstance(handler, FlightStartHandler)

    def test_create_flight_end_handler(self):
        handler = EventHandlerFactory.create_handler("flight_end")
        self.assertIsInstance(handler, FlightEndHandler)

    def test_create_location_update_handler(self):
        handler = EventHandlerFactory.create_handler("location_update")
        self.assertIsInstance(handler, LocationUpdateHandler)

    def test_create_plugin_download_handler(self):
        handler = EventHandlerFactory.create_handler("plugin_download")
        self.assertIsInstance(handler, PluginDownloadHandler)

    def test_create_unknown_handler(self):
        with self.assertRaises(ValueError) as context:
            EventHandlerFactory.create_handler("unknown_event")

        self.assertEqual(str(context.exception), "Unknown event type: unknown_event")

    def test_handler_caching(self):
        handler1 = EventHandlerFactory.create_handler("flight_start")
        handler2 = EventHandlerFactory.create_handler("flight_start")
        self.assertIs(handler1, handler2)

    def test_multiple_event_types(self):
        start_handler = EventHandlerFactory.create_handler("flight_start")
        end_handler = EventHandlerFactory.create_handler("flight_end")
        location_handler = EventHandlerFactory.create_handler("location_update")

        self.assertIsInstance(start_handler, FlightStartHandler)
        self.assertIsInstance(end_handler, FlightEndHandler)
        self.assertIsInstance(location_handler, LocationUpdateHandler)

    @patch("whitebox.event_handler.FlightStartHandler")
    def test_handler_initialization(self, mock_handler):
        # Test that handlers are initialized only once
        EventHandlerFactory.create_handler("flight_start")
        EventHandlerFactory.create_handler("flight_start")
        mock_handler.assert_called_once()

    def test_all_handlers_are_event_handlers(self):
        handlers = [
            EventHandlerFactory.create_handler("flight_start"),
            EventHandlerFactory.create_handler("flight_end"),
            EventHandlerFactory.create_handler("location_update"),
        ]
        for handler in handlers:
            self.assertIsInstance(handler, EventHandler)
