from django.urls import reverse
from django.test import TestCase
from rest_framework.test import APIClient


class TestHealthcheck(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_healthcheck(self):
        url = reverse("api:healthcheck")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["status"], "ok")
