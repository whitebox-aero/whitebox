import logging

from devices.manager import device_manager


global original_device_classes


class DeviceClassResetTestMixin:
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        global original_device_classes
        original_device_classes = device_manager.get_device_classes()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

        device_manager.get_device_classes().clear()
        device_manager.get_device_classes().update(original_device_classes)

    def setUp(self):
        super().setUp()

        device_manager.get_device_classes().clear()

    def tearDown(self):
        super().tearDown()
        device_manager.get_device_classes().clear()


class SupressHTTPErrorLoggingMixin:
    def setUp(self) -> None:
        super().setUp()

        logger = logging.getLogger("django.request")
        self.previous_level = logger.getEffectiveLevel()
        logger.setLevel(logging.ERROR)

    def tearDown(self) -> None:
        super().tearDown()

        logger = logging.getLogger("django.request")
        logger.setLevel(self.previous_level)
