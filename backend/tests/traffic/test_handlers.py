from django.test import TestCase
from unittest.mock import patch, AsyncMock

from traffic.handlers import TrafficUpdateHandler


class TestHandlers(TestCase):
    @patch("whitebox.events.event_emitter.emit_event", new_callable=AsyncMock)
    async def test_handle(self, mock_emit_traffic_update):
        data = {
            "type": "traffic_update",
            "data": {
                "speed": 60,
                "location": "San Francisco",
            },
        }

        handler = TrafficUpdateHandler()
        response = await handler.handle(data)

        self.assertEqual(response["type"], "traffic_update")
        self.assertEqual(response["speed"], 60)
        self.assertEqual(response["location"], "San Francisco")

        mock_emit_traffic_update.assert_awaited_once_with(
            "traffic_update", {"speed": 60, "location": "San Francisco"}
        )
