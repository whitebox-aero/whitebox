from unittest.mock import patch
from django.test import TestCase

from traffic.services import TrafficService


class TestTrafficService(TestCase):
    @patch("traffic.services.TrafficService.channel_layer.group_send")
    async def test_emit_traffic_update(self, mock_group_send):
        await TrafficService.emit_traffic_update(
            {"speed": 60, "location": "San Francisco"}
        )

        mock_group_send.assert_called_once_with(
            "flight",
            {
                "type": "traffic_update",
                "data": {
                    "speed": 60,
                    "location": "San Francisco",
                },
            },
        )
