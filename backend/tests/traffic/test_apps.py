from django.test import TestCase
from django.apps import apps

from traffic.apps import TrafficConfig


class TestTrafficConfig(TestCase):
    def test_app_name(self):
        self.assertEqual(TrafficConfig.name, "traffic")

    def test_default_auto_field(self):
        self.assertEqual(
            TrafficConfig.default_auto_field, "django.db.models.BigAutoField"
        )

    def test_app_config_type(self):
        app_config = apps.get_app_config("traffic")
        self.assertIsInstance(app_config, TrafficConfig)

    def test_app_config_label(self):
        app_config = apps.get_app_config("traffic")
        self.assertEqual(app_config.label, "traffic")

    def test_app_config_models_module(self):
        app_config = apps.get_app_config("traffic")
        self.assertIsNone(app_config.models_module)
