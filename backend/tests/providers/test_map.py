from contextlib import contextmanager
from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse

from plugin.utils import Plugin


class MockedMapProviderPlugin(Plugin):
    name = "GPS Display"
    provider_templates = {
        "map": "map-to-el-dorado.html",
    }
    provides_capabilities = ["map"]

    plugin_css = [
        "/one.css",
        "/two.css",
    ]
    plugin_js = [
        "/three.js",
        "/four.js",
    ]


class TestMapProviderView(TestCase):
    def setUp(self):
        self.plugin = MockedMapProviderPlugin()

    @contextmanager
    def _patch_plugin_installed(self):
        with patch(
            "plugin.manager.plugin_manager.get_plugins",
            return_value=[self.plugin],
        ):
            yield

    @patch("django.template.loader.render_to_string", return_value="rendered")
    def test_render_provider_template(self, mock_render_to_string):
        # GIVEN a plugin is registered that will be the map provider
        # WHEN calling the map provider view
        with self._patch_plugin_installed():
            response = self.client.get(reverse("providers:provider-map"))

        # THEN it should return the rendered template
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers["Content-Type"], "application/json")

        self.assertIn("template", response.json())
        self.assertEqual(response.json()["template"], "rendered")

        self.assertIn("css", response.json())
        expected_css = [
            "%API_URL%/one.css",
            "%API_URL%/two.css",
        ]
        self.assertEqual(response.json()["css"], expected_css)

        self.assertIn("js", response.json())
        expected_js = [
            "%API_URL%/three.js",
            "%API_URL%/four.js",
        ]
        self.assertEqual(response.json()["js"], expected_js)
