import importlib
import pkgutil
import logging
import os

from django.test.runner import DiscoverRunner
from unittest import TestSuite


logger = logging.getLogger("whitebox_test_runner")


class WhiteboxTestRunner(DiscoverRunner):
    def build_suite(self, test_labels=None, **kwargs):
        main_suite = super().build_suite(test_labels, **kwargs)
        plugin_suite, plugin_test_count = self.build_plugin_suite()

        # Log the count of main tests and plugin tests
        main_test_count = main_suite.countTestCases()
        logger.info(f"Found {main_test_count} whitebox test(s).")
        logger.info(f"Found {plugin_test_count} plugin test(s).")
        logger.info(f"Found {main_test_count + plugin_test_count} total test(s).")

        # Combine the main suite and plugin suite
        combined_suite = TestSuite()
        combined_suite.addTests(main_suite)
        combined_suite.addTests(plugin_suite)

        return combined_suite

    def build_plugin_suite(self):
        plugin_suite = TestSuite()
        plugin_test_count = 0

        # Discover plugin packages
        plugin_packages = [
            name
            for finder, name, ispkg in pkgutil.iter_modules()
            if name.startswith("whitebox_test_plugin_") and ispkg
        ]

        for package_name in plugin_packages:
            logger.info(f"Discovering tests in package '{package_name}'")
            try:
                package = importlib.import_module(package_name)
                package_path = os.path.dirname(package.__file__)

                # Discover all test modules within the package
                for module_finder, module_name, ispkg in pkgutil.iter_modules(
                    [package_path]
                ):
                    if module_name.startswith("test_"):
                        full_module_name = f"{package_name}.{module_name}"
                        logger.info(f"  Adding tests from module '{full_module_name}'")
                        try:
                            module = importlib.import_module(full_module_name)
                            tests = self.test_loader.loadTestsFromModule(module)
                            plugin_suite.addTests(tests)
                            plugin_test_count += tests.countTestCases()
                        except Exception as e:
                            logger.exception(
                                f"  Failed to load tests from module '{full_module_name}'."
                            )

            except Exception as e:
                logger.exception(f"Failed to process package '{package_name}'.")

        return plugin_suite, plugin_test_count

    def run_tests(self, test_labels, **kwargs):
        # Run the tests
        result = super().run_tests(test_labels, **kwargs)
        return result
