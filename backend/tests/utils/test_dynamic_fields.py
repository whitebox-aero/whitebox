from unittest import TestCase

from rest_framework import serializers
from rest_framework.fields import empty

from utils.dynamic_fields import generate_dynamic_fields


class TestDynamicFields(TestCase):
    def test_dynamic_field_required_default(self):
        # GIVEN a config for a text field
        field_config = {
            "first": {
                "type": "text",
                "name": "Name",
                "required": True,
            },
            "second": {
                "type": "text",
                "name": "Name",
                "required": False,
            },
            "third": {
                "type": "text",
                "name": "Name",
                "default": "libwhitebox.so",
            },
        }

        # WHEN generating dynamic fields
        fields = generate_dynamic_fields(field_config)

        # THEN the fields are generated correctly
        self.assertEqual(len(fields), 3)
        self.assertEqual(set(["first", "second", "third"]), set(fields))

        self.assertTrue(fields["first"].required)
        self.assertEqual(fields["first"].default, empty)

        self.assertFalse(fields["second"].required)
        self.assertEqual(fields["second"].default, empty)

        self.assertFalse(fields["third"].required)
        self.assertEqual(fields["third"].default, "libwhitebox.so")

    def test_dynamic_fields_generation(self):
        # GIVEN a config for dynamic fields
        field_config = {
            "name": {
                "type": "text",
                "name": "Name",
                "required": True,
            },
            "password": {
                "type": "password",
                "name": "Password",
                "required": True,
            },
            "ip": {
                "type": "ip_address",
                "name": "IP Address",
                "default": "127.0.0.1",
            },
        }

        # WHEN generating dynamic fields
        fields = generate_dynamic_fields(field_config)

        # THEN the fields are generated correctly
        self.assertEqual(len(fields), 3)
        self.assertIn("name", fields)
        self.assertIn("password", fields)
        self.assertIn("ip", fields)

        name_field = fields["name"]
        self.assertEqual(name_field.label, "Name")
        self.assertTrue(name_field.required)

        password_field = fields["password"]
        self.assertEqual(password_field.label, "Password")
        self.assertTrue(password_field.required)
        self.assertTrue(password_field.write_only)

        ip_field = fields["ip"]
        self.assertEqual(ip_field.label, "IP Address")
        self.assertFalse(ip_field.required)
        self.assertEqual(ip_field.default, "127.0.0.1")

    def test_dynamic_fields_usage(self):
        # GIVEN a config for dynamic fields
        field_config = {
            "name": {
                "type": "text",
                "name": "Name",
                "required": False,
            },
            "password": {
                "type": "password",
                "name": "Password",
                "required": True,
            },
            "ip": {
                "type": "ip_address",
                "name": "IP Address",
                "default": "127.0.0.1",
            },
        }
        # AND a serializer including said fields
        fields = generate_dynamic_fields(field_config)
        serializer_class = type(
            "TestSerializer",
            (serializers.Serializer,),
            fields,
        )

        # WHEN instantiating the serializer with various data
        serializer1 = serializer_class(data={})
        serializer2 = serializer_class(
            data={
                "password": "admin:admin:verysecure",
            }
        )
        serializer3 = serializer_class(
            data={
                "name": "Cessna",
                "password": "admin:admin:verysecure",
            }
        )
        serializer4 = serializer_class(
            data={
                "name": "Hubble",
                "password": "admin:admin:verysecure",
                "ip": "10.0.0.1",
            }
        )

        # THEN the serializers should be working properly with said fields
        self.assertFalse(serializer1.is_valid())
        self.assertIn("password", serializer1.errors)
        self.assertEqual(
            serializer1.errors["password"][0],
            "This field is required.",
        )

        self.assertTrue(serializer2.is_valid())
        # password is write_only, so it should not be in the user-facing data
        self.assertNotIn("password", serializer2.data)

        self.assertTrue(serializer3.is_valid())
        self.assertEqual(
            serializer3.validated_data,
            {
                "name": "Cessna",
                "password": "admin:admin:verysecure",
                "ip": "127.0.0.1",
            },
        )

        self.assertTrue(serializer4.is_valid())
        self.assertEqual(serializer4.validated_data["ip"], "10.0.0.1")
