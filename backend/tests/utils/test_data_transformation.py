from unittest import TestCase
from urllib.parse import quote

from utils.data_transformation import (
    URLString,
    deep_format,
    format_url_quoted_string,
)


class TestDeepFormat(TestCase):
    def test_dict_deep_format(self):
        # GIVEN a dictionary with format strings
        random_object = object()

        d = {
            "one": "{hello}",
            "two": "{world}",
            "three": "hello",
            "four": {
                "five": [
                    "{world2}",
                ],
                "six": "hello",
                "seven": {
                    "eight": "{hello}",
                    "nine": random_object,
                },
                "ten!": 10,
                "eleven": URLString(quote("example.com/{hello}/{world}")),
            },
        }

        format_strings = {
            "hello": "qwerty",
            "world": "azerty",
            "world2": "dvorak",
        }

        # WHEN deep formatting the dictionary
        formatted_dict = deep_format(d, format_strings)

        # THEN the dictionary should be formatted correctly
        self.assertEqual(
            formatted_dict,
            {
                "one": "qwerty",
                "two": "azerty",
                "three": "hello",
                "four": {
                    "five": [
                        "dvorak",
                    ],
                    "six": "hello",
                    "seven": {
                        "eight": "qwerty",
                        "nine": random_object,
                    },
                    "ten!": 10,
                    "eleven": "example.com/qwerty/azerty",
                },
            },
        )

    def test_format_url_quoted_string(self):
        # GIVEN a URL string with format strings
        s = quote("example.com/{hello}/{world}")
        self.assertEqual(s, "example.com/%7Bhello%7D/%7Bworld%7D")

        format_strings = {
            "hello": "qwerty",
            "world": "azerty",
        }

        # WHEN formatting the string
        formatted_string = format_url_quoted_string(s, format_strings)

        # THEN the string should be formatted correctly
        self.assertEqual(formatted_string, "example.com/qwerty/azerty")
