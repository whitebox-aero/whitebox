import unittest

from utils.drf.viewsets import SerializersActionMapMixin


class TestSerializersActionMapMixin(unittest.TestCase):
    def test_get_serializer_class(self):
        # GIVEN a viewset with a serializers_action_map that maps the 'list'
        #       action to a specific serializer class
        class TestViewSet(SerializersActionMapMixin):
            serializers_action_map = {
                "list": "ListSerializer",
            }
            serializer_class = "DefaultSerializer"

        viewset = TestViewSet()
        viewset.action = "list"

        # WHEN get_serializer_class is called
        serializer_class = viewset.get_serializer_class()

        # THEN the serializer class should be the one mapped to the current
        #      action
        self.assertEqual(serializer_class, "ListSerializer")

    def test_get_serializer_class_default(self):
        # GIVEN a viewset with a serializers_action_map that does not map the
        #       current action
        class TestViewSet(SerializersActionMapMixin):
            serializers_action_map = {
                "create": "CreateSerializer",
            }
            serializer_class = "DefaultSerializer"

        viewset = TestViewSet()
        viewset.action = "list"

        # WHEN get_serializer_class is called
        serializer_class = viewset.get_serializer_class()

        # THEN the serializer class should be the default serializer class
        self.assertEqual(serializer_class, "DefaultSerializer")
