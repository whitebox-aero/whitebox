from unittest import TestCase

from utils.human_readable import human_file_size


class TestHumanFileSize(TestCase):
    def test_bytes(self):
        self.assertEqual(human_file_size(500), "500.00 B")
        self.assertEqual(human_file_size(1023.99), "1023.99 B")

    def test_kilobytes(self):
        self.assertEqual(human_file_size(1024), "1.00 KB")
        self.assertEqual(human_file_size(2048), "2.00 KB")

    def test_megabytes(self):
        self.assertEqual(human_file_size(1048576), "1.00 MB")
        self.assertEqual(human_file_size(2097152), "2.00 MB")

    def test_gigabytes(self):
        # Test cases for values in gigabytes
        self.assertEqual(human_file_size(1073741824), "1.00 GB")
        self.assertEqual(human_file_size(2147483648), "2.00 GB")

    def test_large_numbers(self):
        self.assertEqual(human_file_size(1099511627776), "1024.00 GB")
        self.assertEqual(human_file_size(1125899906842624), "1048576.00 GB")
