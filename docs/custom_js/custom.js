document.addEventListener("DOMContentLoaded", function () {
  // Open all external links in a new tab
  const externalLinks = document.querySelectorAll("a[href^='http']");
  externalLinks.forEach(function (link) {
    link.setAttribute("target", "_blank");
  });
});
