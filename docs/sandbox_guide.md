# Sandbox Guide

This guide outlines how to deploy and manage sandboxes for the Whitebox project using Ansible and associated scripts. This includes setup instructions, feature descriptions, and additional information to aid new contributors.

## Installation

<!-- prettier-ignore-start -->

1. Install Ansible:
    - For Ubuntu: `sudo apt install ansible`
    - For other systems, follow the [official Ansible installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

2. Ensure your SSH keys are added to the server so you can log in without the usual `user:pass` method.

3. Clone this repository and navigate to the `ansible` directory:

    ```bash
    git clone https://gitlab.com/whitebox-aero/whitebox
    cd whitebox/ansible
    ```

4. Install Ansible requirements:

    ```bash
    ansible-galaxy collection install -r requirements.yml
    ```

    Note: This command installs the required Ansible collections locally in your user's home directory (`~/.ansible/collections`).

5. Configure your `hosts.yml` file to specify the IP address of the server that Ansible will SSH into. For example:

    ```yaml
    sandbox_server:
        hosts:
        sandbox_01:
        ansible_host: <server_ip>
        ansible_user: root
        ansible_port: 22
    ```

<!-- prettier-ignore-end -->

## Usage

The `sandbox.sh` script simplifies deploying and managing sandboxes.

### General Usage

```bash
> ./sandbox.sh
Usage: ./sandbox.sh <repo_url|remote_name> <commit_hash|branch_name> <action_type>
Actions: deploy, destroy

Examples:
 ./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e deploy
 ./sandbox.sh origin feature/123 deploy

Note:
1. When given a commit hash while 'destroy', only the sandbox with that commit hash will be destroyed.
2. When given a branch name while 'destroy', all sandboxes with that branch will be destroyed.
```

### Examples

```bash
# Deploy using a branch name:
./sandbox.sh origin feature/123 deploy

# Deploy using a commit hash:
./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e deploy

# Destroy sandboxes for a branch:
./sandbox.sh origin feature/123 destroy

# Destroy a specific sandbox by commit hash:
./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e destroy
```

## Automatic Sandbox Deployment for Product Review

Sandboxes are deployed automatically via CI when the `need-product-review` label is applied to an MR. The aurthor of the MR is expected to add the label and push some change again to trigger `deploy_sandbox` job for sandbox to be deployed.

After deployment, an additional script `update_sandbox_info.py` updates the MR description with relevant URLs (e.g., frontend/backend access).

This whole process is automated using GitLab CI/CD and Ansible and no manual intervention is required.

## Update Sandbox Info Script

The `update_sandbox_info.py` script updates MR descriptions after sandbox deployment with relevant URLs. This script is triggered automatically after the sandbox is deployed.

For running it over CI, the only requirement is to have `GITLAB_TOKEN` set in the CI/CD settings.

You can get the token from your GitLab account settings by:

1. Going to `User Settings` > `Access Tokens`.
2. Creating a new token with `api` scope.

For running it locally, you would need to set two additional environment variables:

- `CI_PROJECT_ID`
- `CI_MERGE_REQUEST_IID`

`CI_PROJECT_ID` can be found by navigating to the project's `Settings` > `General` > `Project ID`. While `CI_MERGE_REQUEST_IID` can be found in the MR URL. That is the number after the `!` in the URL.

You can set these variables in your local shell by first creating a `.env` file with the following content:

```bash
GITLAB_TOKEN=<your_access_token>
CI_PROJECT_ID=<project_id>
CI_MERGE_REQUEST_IID=<merge_request_id>
```

Then, you can set the environment variables by running:

```bash
source .env
```

Install the required packages by running:

```bash
poetry install
```

Finally, you would write logs of ansible playbook to a file by appending `2>&1 | tee deploy.log` in the end of the `deploy` command. Example:

```bash
./sandbox.sh origin 09b1d86e deploy 2>&1 | tee deploy.log
```

And then run the script:

```bash
poetry run python update_sandbox_info.py
```

During development, you can run tests by running:

```bash
make test
```
