# Whitebox Architecture

Whitebox is built on a modular, event-driven architecture designed for extensibility and real-time operations.

## System Overview

- **Django Channels Server**: Provides the foundation for real-time communication via WebSockets.
- **Plugin System**: Allows for easy extension of functionality through self-contained modules.
- **Event System**: Facilitates communication between components and enables reactive architecture.
- **Database Integration**: Offers persistent storage for flight data with revision history.

## Key Components

### Django Channels Server

- Handles asynchronous requests for improved performance
- Manages WebSocket consumers for real-time, bidirectional communication
- Utilizes channel layers for inter-instance communication

Explore the [whitebox codebase](https://gitlab.com/whitebox-aero/whitebox/-/tree/main/backend/whitebox/whitebox) to learn more about the server implementation.

### Plugin System

- Supports dynamic loading and unloading of plugins at runtime from the environment
- Provides isolated environments for each plugin
- Offers standardized APIs for data persistence, event handling and event emission

Explore the [plugin codebase](https://gitlab.com/whitebox-aero/whitebox/-/tree/main/backend/whitebox/plugin) to learn more about the plugin implementation.

### Event System

- Defines system events (e.g., `flight_start`, `flight_end`)
- Allows custom events for plugin-specific functionality
- Implements asynchronous event handling for improved responsiveness

### Database Integration

- Supports PostgreSQL
- Uses Django's ORM for database interactions
- Provides migration support for managing schema changes

## Real-Time Communication Flow

1. Clients connect to the server via WebSockets
2. Server accepts connections and adds clients to the `flight` group
3. Clients send commands or requests to the server
4. Server processes commands, potentially triggering plugin actions
5. Plugins emit events in response to actions or internal processes
6. Server broadcasts relevant events to connected clients and other plugins that have subscribed to those events
7. Clients update their state based on received events

## Next Steps

- Check out the [Development Guide](development_guide.md)
- Explore the [Plugin Guide](plugin_guide.md) for creating custom plugins
