Whitebox's plugin system allows for easy extension of functionality through self-contained modules.
Official Whitebox plugins are available on [PyPI](https://pypi.org/search/?q=%22whitebox-plugin-%22&o=).

A few example plugins are as follows:

- [Whitebox Plugin - GPS Simulator](https://gitlab.com/whitebox-aero/whitebox-plugin-gps-simulator)
- [Whitebox Plugin - Insta360 Camera Support](https://gitlab.com/whitebox-aero/whitebox-plugin-device-insta360)

## Plugin Structure

A typical plugin structure looks like this:

```plaintext
whitebox_plugin_<plugin_name>
├── whitebox_plugin_<plugin_name>
│   ├── static
│   │   ├── whitebox_plugin_<plugin_name>
│   │   │   ├── whitebox_plugin_<plugin_name>.css
│   │   │   └── whitebox_plugin_<plugin_name>.js
│   │   └── assets
│   │       ├── logo.svg
│   │       └── my_image.png
│   ├── templates
│   │   └── whitebox_plugin_<plugin_name>
│   │       └── whitebox_plugin_<plugin_name>.html
│   ├── __init__.py
│   ├── whitebox_plugin_<plugin_name>.py
│   └── any_other_file.xyz
├── whitebox_test_plugin_<plugin_name>
│   ├── __init__.py
│   ├── test_whitebox_plugin_<plugin_name>.py
│   ├── test_whitebox_plugin_<plugin_name>_browser.py
│   └── test_whitebox_plugin_<plugin_name>_integration.py
├── LICENSE
├── Makefile
├── README.md
├── pyproject.toml
└── poetry.lock
```

For whitebox to be able to discover and load plugins dynamically, the plugin package must be named `whitebox_plugin_<plugin_name>`. The plugin package must contain a `whitebox_plugin_<plugin_name>.py` file that defines the `plugin_class` attribute for the plugin class to be exported correctly.

Additionally, for plugin tests to be able to discover and load dynamically, the test package must be named `whitebox_test_plugin_<plugin_name>`. The test files must start with `test_` and they may end with `_browser` or `_integration` to indicate the type of test to ensure readability.

To initialize a new plugin project, run:

```bash
poetry new whitebox_plugin_<plugin_name>
```

Each plugin is a Python package with its own set of resources, including static files and templates. If any additional assets are required, they should be placed in the `assets` directory within the `static` folder.

If any additional files are required for the plugin to function. Maybe a text file plugin needs to read from or any other file, they should be placed in the root directory of the plugin package not in the root directory of the project. This ensures that poetry can package the plugin correctly when it is published to PyPI.

This structure allows for clear separation of concerns and makes it easy to distribute and install plugins. Additionally, each plugin is expected to have its own repository for version control, CI and documentation if needed.

## Plugin API

Plugins must implement the base plugin class provided by the Whitebox. Depending on the plugin's requirements, they can export some or all of the following attributes and methods:

```python
import whitebox

class MyPlugin(whitebox.Plugin):
    name = "My Plugin"
    plugin_template = "plugin_name/plugin_name.html"
    plugin_template_embed = "plugin_name/plugin_name_embed.html"
    plugin_css = [
       "/static/plugin_name/plugin_name.css",
    ]
    plugin_js = [
       "/static/plugin_name/plugin_name.js",
    ]


plugin_class = MyPlugin
```

If a plugin needs to do some processing before sending template or static files, it can override the following methods:

```python
import whitebox

class MyPlugin(whitebox.Plugin):
    name = "My Plugin"

    def get_template(self) -> str:
        """Return the name of the plugin's main template."""
        pass

    def get_template_embed(self) -> str:
        """
        Return the path to the HTML template file, that will be embedded in an
        iframe for the plugin.
        """
        pass

    def get_css(self) -> list:
        """Return the path to the plugin's CSS files."""
        pass

    def get_js(self) -> list:
        """Return the path to the plugin's JavaScript files."""
        pass

plugin_class = MyPlugin
```

To ensure that all static file paths are correctly resolved, it is recommended to use `django.templatetags.static.static`. This function can resolve the correct URL path for serving static files like images exported by plugins. For example, use:

```python
from django.templatetags.static import static

class MyDevicePlugin(Plugin):
    device_image_url = static("whitebox_plugin_device_xyz/path/to/image.webp")

plugin_class = MyDevicePlugin
```

In the end, the plugin class must be exported as `plugin_class`. If this attribute is not present, the plugin will not be loadable by Whitebox.

## Standard API

Whitebox provides a standard API for plugins to interact with the system and other plugins. This API includes methods for:

- Registering event callbacks
- Unregistering event callbacks
- Emitting events
- Accessing shared resources
- Interacting with the database

Example of registering an event callback:

```python
import whitebox

class MyPlugin(whitebox.Plugin):
    def __init__(self):
        self.whitebox.register_event_callback("flight_start", self.on_flight_start)

    async def on_flight_start(self, data):
        print("Flight started")
```

Example of emitting an event:

```python
import whitebox

class MyPlugin(whitebox.Plugin):
    async def update_location(self, lat, lon, alt):
        # Emit a location update
        await self.whitebox.api.location.emit_location_update(lat, lon, alt)

        # Emit a custom event
        await self.whitebox.emit_event("custom_event", {"data": "example"})
```

Refer to [Plugin API Reference](api_reference/plugin.md) for more details on the available methods and properties.

## Understanding the Plugin System

Whitebox employs a dynamic plugin discovery and management system. The process of loading and unloading plugins is as follows:

1. On startup, the system scans the environment for installed plugins with the `whitebox_plugin_` prefix.
2. Each discovered plugin is instantiated and registered with the system.
3. Plugin resources (templates, static files) are registered with Django's asset pipeline.
4. Event callbacks registered by plugins are added to the event system.
5. Device classes available in the plugin are registered with the device manager.
6. Plugins can be unloaded at runtime, removing their resources and event callbacks from the system by simply removing the plugin package `(poetry remove <plugin_name>)` and calling `/plugins/refresh/` endpoint.

When `/plugins/refresh/` is called, the system will rescan the environment for plugins and remove or add any new plugins without requiring a server restart or altering what is currently running. This process ensures that plugins are properly integrated into the system without requiring manual configuration for each new plugin.

## Plugin Development Workflow

Plugin must be initialized using `poetry` and should adhere to the structure outlined in the [Plugin Structure](#plugin-structure) section. The plugin should implement the base plugin class provided by Whitebox and export it as `plugin_class` as outlined in the [Plugin API](#plugin-api) section. The plugin can then interact with the system using the [Standard API](#standard-api) provided by Whitebox.

To set up a development environment for a plugin, follow these steps:

1. Run the development environment container for Whitebox.
2. In plugins folder, create a new plugin project using poetry.
3. Add the plugin to Whitebox using the following command: `poetry add -e path/to/plugin` within the backend development container.
4. Run the Whitebox server.

This installs the plugin in editable mode, allowing you to make changes to the plugin code and see the effects immediately without reinstalling the plugin in whitebox.

### Testing plugins on CI environment (including sandbox)

To include the plugin during CI runs, you need to add the plugin to the
`whitebox` project as a dependency via specific git ref, which means that you'd
have to, after testing, replace that dependency with the actual plugin version
before merging.

As this is mundane and prone to human error, the CI environment allows you to
add a "temporary" dependency that will be used in every CI step where `whitebox`
is being installed. This is done through the `poetry`'s [optional `temporary-dependencies`
dependency group](https://python-poetry.org/docs/managing-dependencies/#optional-groups).

This mechanism will install those dependencies to use for testing and sandbox
deploys, and they will be removed by the CI upon merge.

To add a plugin to the temporary group, you can run:

```bash
poetry add --group temporary-dependencies git+https://gitlab.com/whitebox-aero/whitebox-plugin-name.git#feature/whitebox-1337
```

This will add the plugin by git repository and branch. To test that everything works
well, you can run:

```bash
poetry install --with temporary-dependencies
```

Take note that, when you include `--with temporary-dependencies`, those dependencies
will take precedence over the ones defined in the standard groups. That means
that you can freely add the plugin to the `temporary-dependencies` group with a
specific git ref, without needing to remove it from the standard groups.

## External assets

### Large static assets

In some cases, plugins may require packaging of large assets. As PyPI imposes a
limit of 100MB for any packages it hosts, Whitebox offers a way for plugins to
depend on externally hosted assets, which can, from runtime's perspective, be
considered as a part of the package itself.

Upon plugin loading, Whitebox will ensure that all the external files are downloaded
and ready to be served. To specify external files, create a file in the plugin's root
directory called `external-asset-manifest.json`, for example:

```
whitebox_plugin_<plugin_name>
├── whitebox_plugin_<plugin_name>
│   ├── __init__.py
│   ├── whitebox_plugin_<plugin_name>.py
│   └── external-asset-manifest.json     <--- this one
├── pyproject.toml
```

Every external file needs to have 3 components:

1. URL from which it'll be sourced from

2. Integrity hash

   Every file must have an integrity hash to verify that the downloaded file is
   a proper one. Within the integrity string, you should specify what hashing
   algorithm is used, in format `[ALGORITHM]-[INTEGRITY_HASH]`.

   Supported hashing algorithms: `sha1`, `sha256`.

   Upon plugin loading, all the files will be checked, and:

   - If a file does not exist, it will be downloaded
   - If a file exists, but the file hash does not match, it will be downloaded,
     replacing the existing file. This behavior allows you to freely update your
     manifest file with new files, without worrying whether the files will be stale.

3. Target path where it will be saved locally and served from

   Whitebox saves these files in a special location for asset files, which will be
   available to plugins in the same manner as if they were the ordinary static files
   within the plugins' `static/` folder.

   For example, if your plugin's package name was `whitebox_plugin_r2d2`, and the
   target path is `voices/beep-boop.mp3`, the file will be available for plugin's
   use at path `/static/whitebox_plugin_r2d2/voices/beep-boop.mp3`.

   Additionally, you can freely use
   `{% static "whitebox_plugin_r2d2/voices/beep-boop.mp3" %}`
   template tags in the templates to reference these files, or alternatively use
   `django.templatetags.static.static("whitebox_plugin_r2d2/voices/beep-boop.mp3")`
   for the same purpose from within the code.

For the above example, the asset manifest file would look like this:

```json
{
  "sources": [
    {
        "url": "https://example.org/r2d2/asset-file.mp3",
        "integrity": "sha1-8dfa2f3e56f3abd46119b698bf6a91cb18482c85",
        "target_path": "voices/beep-boop.mp3"
    },
    ... more files
  ]
}
```

To verify whether the manifest file is proper, you can use the Django's
`verify_external_asset_manifest` command, by providing either the installed plugin's
module name, or the path to file, e.g.

- `poetry run python whitebox/manage.py verify_external_asset_manifest --module-name whitebox_plugin_r2d2`, or
- `poetry run python whitebox/manage.py verify_external_asset_manifest path/to/plugin-root/whitebox_plugin_r2d2/external_asset_manifest.json`

## Testing Plugins

Plugins can only be tested from within the whitebox environment. To run tests, you need to have whitebox running locally. The test runner will automatically discover and run all tests in the `whitebox_test_plugin_<plugin_name>` package as long as they follow the naming convention outlined in the [Plugin Structure](#plugin-structure) section.

Unit & Integration tests would usually begin with the following structure:

```python
from django.test import TestCase
from plugin.manager import plugin_manager

class TestWhiteboxPluginExamplePlugin(TestCase):
    def setUp(self) -> None:
        self.plugin = next(
            (
                x
                for x in plugin_manager.plugins
                if x.__class__.__name__ == "WhiteboxPluginExamplePlugin"
            ),
            None,
        )
        return super().setUp()

    def test_plugin_loaded(self):
        self.assertIsNotNone(self.plugin)

    def test_plugin_name(self):
        self.assertEqual(self.plugin.name, "Example Plugin")

    # Add more tests here
```

While browser tests would usually begin with the following structure:

```python
import os
import logging

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from playwright.sync_api import sync_playwright

# Disable warnings
logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)
logging.getLogger("django.request").setLevel(logging.ERROR)
logging.getLogger("django.server").setLevel(logging.ERROR)

class TestWhiteboxPluginExamplePluginBrowser(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
        super().setUpClass()
        cls.playwright = sync_playwright().start()
        cls.browser = cls.playwright.chromium.launch(headless=True)
        cls.context = cls.browser.new_context()
        cls.page = cls.context.new_page()

    @classmethod
    def tearDownClass(cls):
        cls.page.close()
        cls.context.close()
        cls.browser.close()
        cls.playwright.stop()
        super().tearDownClass()

    def setUp(self):
        self.page.goto(f"{self.live_server_url}{reverse('index')}")

    # Add more tests here
```

Additionally to ensure browser tests run correctly, you would have to install playwright additional dependencies. To install playwright dependencies, follow the steps below:

1. Run: `poetry run playwright install`
2. Run: `poetry run playwright install-deps` (optional, for Linux systems only)
3. Ensure you have added plugin to whitebox: `poetry add -e path/to/plugin`.

Finally you would run test on whitebox using the following command:

```bash
make test
```

## Writing CI

The CI for a plugin would almost always get extended from the whitebox [shared CI](https://gitlab.com/whitebox-aero/whitebox/-/blob/main/.gitlab/config/shared-ci.yml)
file. The CI would usually look like this:

```yaml
image: python:3.10

stages:
  - setup
  - lint
  - test
  - update_version
  - publish

include:
  - project: "whitebox-aero/whitebox"
    ref: "main"
    file: ".gitlab/config/shared-ci.yml"

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.pip-cache"
  POETRY_HOME: "$CI_PROJECT_DIR/.poetry"

cache:
  paths:
    - .pip-cache/
    - .poetry/
    - .venv/

run_setup:
  extends: .shared_plugin_setup

run_lint:
  extends: .shared_plugin_lint

run_test:
  extends: .shared_plugin_test

update_version:
  extends: .shared_plugin_update_version

publish:
  extends: .shared_plugin_publish
```

## Versioning Plugins

Whitebox uses [Semantic Versioning](https://semver.org/) for versioning plugins.
In the CI file above, the `update_version` stage is responsible for updating the
version of the plugin.

When a merge request is merged to `main`, first, patch version will be bumped in the
`pyproject.toml` file. A commit will be made with the new version, along with a new tag,
which the CI will push to the repository. After that, the plugin will be published,
as outlined below.

It is important to ensure that the version is incrementally updated, so that the plugin
can be published correctly to PyPI.

### Automatic versioning setup

The following steps apply for setting up automatic versioning on GitLab,
orchestrated by `.gitlab-ci.yml`.

1. Set up an `Access Token` that CI will use to update the version

   - Go to your repository's settings on Gitlab: `Settings > Access Tokens`
   - Create a new token with the following settings:
     - Name: name that you want to appear as the committer (e.g. `Whitebox CI`)
     - Role: `Maintainer`
     - Scopes: `read_repository`, `write_repository`
   - Copy the token, as it won't be displayed again

2. Add the token to your repository's CI/CD settings

   - Go to your repository's settings on Gitlab: `Settings > CI/CD`
   - Open `Variables`
   - Add a new variable with the following settings:
     - Type: `Variable`
     - Environment scope: `All`
     - Visibility: `Masked`
     - Key: `PUSH_TOKEN`
     - Value: the token you copied

3. Ensure that the `update_version` stage is set up in your `.gitlab-ci.yml`

   - The stage should include the following job:
     ```yaml
     update_version:
       extends: .shared_plugin_update_version
     ```

After this is set up, the CI will automatically update the version of the plugin
when a merge request is merged to the default branch (`main`). This version will
automatically be used when the plugin is published.

## Publishing Plugins

<!-- prettier-ignore-start -->

1. Initial Setup for New Plugins/Repositories:

    - If the plugin is new and does not have a PyPI project, you need to create it using your PyPI account.
    - Perform the initial publish using the command:
    
        ```bash
        poetry publish --build
        ```

    - This should be done from your local machine to automatically create the package on PyPI.
    - Once the project is set up on PyPI, add `antoviaque` as an owner to the project to share access and management.

2. Setting Up PyPI Access Tokens for CI:

    - Create a new access token on your PyPI account. This token should be scoped specifically for the project to limit permissions effectively.
    - Add this project-scoped token to your CI environment configuration to
      enable automated publishing for future releases, similarly to the `PUSH_TOKEN`
      setup above:
      - Type: `Variable`
      - Environment scope: `All`
      - Visibility: `Masked`
      - Key: `PYPI_TOKEN`
      - Value: the token you created

3. For Existing PyPI Projects:

    - If there is already a PyPI project for the repository, you will need to request access from one of the current maintainers. You can find the maintainers listed on the project’s page on the [PyPI website](https://pypi.org/).

<!-- prettier-ignore-end -->

## Next Steps

- Learn more about the [Whitebox Architecture](architecture.md)
- Check out the [Development Guide](development_guide.md)
