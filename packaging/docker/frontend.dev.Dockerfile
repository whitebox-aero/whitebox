FROM frontend-base:latest
WORKDIR /app

# Copy dependencies files
COPY frontend/package.json frontend/package-lock.json ./

# Install dependencies and playwright with dependencies
RUN npm install
RUN npx playwright install && npx playwright install-deps

# Add entrypoint script
COPY packaging/scripts/frontend/entrypoint.dev.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Set entrypoint and command
ENTRYPOINT ["/entrypoint.sh"]
CMD [ "tail", "-f", "/dev/null" ]
