FROM frontend-base:latest as builder
WORKDIR /frontend-build

# Install dependencies first, to cache them when there are no dependency changes
COPY frontend/package.json /frontend-build
COPY frontend/package-lock.json /frontend-build
RUN npm ci

# Build frontend
COPY frontend/public/ /frontend-build/public
COPY frontend/src/ /frontend-build/src
COPY frontend/index.html /frontend-build
COPY frontend/*.config.js /frontend-build

RUN npm run build

COPY packaging/scripts/frontend/50-entrypoint-generate-env-inject.sh /frontend-build/50-entrypoint-generate-env-inject.sh

FROM nginx:stable-alpine

COPY --from=builder /frontend-build/50-entrypoint-generate-env-inject.sh /docker-entrypoint.d/50-entrypoint-generate-env-inject.sh
RUN chmod +x /docker-entrypoint.d/50-entrypoint-generate-env-inject.sh

COPY --from=builder /frontend-build/dist /usr/share/nginx/html
COPY packaging/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
