FROM ubuntu:22.04

# Avoid prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt update
RUN apt update && \
    apt install -y \
    git \
    wget \
    make \
    libncurses5-dev \
    libncursesw5-dev \
    librtlsdr-dev \
    libusb-1.0-0-dev \
    golang \
    udev && \
    rm -rf /var/lib/apt/lists/*

# Create necessary directories
RUN mkdir -p /boot/firmware && \
    mkdir -p /etc/init && \
    mkdir -p /etc/udev/rules.d

# Create necessary files
RUN touch /etc/init/fancontrol.conf

# Clone the Stratux repository
RUN git clone https://github.com/b3nn0/stratux.git

# Set the working directory
WORKDIR /stratux

# Pin stratux to last known working version
RUN git checkout tags/v1.6r1-eu032

# Build the Stratux binaries
RUN make all && make install

# Entrypoint script
COPY packaging/scripts/stratux/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD ["/entrypoint.sh"]
