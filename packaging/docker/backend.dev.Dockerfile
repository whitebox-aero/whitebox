FROM backend-base:latest
WORKDIR /app

# Copy backend directory
COPY backend/ /app/

# `TEMPORARY_DEPENDENCIES` arg is set with `TEMPORARY_DEPENDENCIES=1`
# environment variable during development testing, to pull any MR-specific
# dependency overrides
ARG TEMPORARY_DEPENDENCIES

RUN echo "TEMPORARY_DEPENDENCIES=$TEMPORARY_DEPENDENCIES"; sleep 5

# Install dependencies
RUN if [ "$TEMPORARY_DEPENDENCIES" = "1" ]; \
      then poetry install --with temporary-dependencies; \
    else \
      poetry install; \
    fi

# Install playwright with dependencies
RUN poetry run playwright install --with-deps

# Download external assets
RUN make download_external_assets

# Add entrypoint script
COPY packaging/scripts/backend/entrypoint.dev.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Set entrypoint and command
ENTRYPOINT ["/entrypoint.sh"]
CMD [ "tail", "-f", "/dev/null" ]
