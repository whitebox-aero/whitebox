FROM backend-base:latest
WORKDIR /app

# Copy backend directory
COPY backend/ /app/

# `TEMPORARY_DEPENDENCIES` arg is set through the ansible-generated
# `docker-compose.yml` file on CI, and with `TEMPORARY_DEPENDENCIES=1`
# environment variable during development testing, to pull any MR-specific
# dependency overrides
ARG TEMPORARY_DEPENDENCIES

# Install dependencies
RUN if [ "$TEMPORARY_DEPENDENCIES" = "1" ]; \
      then poetry install --with temporary-dependencies; \
    else \
      poetry install; \
    fi

# Download external assets
RUN make download_external_assets

# Add entrypoint script
COPY packaging/scripts/backend/entrypoint.prod.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

# Set entrypoint and command
ENTRYPOINT ["/app/entrypoint.sh"]
CMD [ "make", "run" ]
