FROM python:3.10.14

# Set environment variables
ENV POETRY_VIRTUALENVS_PATH=/opt/venvs
ENV POETRY_VIRTUALENVS_IN_PROJECT=false
ENV PYTHONUNBUFFERED=1

# Update and install dependencies
RUN apt update
RUN pip install poetry
RUN apt install python3-dev libpq-dev gcc make -y

# Set workdir
WORKDIR /app

# Copy dependencies files
COPY backend/pyproject.toml backend/poetry.lock ./

# Install dependencies
# --no-root is used to avoid installing the project itself as we are not copying project files.
# This step is done to ensure all common dependencies are built and cached in this base image.
RUN poetry install --no-root
