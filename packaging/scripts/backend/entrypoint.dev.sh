#!/bin/sh

# Entrypoint script can't be in /app because it's a volume. Hence, we change the directory to /app first.
cd /app

# Apply existing migrations
echo "Applying existing migrations..."
make migrate
echo "Successfully applied existing migrations."
echo "Please run 'make migrate' to apply new migrations during development."

# Final message
echo "Development environment is ready."

# Start the django development environment
exec "$@"
