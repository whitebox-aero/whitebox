#!/bin/sh

# Wait for the database to be ready (in some cases, the database may not be ready to accept connections immediately)
echo "Waiting for 2 seconds to ensure the database is ready for accepting connections..."
sleep 2

# Apply migrations
echo "Applying existing migrations..."
make migrate

# Start the Django server
exec "$@"
