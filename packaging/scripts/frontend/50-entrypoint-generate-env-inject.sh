#!/bin/sh

# Generate environment script for `react-inject-env` usage
#
# As we are using multi-stage builds to build the frontend
# (refer to `packaging/docker/frontend.Dockerfile`),
# the resulting image will not `npm` installed, meaning
# that we cannot use `npx react-inject-env set` to generate the `env.js` file.
# Instead, we will generate the `env.js` file in the entrypoint script, to allow
# the end-package to remain as thin as possible.

# Path to the `env.js` file within the Docker container
EXPORT_FILE="/usr/share/nginx/html/env.js"

# Define JS object
echo "window.env = {" > $EXPORT_FILE

# Collect targets
TARGETS="$(env | grep '^VITE_.*=')"

# Output targets to the JS object one by one
for var in $TARGETS; do
  key=$(echo "$var" | cut -d'=' -f1)
  value=$(echo "$var" | cut -d'=' -f2-)
  echo "  \"$key\": \"$value\"," >> $EXPORT_FILE
done

# Close the JS object
echo "}" >> $EXPORT_FILE
