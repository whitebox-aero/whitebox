#!/bin/sh

# Entrypoint script can't be in /app because it's a volume. Hence, we change the directory to /app first.
cd /app

# Final message
echo "Development environment is ready."

# Start the development environment
exec "$@"
