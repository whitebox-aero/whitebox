#!/bin/sh

# Ensure proper permissions for USB devices
chmod 666 /dev/bus/usb/*/*

# Reload udev rules
udevadm control --reload-rules
udevadm trigger

# Run the pre-start script
/opt/stratux/bin/stratux-pre-start.sh

# Start gen_gdl90
exec /opt/stratux/bin/gen_gdl90
