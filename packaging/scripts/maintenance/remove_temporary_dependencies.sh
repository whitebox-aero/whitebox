echo "Removing CI testing dependencies..."

TARGET_GROUP="temporary-dependencies"
poetry_output=$(poetry show --only $TARGET_GROUP --top-level 2>/dev/null)

if [ $? -ne 0 ]; then
  echo "Dependency group \"$TARGET_GROUP\" does not exist in pyproject.toml"
  exit 1
fi

if [ -z "$poetry_output" ]; then
  echo "No temporary dependencies set in the \"$TARGET_GROUP\" group"
  exit 0
fi


targets=$(echo "$poetry_output" | awk '{print $1}')

echo
echo "Targets to remove from \"$TARGET_GROUP\" group:"
for target in $targets; do
  echo "  - $target"
done

echo

poetry remove --group $TARGET_GROUP $targets
