import argparse
import json


class Version:
    def __init__(self, major, minor, patch):
        self.major = int(major)
        self.minor = int(minor)
        self.patch = int(patch)

    def __str__(self):
        return "{}.{}.{}".format(self.major, self.minor, self.patch)


def extract_version_from_pyproject_toml(pyproject_toml):
    with open(pyproject_toml, "r") as f:
        lines = f.readlines()

    for line in lines:
        if "version" in line:
            version_info = line.split("=")[1].strip().strip('"')
            break
    else:
        raise ValueError("version not found in pyproject.toml")

    version = Version(*version_info.split("."))
    return version


def get_next_version(from_version):
    next_version = Version(
        from_version.major,
        from_version.minor,
        from_version.patch + 1,
    )
    return next_version


def set_version_pyproject_toml(pyproject_toml, from_version, to_version):
    with open(pyproject_toml, "r+") as f:
        content = f.read()

        lookup = 'version = "{}"'.format(from_version)
        replacement = 'version = "{}"'.format(to_version)

        content = content.replace(lookup, replacement, 1)

        f.seek(0)
        f.write(content)
        f.truncate()


def set_version_package_json(package_json, to_version):
    # `package.json`'s current version will be just discarded, no matter what
    # it is set to. The new version will be set to the new version number in
    # sync with the backend version (in `pyproject.toml`)
    with open(package_json, "r+") as f:
        content = f.read()

        parsed_package_json = json.loads(content)
        current_version_string = parsed_package_json["version"]

        lookup = '"version": "{}"'.format(current_version_string)
        replacement = '"version": "{}"'.format(to_version)

        content = content.replace(lookup, replacement, 1)

        f.seek(0)
        f.write(content)
        f.truncate()


def main(args):
    pyproject_toml = args.pyproject_toml
    package_json = args.package_json

    current_version = extract_version_from_pyproject_toml(pyproject_toml)
    next_version = get_next_version(current_version)

    set_version_pyproject_toml(pyproject_toml, current_version, next_version)
    set_version_package_json(package_json, next_version)

    # In case of a successful update, the command should print only the new
    # version number to stdout. This is to allow the CI/CD pipeline to use the
    # new version number in the next steps
    print(next_version)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pyproject-toml",
        type=str,
        required=True,
        help="Path to pyproject.toml",
    )
    parser.add_argument(
        "--package-json",
        type=str,
        required=True,
        help="Path to package.json",
    )

    parsed_args = parser.parse_args()
    main(parsed_args)
