from unittest import TestCase
from unittest.mock import mock_open, patch

import whitebox_update_version


PYPROJECT_TOML_CONTENT = """
[tool.poetry]
name = "whitebox"
version = "1.2.3"

[tool.poetry.dependencies]
python = ">=3.10.0"
"""


PACKAGE_JSON_CONTENT = """
{
  "name": "whitebox",
  "version": "1.2.3",
  "description": "Whitebox is a non-blackbox blackbox"
}
"""


class TestWhiteboxUpdateVersionScript(TestCase):
    def test_version_string_repr(self):
        version = whitebox_update_version.Version(1, 2, 3)
        self.assertEqual(str(version), "1.2.3")

    def test_extract_version_from_pyproject_toml(self):
        with patch(
            "builtins.open",
            mock_open(read_data=PYPROJECT_TOML_CONTENT),
        ) as m:
            version = whitebox_update_version.extract_version_from_pyproject_toml(
                "pyproject.toml",
            )
            m.assert_called_with("pyproject.toml", "r")

            self.assertIsInstance(version, whitebox_update_version.Version)
            self.assertEqual(version.major, 1)
            self.assertEqual(version.minor, 2)
            self.assertEqual(version.patch, 3)

    def test_get_next_version(self):
        version = whitebox_update_version.Version(1, 2, 3)
        next_version = whitebox_update_version.get_next_version(version)

        self.assertIsInstance(next_version, whitebox_update_version.Version)
        self.assertEqual(next_version.major, 1)
        self.assertEqual(next_version.minor, 2)
        self.assertEqual(next_version.patch, 4)

    def test_set_version_pyproject_toml(self):
        pyproject_toml = "pyproject.toml"
        from_version = whitebox_update_version.Version(1, 2, 3)
        to_version = whitebox_update_version.Version(1, 2, 4)

        with patch(
            "builtins.open",
            mock_open(read_data=PYPROJECT_TOML_CONTENT),
        ) as m:
            whitebox_update_version.set_version_pyproject_toml(
                pyproject_toml,
                from_version,
                to_version,
            )
            m.assert_called_with(pyproject_toml, "r+")

            expected_content = PYPROJECT_TOML_CONTENT.replace(
                'version = "1.2.3"',
                'version = "1.2.4"',
            )
            f = m.return_value
            f.seek.assert_called_with(0)
            f.write.assert_called_with(expected_content)
            f.truncate.assert_called_with()

    def test_set_version_package_json(self):
        package_json = "package.json"
        to_version = whitebox_update_version.Version(1, 2, 4)

        with patch(
            "builtins.open",
            mock_open(read_data=PACKAGE_JSON_CONTENT),
        ) as m:
            whitebox_update_version.set_version_package_json(
                package_json,
                to_version,
            )
            m.assert_called_with(package_json, "r+")

            expected_content = PACKAGE_JSON_CONTENT.replace(
                '"version": "1.2.3"',
                '"version": "1.2.4"',
            )
            f = m.return_value
            f.seek.assert_called_with(0)
            f.write.assert_called_with(expected_content)
            f.truncate.assert_called_with()

    @patch(
        "whitebox_update_version.extract_version_from_pyproject_toml",
        return_value=whitebox_update_version.Version(1, 2, 3),
    )
    @patch(
        "whitebox_update_version.get_next_version",
        return_value=whitebox_update_version.Version(1, 2, 4),
    )
    @patch("whitebox_update_version.set_version_pyproject_toml")
    @patch("whitebox_update_version.set_version_package_json")
    def test_main(
        self,
        mock_package_json,
        mock_pyproject_toml,
        mock_next_version,
        mock_extract_version,
    ):
        mock_args = type("", (), {})()
        mock_args.pyproject_toml = "pyproject.toml"
        mock_args.package_json = "package.json"

        with patch("builtins.print") as mock_print:
            whitebox_update_version.main(mock_args)

        mock_print.assert_called_once_with(mock_next_version.return_value)

        mock_extract_version.assert_called_with("pyproject.toml")
        mock_next_version.assert_called_with(
            mock_extract_version.return_value,
        )
        mock_pyproject_toml.assert_called_with(
            "pyproject.toml",
            mock_extract_version.return_value,
            mock_next_version.return_value,
        )
        mock_package_json.assert_called_with(
            "package.json",
            mock_next_version.return_value,
        )
