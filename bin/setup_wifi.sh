#!/bin/bash

set -e

echo "Setting whitebox access point"

SSID=whitebox
PASSWORD=12345678
INTERFACE=wlan0

echo "Setting WiFi SSID to ${SSID}"
sudo nmcli con add con-name hotspot ifname ${INTERFACE} type wifi ssid ${SSID}

echo "Setting WiFi password to ${PASSWORD}"
sudo nmcli con modify hotspot wifi-sec.key-mgmt wpa-psk
sudo nmcli con modify hotspot wifi-sec.psk ${PASSWORD}

echo "Setting WiFi mode to AP"
sudo nmcli con modify hotspot 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared

echo "You can now connect to the WiFi network ${SSID} with password ${PASSWORD}"

echo "Rebooting to apply changes"
sudo reboot now
