# Hotfix MR Template

## Urgency Level

- Describe the severity of the issue and why this is urgent.

## Fix Description

- Explain what the hotfix addresses.

# Anything Else?

- Add any additional information that may be relevant to this MR.

# Before Merge Checklist

- [ ] Test all new code (unit & integration)
- [ ] Update the documentation (if applicable).
- [ ] Pass 2 code reviews (get two :thumbsup: and no outstanding comment)
- [ ] Pass 1 product review (if the code change affects users)

# Follow-up Issues Or MRs (if applicable)

- List any follow-up issues or MRs.
