# Merge Request Template

## What

- Describe what changes are being introduced in this MR.

## Why

- Explain why these changes are necessary or beneficial.

## How

- Provide a brief overview of how the changes were implemented (e.g., technologies used, main components affected).

# Testing

## Sandbox Hosting [auto-managed section, do not edit manually]

## Sandbox Testing Instructions (if applicable)

- **Testing Steps:** Provide a list of instructions to test the changes in the sandbox for product review.

## Developer Testing Instructions

- **Setup Instructions:** Detail steps for developers to pull and set up the changes locally.
- **Testing Steps:** Provide a list of steps to test the changes locally.

## Common Testing Instructions

- Include any steps common to both sandbox and developer testing to avoid duplication.

# Screenshots (if applicable)

- Add screenshots of the changes if applicable.

# Anything Else?

- Add any additional information that may be relevant to this MR.

# Before Merge Checklist

- [ ] Test all new code (unit & integration)
- [ ] Update the documentation (if applicable).
- [ ] Pass 2 code reviews (get two :thumbsup: and no outstanding comment)
- [ ] Pass 1 product review (if the code change affects users)

# Follow-up Issues Or MRs (if applicable)

- List any follow-up issues or MRs.
