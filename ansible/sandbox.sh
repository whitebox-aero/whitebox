#!/bin/bash

set -e

usage() {
    echo "Usage: $0 <repo_url|remote_name> <commit_hash|branch_name> <action_type>"
    echo "Actions: deploy, destroy"
    echo
    echo "Examples:"
    echo " $0 https://gitlab.com/whitebox-aero/whitebox 09b1d86e deploy"
    echo " $0 origin feature/123 deploy"
    echo
    echo "Note:"
    echo "1. When given a commit hash while 'destroy', only the sandbox with that commit hash will be destroyed."
    echo "2. When given a branch name while 'destroy', all sandboxes with that branch will be destroyed."
}

check_git_installed() {
    if ! command -v git &> /dev/null; then
        echo "Error: Git is not installed."
        echo "Please install Git and try again."
        echo "For installation instructions, visit: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git"
        exit 1
    fi
}

resolve_repo_url() {
    local remote_name=$1
    local repo_url

    # Check if the remote name is an HTTP URL
    if [[ $remote_name == http* ]]; then
        repo_url=$remote_name

    # Check if the remote name is a git URL
    elif [[ $remote_name == git* ]]; then
        repo_url=$remote_name

    # Else, resolve the remote name to a URL
    else
        repo_url=$(git remote get-url $remote_name) || {
            echo "Remote '$remote_name' not found"
            exit 1
        }
    fi

    # Convert SSH URLs (git@) to HTTPS URLs
    if [[ $repo_url =~ ^git@([^:]+):(.+)\.git$ ]]; then
        local host="${BASH_REMATCH[1]}"
        local repo_path="${BASH_REMATCH[2]}"
        repo_url="https://${host}/${repo_path}.git"
    fi

    echo $repo_url
}

resolve_to_commit_hash() {
    local repo_url=$1
    local branch_or_commit=$2

    # If the branch_or_commit is a commit hash, return it
    if [[ $branch_or_commit =~ ^[0-9a-f]{40}$ ]] || [[ $branch_or_commit =~ ^[0-9a-f]{8}$ ]]; then
        echo "$branch_or_commit"
        return
    fi

    # Capture the output of git ls-remote
    local output
    output=$(git ls-remote "$repo_url" "$branch_or_commit")

    # Check if the output is empty
    if [ -z "$output" ]; then
        >&2 echo "Could not resolve commit hash for branch or commit '$branch_or_commit'"
        exit 1
    fi

    # Extract the commit hash
    local resolved_commit_hash
    resolved_commit_hash=$(echo "$output" | cut -f1)

    echo "$resolved_commit_hash"
}

is_commit_hash() {
    local commit_hash=$1
    [[ $commit_hash =~ ^[0-9a-f]{40}$ ]]
}

main() {
    # Check if the number of arguments is correct else print usage
    if [ "$#" -ne 3 ]; then
        usage
        exit 1
    fi

    # Check if git is installed
    check_git_installed

    # Parse the arguments
    REPO_SOURCE=$1
    BRANCH_OR_COMMIT=$2
    ACTION_TYPE=$3

    # Resolve the repo URL and commit hash
    REPO_URL=$(resolve_repo_url "$REPO_SOURCE")

    case "$ACTION_TYPE" in
        deploy)
            COMMIT_HASH=$(resolve_to_commit_hash "$REPO_URL" "$BRANCH_OR_COMMIT")
            echo "Deploying sandbox with commit hash: $COMMIT_HASH"
            ansible-playbook deploy.yml \
                -e "repo_url=$REPO_URL" \
                -e "commit_hash=$COMMIT_HASH" \
                -v
            ;;
        destroy)
            if is_commit_hash "$BRANCH_OR_COMMIT"; then
                COMMIT_HASH=$BRANCH_OR_COMMIT
                echo "Destroying sandbox with commit hash: $COMMIT_HASH"
                ansible-playbook destroy.yml \
                    -e "repo_url=$REPO_URL" \
                    -e "commit_hash=$COMMIT_HASH" \
                    -e "branch_name=undefined" \
                    -v
            else
                echo "Destroying all sandboxes with branch: $BRANCH_OR_COMMIT"
                ansible-playbook destroy.yml \
                    -e "repo_url=$REPO_URL" \
                    -e "commit_hash=undefined" \
                    -e "branch_name=$BRANCH_OR_COMMIT" \
                    -v
            fi
            ;;
    esac
}

main "$@"
