import unittest
import logging

from unittest.mock import patch, mock_open, MagicMock
from update_sandbox_info import (
    extract_urls_from_log,
    update_mr_description,
    prepare_sandbox_info_artifact,
    main,
)


VALID_LOG_CONTENT = """
TASK [Sandbox URLs] ******************************************************************************************************************************************************
ok: [sandbox_01] => {
    "msg": "Backend: http://10.0.0.1:8000\\nFrontend: http://10.0.0.1:3000"
}
"""

INVALID_LOG_CONTENT = """
Some deployment logs...
No URLs here
More logs...
"""

EXISTING_MR_DESCRIPTION = """
...

## Sandbox Hosting [auto-managed section, do not edit manually]

## Sandbox Testing Instructions (if applicable)

...
"""


class TestSandboxUpdate(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.env_patcher = patch.dict(
            "os.environ",
            {
                "GITLAB_TOKEN": "dummy_token",
                "CI_PROJECT_ID": "123",
                "CI_MERGE_REQUEST_IID": "456",
            },
        )
        self.env_patcher.start()

    def tearDown(self):
        logging.disable(logging.NOTSET)
        self.env_patcher.stop()

    def test_extract_urls_from_log_valid(self):
        with patch("builtins.open", mock_open(read_data=VALID_LOG_CONTENT)):
            result = extract_urls_from_log()
            self.assertEqual(
                result,
                {"backend": "http://10.0.0.1:8000", "frontend": "http://10.0.0.1:3000"},
            )

    def test_extract_urls_from_log_invalid(self):
        with patch("builtins.open", mock_open(read_data=INVALID_LOG_CONTENT)):
            result = extract_urls_from_log()
            self.assertIsNone(result)

    def test_extract_urls_from_log_file_not_found(self):
        with patch("builtins.open", mock_open()) as mock_file:
            mock_file.side_effect = FileNotFoundError()
            with self.assertRaises(FileNotFoundError):
                extract_urls_from_log()

    def test_update_mr_description_new_section(self):
        # Mock GitLab API
        mock_mr = MagicMock()
        mock_mr.description = "Original description"

        with patch("gitlab.Gitlab") as MockGitlab:
            mock_project = MagicMock()
            mock_project.mergerequests.get.return_value = mock_mr
            MockGitlab.return_value.projects.get.return_value = mock_project

            urls = {
                "backend": "http://10.0.0.1:8000",
                "frontend": "http://10.0.0.1:3000",
            }

            update_mr_description(urls)

            # Verify the description was updated
            self.assertIn("http://10.0.0.1:8000", mock_mr.description)
            self.assertIn("http://10.0.0.1:3000", mock_mr.description)
            mock_mr.save.assert_called_once()

    def test_update_mr_description_replace_existing(self):
        # Mock GitLab API
        mock_mr = MagicMock()
        mock_mr.description = EXISTING_MR_DESCRIPTION

        with patch("gitlab.Gitlab") as MockGitlab:
            mock_project = MagicMock()
            mock_project.mergerequests.get.return_value = mock_mr
            MockGitlab.return_value.projects.get.return_value = mock_project

            urls = {
                "backend": "http://10.0.0.1:8000",
                "frontend": "http://10.0.0.1:3000",
            }

            update_mr_description(urls)

            # Verify the description was updated
            self.assertNotIn("http://old.frontend", mock_mr.description)
            self.assertNotIn("http://old.backend", mock_mr.description)
            self.assertIn("http://10.0.0.1:8000", mock_mr.description)
            self.assertIn("http://10.0.0.1:3000", mock_mr.description)
            mock_mr.save.assert_called_once()

    def test_prepare_sandbox_info_artifact(self):
        urls = MagicMock()

        with (
            patch("builtins.open", mock_open()) as mock_file,
            patch("json.dump") as mock_json_dump,
        ):
            prepare_sandbox_info_artifact(urls)

            mock_json_dump.assert_called_once_with(urls, mock_file())

    def test_main_success(self):
        with (
            patch("update_sandbox_info.extract_urls_from_log") as mock_extract,
            patch("update_sandbox_info.update_mr_description") as mock_update,
            patch("update_sandbox_info.prepare_sandbox_info_artifact") as mock_prep,
        ):
            mock_extract.return_value = {
                "backend": "http://10.0.0.1:8000",
                "frontend": "http://10.0.0.1:3000",
            }

            main()

            mock_extract.assert_called_once()
            mock_update.assert_called_once()
            mock_prep.assert_called_once()

    def test_main_no_urls_found(self):
        """Test main function when no URLs are found"""
        with patch("update_sandbox_info.extract_urls_from_log") as mock_extract:
            mock_extract.return_value = None

            with self.assertRaises(SystemExit) as cm:
                main()

            self.assertEqual(cm.exception.code, 1)

    def test_main_update_fails(self):
        """Test main function when MR update fails"""
        with (
            patch("update_sandbox_info.extract_urls_from_log") as mock_extract,
            patch("update_sandbox_info.update_mr_description") as mock_update,
            patch("update_sandbox_info.prepare_sandbox_info_artifact") as mock_prep,
        ):
            mock_extract.return_value = {
                "backend": "http://10.0.0.1:8000",
                "frontend": "http://10.0.0.1:3000",
            }
            mock_update.side_effect = Exception("API Error")

            with self.assertRaises(SystemExit) as cm:
                main()

            self.assertEqual(cm.exception.code, 1)
            mock_prep.assert_not_called()
