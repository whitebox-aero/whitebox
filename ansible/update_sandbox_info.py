"""
Convenience script to extract sandbox URLs from Ansible deploy log and update the merge request description with them.
This script is intended to be run as a GitLab CI job after the deployment job for sandbox finishes.
"""

import os
import re
import json
import logging

import gitlab


logger = logging.getLogger("update_sandbox_info")
handler = logging.StreamHandler()
formatter = logging.Formatter(
    "%(asctime)s: %(levelname)s: %(name)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


SECTION_HEADER = "## Sandbox Hosting [auto-managed section, do not edit manually]"


def extract_urls_from_log(log_file: str = "deploy.log") -> dict:
    """
    Extract backend and frontend URLs from Ansible output.

    Parameters:
        log_file: Path to the Ansible deploy log file.

    Returns:
        A dictionary with 'backend' and 'frontend' keys.
    """

    with open(log_file, "r") as f:
        content = f.read()

    urls_pattern = r"Backend: (http:\/\/[\d.:]+)\\nFrontend: (http:\/\/[\d.:]+)"
    match = re.search(urls_pattern, content)

    if match:
        return {"backend": match.group(1), "frontend": match.group(2)}
    return None


def find_sandbox_section_position(description: str) -> tuple:
    """
    Find the start and end positions of the sandbox URL section in the merge request description.

    Parameters:
        description: The current merge request description.

    Returns:
        A tuple with the start and end positions of the section, and a boolean indicating if the section was found.
    """

    # Look for the auto-managed section header
    section_start = description.find(SECTION_HEADER)

    if section_start == -1:
        return len(description), len(description), False

    # Find the next section heading or end of description
    next_section_match = re.search(
        r"\n##\s+", description[section_start + len(SECTION_HEADER) :]
    )
    if next_section_match:
        section_end = section_start + len(SECTION_HEADER) + next_section_match.start()
    else:
        section_end = len(description)

    return section_start, section_end, True


def update_mr_description(urls: dict) -> None:
    """
    Update merge request description with sandbox URLs.

    Parameters:
        urls: A dictionary with 'backend' and 'frontend' keys.

    Returns:
        None
    """

    gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GITLAB_TOKEN"])
    project = gl.projects.get(os.environ["CI_PROJECT_ID"])
    mr = project.mergerequests.get(os.environ["CI_MERGE_REQUEST_IID"])

    current_description = mr.description or ""

    sandbox_section = f"""{SECTION_HEADER}
- **Frontend:** {urls['frontend']}
- **Backend:** {urls['backend']}
"""

    start_pos, end_pos, section_found = find_sandbox_section_position(
        current_description
    )

    if not section_found:
        new_description = current_description.rstrip() + "\n\n" + sandbox_section
    else:
        new_description = (
            current_description[:start_pos].rstrip()
            + "\n\n"
            + sandbox_section
            + current_description[end_pos:].lstrip()
        )

    mr.description = new_description
    mr.save()


def prepare_sandbox_info_artifact(urls):
    with open("sandbox_info.json", "w") as f:
        json.dump(urls, f)


def main():
    urls = extract_urls_from_log()
    if not urls:
        logger.exception("No URLs found in the deploy log")
        exit(1)

    try:
        update_mr_description(urls)
        logger.info("Successfully updated MR description with sandbox URLs")
    except Exception as e:
        logger.exception("Failed to update MR description")
        exit(1)

    prepare_sandbox_info_artifact(urls)


if __name__ == "__main__":
    main()
