# Installation

1. Install Ansible:

   - For Ubuntu: `sudo apt install ansible`
   - For other systems, follow the [official Ansible installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

2. Ensure your SSH keys are added to the server so you can log in without the usual `user:pass` method.

3. Clone this repository and navigate to the `ansible` directory:

   ```bash
   git clone https://gitlab.com/whitebox-aero/whitebox
   cd whitebox/ansible
   ```

4. Install Ansible requirements:

   ```bash
   ansible-galaxy collection install -r requirements.yml
   ```

   Note: This command installs the required Ansible collections locally in your user's home directory (`~/.ansible/collections`).

5. Now, you are ready to deploy or destroy sandboxes!

# Usage

```bash
> ./sandbox.sh
Usage: ./sandbox.sh <repo_url|remote_name> <commit_hash|branch_name> <action_type>
Actions: deploy, destroy

Examples:
 ./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e deploy
 ./sandbox.sh origin feature/123 deploy

Note:
1. When given a commit hash while 'destroy', only the sandbox with that commit hash will be destroyed.
2. When given a branch name while 'destroy', all sandboxes with that branch will be destroyed.
```

# Deploy Sandbox

```bash
./sandbox.sh origin feature/90-auto-deploy deploy
OR
./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e7e3366091e02870caaff6d60ee88e179 deploy
```

# Destroy Sandbox

```bash
./sandbox.sh origin feature/90-auto-deploy destroy
OR
./sandbox.sh https://gitlab.com/whitebox-aero/whitebox 09b1d86e7e3366091e02870caaff6d60ee88e179 destroy
```

# Note

When deploying a sandbox with ansible, it automatically adds `{{ ansible_host }}` to the `DJANGO_ALLOWED_HOSTS` ENV variable in the `compose.yml` file. This adds public IP to `ALLOWED_HOSTS` in Django settings. This ensures that the Django server can be accessed from the public IP of the server.
